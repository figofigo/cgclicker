#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-27 23:30:58
LastEditTime: 2024-04-09 22:10:24
LastEditors: figo - uz
Description: 和战斗资源(血蓝)有关的查询功能
copyright: figo software 2020-2021
'''

import os
import sys
sys.path.append('.')
import math
import logging
import PIL
import ctypes

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('source_check')
logger.setLevel(logging.INFO)
import time
import json
import pyautogui
import os
import random


class source_check():
    def __init__(self,win):
        super(source_check,self).__init__()
        self.win = win
        self.rect = self.win.rect
        # 截图检查的时候，确定一下区域
        

    def number_indentyfy(self,data_soup):
        '''
        description: 根据data_soup分析数字
        return {*}
        '''    
        dataparttern = {
            'e':"0000000",#[False,False,False,False,False,False,False],
            '0':"0111100",#[False,True ,True ,True ,True ,False,False],
            '1':"0100011",#[False,True ,False,False,False,True ,True ],
            '2':"0101001",#[False,True ,False,True ,False,False,True ],
            '3':"0111001",#[False,True, True, True, False,False, True],
            '4':"0011011",#[False,False,True ,True ,False,True ,True ],
            '5':"1111100",#[True ,True ,True ,True ,True ,False,False],
            '6':"0111110",#[False,True ,True ,True ,True ,True ,False],
            '7':"1001011",#[True ,False,False,True ,False,True ,True ],
            '8':"0111101",#[False,True ,True ,True ,True ,False,True ],
            '9':"0101100",#[False,True ,False,True ,True ,False,False],
            'w':"1111111",#[True ,True ,True ,True ,True ,True ,True ],


        }
        input_barray = int(data_soup)
        for key,val in dataparttern.items():
            
            calcu = input_barray - int(val)
            if calcu == 0:
                return key
            # else:
        # print ('error',data_soup)
        return False
                

    def shelf_mode(self):
        '''
        description: 判断左上角状态栏处于哪个状态
        return {*}
        '''        
        checkheight = 6
        modcheck = (self.win.localx(137),self.win.localy(17),1 ,checkheight)
        im = pyautogui.screenshot(region = modcheck)
        # im.save('d:/modline.png')
        vallist = []
        for i in range(checkheight):
            lineval = im.getpixel((0,i))
            vallist.append (sum(lineval))
        # print (vallist)
        cktop = 138<vallist[0] < 142
        ckbottom = 138<vallist[-1] < 142
        ckmid = (vallist[3]==396) or (vallist[3]==734)

        if not ckmid:
            return "close"
        elif ckbottom and not cktop :
            return "normal"
        elif cktop and not ckbottom:
            return "shrink"
        else:
            logger.warn('error at check shelf mod')
            return "error"

    # @basic.timeit
    def red(self,maskcode = '1111'): 
        '''
        description: 新版血查询
        return {*}
        '''               
        logger.debug('查询当前角色血量')
        
        shelfmod = self.shelf_mode()
        if shelfmod == 'normal':
            y_axis = 19
            x_base = 21
            logger.debug('in normal mode')
        elif shelfmod == 'shrink':
            y_axis = 2
            x_base = 21
            logger.debug('in shrink mode')
        else:
            logger.debug('in close mode')
            return -1
        # 判断当前状态栏处在什么状态，并初始化范围数据

        areadict_soup = {
            "ten":(self.win.localx(x_base+13),self.win.localy(y_axis),3 ,7),
            'hundred':(self.win.localx(x_base+6),self.win.localy(y_axis),3 ,7),
            'unit':(self.win.localx(x_base+20),self.win.localy(y_axis),3 ,7),
            'thousand':(self.win.localx(x_base-1),self.win.localy(y_axis),3 ,7),
        }

        areadict = {}
        if maskcode[0] == '1':
            areadict['thousand'] = areadict_soup['thousand']
        if maskcode[1] == '1':
            areadict['hundred'] = areadict_soup['hundred']
        if maskcode[2] == '1':
            areadict['ten'] = areadict_soup['ten']
        if maskcode[3] == '1':
            areadict['unit'] = areadict_soup['unit']
        # 根据掩码，继续优化数据源

        keypoint_list = [0,1,6 ,7,10,17,18]
        # 优化过的查询点信息

        result_dict = {}

        for key,val in areadict.items():
            im = pyautogui.screenshot(region = val)
            width, height = im.size
            pic_datasoup = ''
            # 初始化data_soup
            count = 0
            for wid in range(width):
                for hgt in range(height):
                    if count in keypoint_list:
                        # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                        get_R = im.getpixel((wid,hgt))[0]
                        # im.putpixel((wid,hgt),(255,0,0))
                        # im.save('d:/red%s.png' % key) 
                        if get_R <100:
                            pic_datasoup+='1'
                            # 根据优化过的点数据，获取相应的点颜色
                        else:
                            pic_datasoup+='0'
                    count+=1
            # print (pic_datasoup)
            thenumber = self.number_indentyfy(pic_datasoup)
            # print (thenumber)
            if thenumber == 'e' or isinstance(thenumber,bool):
                result_dict[key] = 0
            elif thenumber.isdigit():
                result_dict[key] = int(thenumber)
            else:
                return -1


        sumval = 0
        for key,val in result_dict.items():
            if key == 'thousand':
                sumval += (val*1000)
            elif key == 'hundred':
                sumval += (val*100)
            elif key == 'ten':
                sumval += (val*10)
            elif key == 'unit':
                sumval += (val*1)
            else:
                pass

        if sumval == 0:
            print (result_dict)
        return sumval


    # @basic.timeit
    def red_max(self,maskcode = '1111'): 
        '''
        description: 新版血查询
        return {*}
        '''               
        logger.debug('查询当前角色血量')
        
        shelfmod = self.shelf_mode()
        if shelfmod == 'normal':
            y_axis = 19
            x_base = 56
            logger.debug('in normal mode')
        elif shelfmod == 'shrink':
            y_axis = 2
            x_base = 56
            logger.debug('in shrink mode')
        else:
            logger.debug('in close mode')
            return -1
        # 判断当前状态栏处在什么状态，并初始化范围数据

        areadict_soup = {
            "ten":(self.win.localx(x_base+13),self.win.localy(y_axis),3 ,7),
            'hundred':(self.win.localx(x_base+6),self.win.localy(y_axis),3 ,7),
            'unit':(self.win.localx(x_base+20),self.win.localy(y_axis),3 ,7),
            'thousand':(self.win.localx(x_base-1),self.win.localy(y_axis),3 ,7),
        }

        areadict = {}
        if maskcode[0] == '1':
            areadict['thousand'] = areadict_soup['thousand']
        if maskcode[1] == '1':
            areadict['hundred'] = areadict_soup['hundred']
        if maskcode[2] == '1':
            areadict['ten'] = areadict_soup['ten']
        if maskcode[3] == '1':
            areadict['unit'] = areadict_soup['unit']
        # 根据掩码，继续优化数据源

        keypoint_list = [0,1,6 ,7,10,17,18]
        # 优化过的查询点信息

        result_dict = {}

        for key,val in areadict.items():
            im = pyautogui.screenshot(region = val)
            # im.save('d:/status_check.png')
            width, height = im.size
            pic_datasoup = ''
            # 初始化data_soup
            count = 0
            for wid in range(width):
                for hgt in range(height):
                    if count in keypoint_list:
                        # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                        get_R = im.getpixel((wid,hgt))[0]
                        # im.putpixel((wid,hgt),(255,0,0))
                        # im.save('d:/redmax%s.png' % key) 
                        if get_R <100:
                            pic_datasoup+='1'
                            # 根据优化过的点数据，获取相应的点颜色
                        else:
                            pic_datasoup+='0'
                    count+=1
            # print (pic_datasoup)
            thenumber = self.number_indentyfy(pic_datasoup)
            # print (thenumber)
            if thenumber == 'e' or isinstance(thenumber,bool):
                result_dict[key] = 0
            elif thenumber.isdigit():
                result_dict[key] = int(thenumber)
            else:
                return -1


        sumval = 0
        # print ('resultdict',result_dict)
        for key,val in result_dict.items():
            if key == 'thousand':
                sumval += (val*1000)
            elif key == 'hundred':
                sumval += (val*100)
            elif key == 'ten':
                sumval += (val*10)
            elif key == 'unit':
                sumval += (val*1)
            else:
                pass

        if sumval == 0:
            print (result_dict)
        return sumval

    # @basic.timeit
    def blue_max(self,maskcode = '1111'):   
        '''
        description: 新版蓝查询
        return {*}
        '''      
        
        logger.debug('查询当前角色魔法量')
        
        shelfmod = self.shelf_mode()
        # print ('shelfmod',shelfmod)
        if shelfmod == 'normal':
            y_axis = 29
            x_base = 56
            logger.debug('in normal mode')
        elif shelfmod == 'shrink':
            y_axis = 12
            x_base = 56
            logger.debug('in shrink mode')
        else:
            logger.debug('in close mode')
            return -1
        # 判断当前状态栏处在什么状态，并初始化范围数据

        areadict_soup = {
            "ten":(self.win.localx(x_base+13),self.win.localy(y_axis),3 ,7),
            'hundred':(self.win.localx(x_base+6),self.win.localy(y_axis),3 ,7),
            'unit':(self.win.localx(x_base+20),self.win.localy(y_axis),3 ,7),
            'thousand':(self.win.localx(x_base-1),self.win.localy(y_axis),3 ,7),
        }

        areadict = {}
        if maskcode[0] == '1':
            areadict['thousand'] = areadict_soup['thousand']
        if maskcode[1] == '1':
            areadict['hundred'] = areadict_soup['hundred']
        if maskcode[2] == '1':
            areadict['ten'] = areadict_soup['ten']
        if maskcode[3] == '1':
            areadict['unit'] = areadict_soup['unit']
        # 根据掩码，继续优化数据源

        keypoint_list = [0,1,6 ,7,10,17,18]
        # 优化过的查询点信息

        result_dict = {}

        for key,val in areadict.items():
            im = pyautogui.screenshot(region = val)
            # im.save('d:/status_check.png')
            width, height = im.size
            pic_datasoup = ''
            # 初始化data_soup
            count = 0
            for wid in range(width):
                for hgt in range(height):
                    if count in keypoint_list:
                        # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                        get_R = im.getpixel((wid,hgt))[0]
                        # im.putpixel((wid,hgt),(255,0,0))
                        # im.save('d:/%s.png' % key) 
                        if get_R <100:
                            pic_datasoup+='1'
                            # 根据优化过的点数据，获取相应的点颜色
                        else:
                            pic_datasoup+='0'
                    count+=1
            # print (pic_datasoup)
            thenumber = self.number_indentyfy(pic_datasoup)
            # print (thenumber)
            if thenumber == 'e' or isinstance(thenumber,bool):
                result_dict[key] = 0
            elif thenumber.isdigit():
                result_dict[key] = int(thenumber)
            else:
                return -1


        sumval = 0
        for key,val in result_dict.items():
            if key == 'thousand':
                sumval += (val*1000)
            elif key == 'hundred':
                sumval += (val*100)
            elif key == 'ten':
                sumval += (val*10)
            elif key == 'unit':
                sumval += (val*1)
            else:
                pass

        if sumval == 0:
            print (result_dict)
        return sumval


    # @basic.timeit
    def blue(self,maskcode = '1111'):   
        '''
        description: 新版蓝查询
        return {*}
        '''      
        
        logger.debug('查询当前角色魔法量')
        
        shelfmod = self.shelf_mode()
        # print ('shelfmod',shelfmod)
        if shelfmod == 'normal':
            y_axis = 29
            x_base = 21
            logger.debug('in normal mode')
        elif shelfmod == 'shrink':
            y_axis = 12
            x_base = 21
            logger.debug('in shrink mode')
        else:
            logger.debug('in close mode')
            return -1
        # 判断当前状态栏处在什么状态，并初始化范围数据

        areadict_soup = {
            "ten":(self.win.localx(x_base+13),self.win.localy(y_axis),3 ,7),
            'hundred':(self.win.localx(x_base+6),self.win.localy(y_axis),3 ,7),
            'unit':(self.win.localx(x_base+20),self.win.localy(y_axis),3 ,7),
            'thousand':(self.win.localx(x_base-1),self.win.localy(y_axis),3 ,7),
        }

        areadict = {}
        if maskcode[0] == '1':
            areadict['thousand'] = areadict_soup['thousand']
        if maskcode[1] == '1':
            areadict['hundred'] = areadict_soup['hundred']
        if maskcode[2] == '1':
            areadict['ten'] = areadict_soup['ten']
        if maskcode[3] == '1':
            areadict['unit'] = areadict_soup['unit']
        # 根据掩码，继续优化数据源

        keypoint_list = [0,1,6 ,7,10,17,18]
        # 优化过的查询点信息

        result_dict = {}

        for key,val in areadict.items():
            im = pyautogui.screenshot(region = val)
            # im.save('d:/status_check.png')
            width, height = im.size
            pic_datasoup = ''
            # 初始化data_soup
            count = 0
            for wid in range(width):
                for hgt in range(height):
                    if count in keypoint_list:
                        # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                        get_R = im.getpixel((wid,hgt))[0]
                        # im.putpixel((wid,hgt),(255,0,0))
                        # im.save('d:/%s.png' % key) 
                        if get_R <100:
                            pic_datasoup+='1'
                            # 根据优化过的点数据，获取相应的点颜色
                        else:
                            pic_datasoup+='0'
                    count+=1
            # print (pic_datasoup)
            thenumber = self.number_indentyfy(pic_datasoup)
            # print (thenumber)
            if thenumber == 'e' or isinstance(thenumber,bool):
                result_dict[key] = 0
            elif thenumber.isdigit():
                result_dict[key] = int(thenumber)
            else:
                return -1


        sumval = 0
        for key,val in result_dict.items():
            if key == 'thousand':
                sumval += (val*1000)
            elif key == 'hundred':
                sumval += (val*100)
            elif key == 'ten':
                sumval += (val*10)
            elif key == 'unit':
                sumval += (val*1)
            else:
                pass

        if sumval == 0:
            print (result_dict)
        return sumval






    def blue_sync(self,clr):
        '''
        description: 根据输入值，判断蓝槽当前是有蓝还是没蓝，或者是空数据
        return {*}
        '''        
        good = (125, 183, 255)
        empty = (0, 0, 1)
        isgood = self.isSimilarColor(clr,good)
        if isgood:
            return 'good'
        else:
            isempty = self.isSimilarColor(clr,empty)
            if isempty:
                return 'empty'
        return 'other'


    def blue_percent(self,im):
        pos_100 = (34,2)
        pos_90 = (31,2)
        pos_75 = (27,2)
        pos_60 = (22,2)
        pos_50 = (19,2)
        pos_40 = (16,2)
        pos_25 = (11,2)
        pos_10 = (6,2)
        pos_0 = (3,2)
        val_50 = im.getpixel(pos_50)

        ans_50 = self.blue_sync(val_50)
        if ans_50 == 'good':
            val_75 = im.getpixel(pos_75)
            ans_75 = self.blue_sync(val_75)
            if ans_75 == 'good':
                val_90 = im.getpixel(pos_90)
                ans_90 = self.blue_sync(val_90)
                if ans_90 == 'good':
                    val_100 = im.getpixel(pos_100)
                    ans_100 = self.blue_sync(val_100)
                    if ans_100 == 'good':
                        return 100
                    else:
                        return 90   
                else:
                    return 75         
            else:
                val_60 = im.getpixel(pos_60)
                ans_60 = self.blue_sync(val_60)
                if ans_60 == 'good':
                    return 60
                else:
                    return 50
        else:            
            val_25 = im.getpixel(pos_25)
            ans_25 = self.blue_sync(val_25)
            if ans_25 == 'good':
                val_40 = im.getpixel(pos_40)
                ans_40 = self.blue_sync(val_40)
                if ans_40 == 'good':
                    return 40
                else:
                    return 25
            else:
                val_10 = im.getpixel(pos_10)
                ans_10 = self.blue_sync(val_10)
                if ans_10 == 'good':
                    return 10
                else:
                    val_0 = im.getpixel(pos_0)
                    ans_0 = self.blue_sync(val_0)
                    if ans_0 == "good":
                        return 0
                    else:
                        return 5


    def red_sync(self,clr):        
        '''
        description: 根据输入值，判断血槽当前是有血还是没血，或者是空数据
        return {*}
        '''        
        good = (255, 0, 0)
        empty = (84, 84, 84)
        isgood = self.isSimilarColor(clr,good)
        if isgood:
            return 'good'
        else:
            isempty = self.isSimilarColor(clr,empty)
            if isempty:
                return 'empty'
        return 'other'


    def red_percent(self,im):
        pos_100 = (34,0)
        pos_90 = (31,0)
        pos_75 = (27,0)
        pos_60 = (22,0)
        pos_50 = (19,0)
        pos_40 = (16,0)
        pos_25 = (11,0)
        pos_10 = (6,0)
        pos_0 = (3,0)
        val_50 = im.getpixel(pos_50)

        ans_50 = self.red_sync(val_50)
        if ans_50 == 'good':
            val_75 = im.getpixel(pos_75)
            ans_75 = self.red_sync(val_75)
            if ans_75 == 'good':
                val_90 = im.getpixel(pos_90)
                ans_90 = self.red_sync(val_90)
                if ans_90 == 'good':
                    val_100 = im.getpixel(pos_100)
                    ans_100 = self.red_sync(val_100)
                    if ans_100 == 'good':
                        return 100
                    else:
                        return 90   
                else:
                    return 75         
            else:
                val_60 = im.getpixel(pos_60)
                ans_60 = self.red_sync(val_60)
                if ans_60 == 'good':
                    return 60
                else:
                    return 50
        else:            
            val_25 = im.getpixel(pos_25)
            ans_25 = self.red_sync(val_25)
            if ans_25 == 'good':
                val_40 = im.getpixel(pos_40)
                ans_40 = self.red_sync(val_40)
                if ans_40 == 'good':
                    return 40
                else:
                    return 25
            else:
                val_10 = im.getpixel(pos_10)
                ans_10 = self.red_sync(val_10)
                if ans_10 == 'good':
                    return 10
                else:
                    val_0 = im.getpixel(pos_0)
                    ans_0 = self.red_sync(val_0)
                    if ans_0 == "good":
                        return 0
                    else:
                        return 5

    def is_need_heel(self,line=51):
        '''
        description: 在战斗animation阶段判断是否有补血的必要
        return {*}
        '''        
        pyautogui.press('home')
        positiondata = self.humanState()
        list_val = []
        for i in positiondata.values():
            if i['ishas']:
                list_val.append(i)
        list_val.sort(key=lambda x:str(x['red']),reverse=True)
        lowest = 100
        for i in list_val:
            keyval = int(i['red'])
            if keyval < lowest:
                lowest = keyval
        print ('当前最低为 ',lowest,'/',line)
        if lowest< line:
            print('激活补血')
            return True
        else:
            print ('没有触发')
            return False





if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    test = battle_query(m)

    ans = test.reset_autobattle_pos()
    print (ans)


