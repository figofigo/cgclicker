#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-04-01 02:18:21
LastEditTime: 2024-06-03 02:08:08
LastEditors: figo - uz
Description: 
copyright: figo software 2020-2021
'''


import os
import random
import sys
sys.path.append('.')
import logging


logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('autobattle_check')
logger.setLevel(logging.INFO)
import time
import json
import pyautogui
import os


class autobattle_check():
    def __init__(self,win):
        super(autobattle_check,self).__init__()
        self.win = win
        self.last_pos = [0,0]
        # 截图检查的时候，确定一下区域
        self.defined_window_pos = [55,self.win.size[1]-150]

    @property
    def pos(self):
        statuspath = '%s\\resources\\autobattle' % self.win.root_path()
        onmark = 'title.png'
        onPath = '%s\\%s' % (statuspath,onmark)
                
        try:
            ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect) 
            self.last_pos = [ans.x,ans.y]
            return ans
        except:
            logger.warn('没有找到自动战斗的窗口')
            self.win.resetMouse()
            self.last_pos = [0,0]
        finally:
            pass
        return None



    def setAutoBattle(self,val,renew=1):
        if isinstance(val,bool):
            check = self.isAutoBattle()

            for i in range(3):
                if check != val:
                    # self.win.resetMouse()
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('z') 
                    pyautogui.keyUp('ctrl') 
                    logger.info('自动战斗设置为 %s' % str(val))
                else:
                    logger.debug('期望设置的状态为[%s]，当前状态为[%s],设置相符，退出。'  % (str(val),str(check)))
                    break
                check = self.isAutoBattle()
            if val is True:    
                self.reset_window_pos()
            if renew:
                if val is True:                     
                    self.reset_count()
        else:
            logger.info('不接受bool值以外的输入，当前输入类型为【%s】' % type(val))


    def isAutoBattle(self):
        '''
        description:这个功能改写成查询自动攻击的状态的字典
        return {dict}  {"status":True/False,"run":True/False,"runover":True/False,"pos":[0,0]}
        '''    
        if self.pos:
            return True
        else:
            return False
        
    def isTaopao(self):
        # print ('self.last_pos',self.last_pos)
        if isinstance(self.last_pos,list):
            if sum(self.last_pos) == 0:
                # 初始化一下数值
                self.pos
        else:
            self.pos
        statuspath = '%s\\resources\\autobattle' % self.win.root_path()
        onmark = 'taopao.png'
        onPath = '%s\\%s' % (statuspath,onmark)
        
        theregion = [self.last_pos[0]-25,self.last_pos[1],50,25]

        
        # im = pyautogui.screenshot(region=theregion)
        # im.save('d:/crossgate/testtaopao.png')
        try:
            ans = pyautogui.locateCenterOnScreen(onPath,region = theregion) 
            return ans
        except:
            logger.warn('没有找到逃跑的标志')
            return False
        finally:
            pass
        return False


    def reset_window_pos(self,f=0):
        '''
        description: 重置自动战斗窗口的位置
        f {int} 控制是否强制打开窗口
        return {*}
        '''        
        targetpos = self.win.localpos(self.defined_window_pos)
        origpos = [self.last_pos[0]+30,self.last_pos[1]]
        if (origpos != self.defined_window_pos):                    
            print ('===xxxx====')
            print ('targetpos',targetpos)
            print ('lastpos',self.last_pos)
        else:
            logger.info('自动战斗窗口的位置正确，不需要移动')
            return True
        # statuspath = '%s\\resources\\autobattle' % self.win.root_path()
        # onmark = 'title.png'
        # onPath = '%s\\%s' % (statuspath,onmark)
        
        for repeat in range(5):
            ans = self.pos
            
            
            if not ans:
                if f:
                    self.win.set_to_top()
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('z') 
                    pyautogui.keyUp('ctrl')
                else:
                    logger.warn('自动战斗窗口没打开，且未开启强制标签，重置功能结束')
                    return False
            else:
                dis = self.win.getDistance(targetpos,[ans[0],ans[1]])
                # print(targetpos,[ans[0]-3,ans[1]-26],dis)
                if dis <1:
                    return True
                else:
                    pyautogui.moveTo(ans[0]+30,ans[1])                
                    pyautogui.dragTo(self.win.localx(self.defined_window_pos[0]+30),self.win.localy(self.defined_window_pos[1]),duration=1,mouseDownUp=True)
        return True

    def number_indentyfy(self,data_soup):
        '''
        description: 根据data_soup分析数字
        return {*}
        '''    
        # keypoint_list = [0,1,6 ,7,10,17,18] 1111001
        dataparttern = {
            'e':"0000000",#x[False,False,False,False,False,False,False],
            '0':"0111000",#x[False,True ,True ,True ,True ,False,False],
            '1':"0000011",#[False,True ,False,False,False,True ,True ],
            '2':"0111001",#x[False,True ,False,True ,False,False,True ],
            '3':"0111010",#x[False,True, True, True, False,False, True],
            '4':"0000100",#x[False,False,True ,True ,False,True ,True ],
            '5':"1111110",#x[True ,True ,True ,True ,True ,False,False],
            '6':"0010010",#x[False,True ,True ,True ,True ,True ,False],
            '7':"0001001",#x[True ,False,False,True ,False,True ,True ],
            '8':"0111110",#x[False,True ,True ,True ,True ,False,True ],
            '9':"0111001",#x[False,True ,False,True ,True ,False,False],
            'w':"1111111",#x[True ,True ,True ,True ,True ,True ,True ],


        }
        input_barray = int(data_soup)
        for key,val in dataparttern.items():
            
            calcu = input_barray - int(val)
            if calcu == 0:
                return key
            # else:
        # print ('error',data_soup)
        return False
                

    def reset_count(self):
        ans = self.pos
        if not ans:
            return None
        x_base = ans[0] + (int(random.random()*10))
        y_axis = ans[1] + 120

        pyautogui.click(x_base,y_axis,duration=0.1)

    def count(self,maskcode = '111'):   
        '''
        description: 新版蓝查询
        return {*}
        '''      
        
        logger.debug('查询当前角色魔法量')
        ans = self.pos
        # print('ssssself.pos',self.pos)
        if not ans:
            return -1
        # print (ans)
        x_base = ans[0] - 24 + 4
        y_axis = ans[1] + 39 + 26


        areadict_soup = {
            "ten":((x_base+5),(y_axis),3 ,7),
            'hundred':((x_base-1),(y_axis),3 ,7),
            'unit':((x_base+11),(y_axis),3 ,7),
            }

        areadict = {}
        if maskcode[0] == '1':
            areadict['hundred'] = areadict_soup['hundred']
        if maskcode[1] == '1':
            areadict['ten'] = areadict_soup['ten']
        if maskcode[2] == '1':
            areadict['unit'] = areadict_soup['unit']
        # 根据掩码，继续优化数据源

        keypoint_list = [0,1,6 ,7,10,17,18]
        # 优化过的查询点信息

        result_dict = {}

        for key,val in areadict.items():
            im = pyautogui.screenshot(region = val)
            # im.save('d:/crossgate/auto_count_%s.png' % key)
            width, height = im.size
            pic_datasoup = ''
            # 初始化data_soup
            count = 0
            for wid in range(width):
                for hgt in range(height):
                    if count in keypoint_list:
                        # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                        get_R = im.getpixel((wid,hgt))[0]
                        # print ('get_R',get_R)
                        # im.putpixel((wid,hgt),(255,0,0))
                        # im.save('d:/data/auto%s.png' % key) 
                        if get_R <150:
                            pic_datasoup+='1'
                            # 根据优化过的点数据，获取相应的点颜色
                        else:
                            pic_datasoup+='0'
                    count+=1
            # print (key,pic_datasoup)
            thenumber = self.number_indentyfy(pic_datasoup)
            # print (thenumber)
            if thenumber == 'e' or isinstance(thenumber,bool):
                result_dict[key] = 0
            elif thenumber.isdigit():
                result_dict[key] = int(thenumber)
            else:
                return -1


        sumval = 0
        for key,val in result_dict.items():
            if key == 'hundred':
                sumval += (val*100)
            elif key == 'ten':
                sumval += (val*10)
            elif key == 'unit':
                sumval += (val*1)
            else:
                pass

        if sumval == 0:
            print (result_dict)
        return sumval

    @property
    def policy(self):
        ans = self.pos
        if not ans:
            return None
        # print (ans)
        x_base = ans[0] - 24
        y_axis = ans[1] + 21
        val = (self.win.localx(x_base),self.win.localy(y_axis),23 ,1)
        im = pyautogui.screenshot(region = val)
        # im.save('d:/data/testpolicy.png')
        width, height = im.size
        pic_datasoup = ''
        # 初始化data_soup
        keypoint_list = [2,4,5,13]
        count = 0
        for wid in range(width):
            if wid in keypoint_list:
                # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                get_R = im.getpixel((wid,0))[0]
                if get_R <150:
                    pic_datasoup+='1'
                    # 根据优化过的点数据，获取相应的点颜色
                else:
                    pic_datasoup+='0'
        # print (pic_datasoup)
        indentify = {'xuesh':'0100',
         'zhumu':'0011',
         'duomu':'0111',
         'suiji':'1110'
         }
        for key,val in indentify.items():
            if pic_datasoup == val:
                return key
        return None
    
    def set_next_policy(self,clicks=1):
        ans = self.pos
        if not ans:
            return None
        # print (ans)
        x_base = ans[0] + 55
        y_axis = ans[1] + 50
        pyautogui.click(x_base,y_axis,duration=0.1,clicks=clicks,interval=0.5)
    
    def set_prev_policy(self,clicks=1):
        ans = self.pos
        if not ans:
            return None
        # print (ans)
        x_base = ans[0] + 45
        y_axis = ans[1] + 50
        pyautogui.click(x_base,y_axis,duration=0.1,clicks=clicks,interval=0.5)

    def is_autowalk(self):
        ans = self.last_pos
        pyautogui.moveTo([self.last_pos[0]+30,self.last_pos[1]])
        if not ans:
            return None
        # print (ans)
        x_base = ans[0] + 69
        y_axis = ans[1] + 98

        val = (x_base,y_axis,2,1)
        im = pyautogui.screenshot(region = val)
        # im.save('d:/data/ppp.png')
        get_R = im.getpixel((0,0))[0]
        if (get_R)<200:
            return False
        else: 
            return True
        
    def set_autowalk(self,status):
        ans = self.last_pos
        if not ans:
            return None
        x_base = ans[0] + 65 + int(random.random()*3)
        y_axis = ans[1] + 100
        for i in range(3):
            if status != self.is_autowalk():
                pyautogui.click(x_base,y_axis,duration=0.1)
                pyautogui.moveRel(xOffset=30,yOffset=0)
            else:
                break

        return True

    def serialize(self):
        rtData = {'enabled':False}
        if self.pos:
            rtData['enabled'] = True
        else:
            return rtData
        
        rtData['pos'] = self.pos
        rtData['policy'] = self.policy
        rtData['count'] = self.count()

        return rtData