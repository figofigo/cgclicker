#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-29 05:58:29
LastEditTime: 2024-06-04 23:26:13
LastEditors: figo - uz
Description: 基于地图窗口坐标的位置查询系统
inhert: basic.basic
copyright: figo software 2020-2021
'''
import random
import os
import sys
sys.path.append('.')
import time
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('whereAmI')
logger.setLevel(logging.DEBUG)

import pyautogui

class whereAmI():
    def __init__(self,win) :
        super(whereAmI,self).__init__()
        self.win = win

        rootPath = self.win.root_path()
        self.sourcepath = '%s\\resources\\where' % rootPath
        self.numberpath = '%s\\resources\\where\\number' % rootPath
        self.numberDict = self.initNumber()
        self.logoutpath = '%s\\resources\\logout' % rootPath

        self.last_pos = [0,0]
        

        self.num_wid = 5
        self.num_hgt = 8

    def open_sys_window(self,withclick=True):
        logger.debug('打开系统设定的窗口')
        # mark = 'logout.png'
        # if self.win.size[-1] < 500:
        mark = 'logout800.png'
        Path = '%s\\%s' % (self.logoutpath,mark)

        try:
            ans = pyautogui.locateCenterOnScreen(Path,region = self.win.rect)
            if ans:
                logger.debug ('系统设定的窗口找到了')   
                resetpos = [ans.x+40,ans.y]
                pyautogui.moveTo(resetpos,duration=0.2)
                return ans
            else:                    
                logger.debug('系统设定的窗口 - None')
        except:
            logger.debug('系统设定的窗口 - Error')
        
        self.win.set_to_top()
        xitongpos = [self.win.localx(520),self.win.pos[1] + self.win.size[1]+10 ]
        
        clickpos = self.win.wrangle_pos(xitongpos,xoffset = 3,yoffset=5)
        logger.debug('移动到右下角')
        pyautogui.moveTo(clickpos,duration=0.2)
        pyautogui.click(clickpos,clicks=1,duration=0.3)
        time.sleep(0.5)
        return False
    
    
    def out(self):
        ans = self.open_sys_window()
        if ans:
            time.sleep(2)
            pyautogui.click(self.px-150,self.py+15,duration=0.1,clicks=1,interval=0.1)
            # now = datetime.now() # current date and time
            date_time = time.strftime("%Y-%m-%d, %H:%M:%S",time.localtime())
            print("date and time:",date_time)

    def back(self):
        mark = 'huicheng.png'
        Path = '%s\\%s' % (self.logoutpath,mark)
        syswinpos = [0,0]
        for win_sys_loop in range(10):
            syswinpos = self.open_sys_window()
            if syswinpos:break
        
        if syswinpos:
            for logout_loop in range(3):
                try:
                    logout_ans = pyautogui.locateCenterOnScreen(Path,region = self.win.rect)
                    if logout_ans:
                        logger.debug ('点击登出') 
                        pyautogui.moveTo(logout_ans,duration=0.2)
                        pyautogui.click(logout_ans,duration=0.1,clicks=2,interval=0.1)
                        break
                    else:
                        logger.debug('第一页展开失败')
                except:
                    logger.debug('第一页展开出错')

                pos_x = syswinpos[0] 
                pos_y = syswinpos[1]+ 30
                pyautogui.click(pos_x,pos_y,duration=0.2)
                time.sleep(1)


        else:
            logger.warn('重复了10次无法拿到系统窗口坐标')
            return False

        for i in range(100):
            # print(thewin.status)
            if self.win.status == 'peace':
                logger.info('回城完成')
                return True
            else:
                time.sleep(0.2)
        return False

    def searchFunction(self,picname):
        mark = '%s.png' % picname
        Path = '%s\\%s' % (self.sourcepath,mark)
        for i in range(10):
            isblack = self.win.status
            if isblack == 'loading':
                logger.debug('loading %i / 5' % i)
                time.sleep(0.1)
            else:
                break
        try:
            ans = pyautogui.locateCenterOnScreen(Path,region = self.win.rect)
        except:
            ans = None
        finally:
            pass
        
        if ans:
            logger.info('现在在[%s]' % picname)
            return True
        else:
            logger.debug('无法获取到地图信息')
            return False

    
    def isHuilang(self):
        onmark = 'huilang'
        ans = self.searchFunction(onmark)
        return ans
    
    def isZhaohuanzhijian(self):
        onmark = 'zhaohuanzhijian'
        ans = self.searchFunction(onmark)
        return ans

    def isMingrentang(self):
        onmark = 'mingrentang'
        ans = self.searchFunction(onmark)
        return ans
    
    def isCunzhangjia(self):
        
        onmark = 'cunzhangjia'
        ans = self.searchFunction(onmark)
        return ans


    def isQichengzhijian(self):
        onmark = 'qichengzhijian'
        ans = self.searchFunction(onmark)
        return ans

    def isLixieliyabao(self):
        onmark = 'lixieliyabao'
        ans = self.searchFunction(onmark)
        return ans

    def isXuanwu20(self):
        
        onmark = 'xuanwu20'
        ans = self.searchFunction(onmark)
        return ans

    def isXuanwuzhijing(self):
        
        onmark = 'xuanwuzhijing'
        ans = self.searchFunction(onmark)
        return ans



    def isHospital(self):
        onmark = 'yiyuan'
        ans = self.searchFunction(onmark)
        return ans

    def isShalianna(self):
        
        onmark = 'shalianna'
        ans = self.searchFunction(onmark)
        return ans

    def isJienuowa(self):
        
        onmark = 'jienuowa'
        ans = self.searchFunction(onmark)
        return ans

    
    def isMatata(self):
        
        onmark = 'matatapingyuan'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isHuanzhididiyiji(self):
        
        onmark = 'huanzhididiyiji'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isHuodong(self):
        
        onmark = 'huodong'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isKulukesi(self):
        
        onmark = 'kulukesi'
        
        ans = self.searchFunction(onmark)
        return ans

    def isShushuleboss(self):
        onmark = 'shushule_boss'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isShushule5(self):
        onmark = 'shushule_5'
        
        ans = self.searchFunction(onmark)
        return ans

    
    def isLeiketaer(self):
        
        onmark = 'leiketaer'
        
        ans = self.searchFunction(onmark)
        return ans

    def isXierwei(self):
        
        onmark = 'xierweicun'
        ans = self.searchFunction(onmark)
        return ans

    # def saveMark(self,bbox_orig,saveDir):
    #     '''
    #     description: 将指定的坐标信息存成图片的功能
    #     return {*}
    #     '''        
    #     if not os.path.exists(saveDir):
    #         os.makedirs(saveDir)
    #     saveFile = saveDir + '\\' + str(time.time()) + '.png'

    #     real_region = [ self.localx(bbox_orig[0]),
    #                     self.localy(bbox_orig[1]),
    #                     self.localx(bbox_orig[2]),
    #                     self.localy(bbox_orig[3])]
    #     im = pyautogui.screenshot(real_region)
    #     im.save(saveFile)
    #     return saveFile

    def initNumber(self):
        '''
        description: 将数字装载到字典中
        return {*}
        '''        
        rtList = {}
        filelist = os.listdir(self.numberpath)
        for i in filelist:
            key = i.split('.')[0]
            fullpath = '%s\\%s' % (self.numberpath,i)
            if os.path.isfile(fullpath):
                rtList[key] = fullpath
        return rtList

    def reset_mapwindow_pos(self,q=0):
        '''
        description: 将地图窗口移动到指定位置
        return {*}
        '''     
        self.set_map(True)

        tx=self.win.localx(self.win.size[0]-100)
        ty=self.win.localy(35)

        
        dragoffset_x = 20

        isSuccess = False
        for i in range(5):
            logger.info('moveloop[%i] ' % i)
            baseinfo = self.pos
            if not baseinfo:
                time.sleep(2)
                continue
            sx = baseinfo[0]
            sy = baseinfo[1]

            offset_x = tx-sx
            offset_y = ty-sy

            if (offset_x + offset_y + dragoffset_x) == 0:
                logger.info('done')
                isSuccess = True
                break
            else:
                logger.info('offset [%i,%i]' % (offset_x +dragoffset_x,offset_y))
                pyautogui.moveTo(sx+dragoffset_x,sy,duration=0.2)
                pyautogui.dragRel(offset_x+dragoffset_x,offset_y,duration=1)
        return isSuccess


    def set_map(self,val):
        
        if not isinstance(val,bool):
            return False
        
        self.win.set_to_top()
        # self.win.resetMouse([10,10])
        # pyautogui.click()
        # 强制前台

        ans = self.pos
        if val == bool(ans):
            return True
        else:
            pyautogui.keyDown('ctrl') 
            pyautogui.press('s') 
            pyautogui.keyUp('ctrl')



    def get_number(self,offsetx):
        '''
        description: 查x轴百位
        return {*}
        '''     
        # print ('self.last_pos',self.last_pos)
        # print ('self.pos',self.pos)
        isneedrepos = False
        if isinstance(self.last_pos,list):
            if sum(self.last_pos) == 0:
                isneedrepos = True
        else:
            isneedrepos = True
        if isneedrepos:
            if (isinstance(self.pos,bool)):
                logger.warning ('地图位置获取失败，请确认是否已经打开了地图')
                return -1


        tgx = (self.last_pos[0] + offsetx)
        tgy = (self.last_pos[1] + 15)

        

        search_region = (tgx,tgy,self.num_wid,self.num_hgt)
        # print (search_region)

        im = pyautogui.screenshot(region=search_region)

        # im.save('d:/crossgate/testnumber%s.png' % str(offsetx))
        # print (99999999999999999999999999)
        # raise

        firstLoop = {'check1':(1,1),'check2':(4,1),'check3':(1,4)}
        nextLoop = {}

        firstData = {}
        for key,val in firstLoop.items():
            temp = im.getpixel(val)
            firstData[key] = int(sum(temp) > 150)
        # 这一波可以判断出来6,1,9,[4,5],[0.2.3.7.8]

        
        checkStr = '%(check1)i%(check2)i%(check3)i' % firstData

        if checkStr == '111':
            return 6
        elif checkStr == '011':
            return 9
        elif checkStr == '100':
            return 1
        elif checkStr == '000':
            temp = im.getpixel((2,3))
            ans = (sum(temp) > 150)
            if ans:
                return 5
            else:
                temp2 = im.getpixel((1,3))
                ans2 = (sum(temp2) > 150)
                if ans2:
                    return 4
                else:
                    return 0
        else:
            nextLoop = {'check1':(0,2),'check2':(0,3),'check3':(2,3)}

            nextData = {}
            for key,val in nextLoop.items():
                temp = im.getpixel(val)
                nextData[key] = int(sum(temp) > 150)


            nextStr = '%(check1)i%(check2)i%(check3)i' % nextData

            if nextStr == '000':
                return 7
            elif nextStr == '100':
                return 2
            elif nextStr == '101':
                return 8
            elif nextStr == '110':
                return 0
            elif nextStr == '001':
                return 3
            else:
                # 空白为，没有数字
                return 0

            
        # 如果在循环里没找到，就是0
        return 0



    def get_pos(self,direct,testtime=False):
        '''
        description: 获取坐标的功能
        direct {str} 只接受x与y
        return {int}
        '''        

        if direct == 'x':
            offsets = {'ones':-43,'tens':-49,'hundreds':-55}
        else:
            offsets = {'ones':65,'tens':59,'hundreds':53}

        ons = self.get_number(offsets['ones'])
        # print ('ones',ons)
        if ons>=0:
            tens = self.get_number(offsets['tens'])
            # print ('tens',tens)
        else:
            return -1
        
        if ons>=0 and tens>=0:
            hundreds = self.get_number(offsets['hundreds'])
            # print ('hundreds',hundreds)
        else:
            return -1


        value = ons+(tens*10) + (hundreds*100)

        return value

    @property
    def pos(self):
        '''
        description: 地图模块的坐标
        return {*}
        '''   
        onmark = 'ditu.png'
        onPath = '%s\\%s' % (self.sourcepath,onmark)

        try:
            ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect)
            self.last_pos = [ans.x,ans.y]
        except:
            self.last_pos = [0,0]
            return False
        finally:
            pass
        return ans

    def coord(self):
        # self.get_pos('x')
        try:
            thex = self.get_pos('x')
        except:
            thex = -1

        try:       
            they = self.get_pos('y')
        except:
            they = -1
        returnval = [thex,they]
        if sum(returnval) < 0:
            return None
        else:
            return [thex,they]
    
    def serialize(self):
        coord = self.coord()
        print (coord)
        print (self.isHospital())

if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    print (m)
    whr = whereAmI(m)
    # whr.logNumber()
    gets = whr.pos()
    
    # gets = whr.pos()
    print (gets)