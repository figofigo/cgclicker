#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-04-20 23:22:20
LastEditTime: 2024-06-17 00:38:11
LastEditors: figo - uz
Description: 
copyright: figo software 2020-2021
'''

import os
import random
import time

import PIL
import pyautogui
import logging

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('cast')
logger.setLevel(logging.INFO)

class cast():
    def __init__(self,win):
        super(cast,self).__init__()
        self.win = win
        self.skillSlotHeight = 16
        filePath = os.path.abspath(__file__).replace('util','')
        rootPath = os.path.dirname(filePath)
        self.sourcepath = '%s\\resources\\battle' % rootPath 

    def pixelSolver(self,center,val = '',region = [10,1],istest=True,targetColor=[255,255,255]):
        if istest:
            region = [10,10]
        bbox_orig = (center[0]-region[0],center[1]-region[1],center[0]+region[0],center[1]+region[1])
        im = None
        im = PIL.ImageGrab.grab(bbox_orig)
        if istest:
            strtime = str(time.time())
            # im.save("C:\\gitlab\\cgclicker\\resources\\test\\"+strtime + '.png')
        for x in range(region[0]*2):
            for y in range(region[1]*2):                
                getcolor = im.getpixel((x,y))
                #print (getcolor)
                if sum(getcolor)>= 765:
                    return True
        return False
    
    def mosterSlot(self,q=1):
        '''
        description: 详细的获取怪物信息，可以在读秒外使用
        return {*}
        '''        
        pass

    def numMonster(self):
        data = self.mosterState()
        count = 0
        for i in range(10):
            if data[i]['ishas']:
                count += 1
        return count

    def mosterState(self,isdisplay=True):
        '''
        在读秒的时候使用
        粗略的获取怪物的信息，只有位置和有没有怪
        description: 从左到右，从下到上，1到10
        return {*}
        '''
        logger.info('获取魔物的站位情况')

        if self.win.size[0] > 700:
            slots = {
                1:[108,390],
                2:[188,345],
                3:[268,300],
                4:[346,255],
                5:[426,210],
                6:[33,328],
                7:[113,283],
                8:[193,238],
                9:[273,193],
                10:[351,146]        
            }
        else:
            slots = {
                1:[89,315],
                2:[153,279],
                3:[217,243],
                4:[280,207],
                5:[344,171],
                6:[29,265],
                7:[93,229],
                8:[157,193],
                9:[221,157],
                10:[284,120]        
            }

        dataDict = {}
        
        for i in range(10):
            id= i+1
            # print ('pos',id)
            realpos_x = self.win.localx(slots[id][0]) #- 15
            realpos_y = self.win.localy(slots[id][1]) -26 - 7
            
            checklength = 20
            checkheight = 1
            im = pyautogui.screenshot(region=[realpos_x,realpos_y,checklength,checkheight])
            if False:
                im.save('d:/crossgate/monster_status_%i.png'%i)

            indenty_dict = {'white':0,'black':0,'other':0}
            check_color_list = []
            for check_id in range(checklength):

                check_clr = im.getpixel((check_id,0))
                check_color_list.append(check_clr) # 收集查询数据
                # print('check_clr',check_clr)
                check_white = self.win.isSimilarColor(check_clr,[255,255,255],limit=2)                    
                check_black = self.win.isSimilarColor(check_clr,[0,0,0])
                if check_white:
                    indenty_dict['white'] +=1
                elif check_black:
                    indenty_dict['black'] +=1
                else:                        
                    indenty_dict['other'] +=1
            #print(check_color_list)
            #print(list(set(check_color_list)))
            rough_measure = float(len(list(set(check_color_list)))) / float(len(check_color_list))
            # print ('rough_measure',rough_measure)
            if rough_measure>0.55:
                gets = True
            else:
                # 粗估一下，如果重复颜色过多，比率会低于0.55
                # 即，其中有一半颜色是底色，此处大概不会有生物存在
                gets = False
            if indenty_dict['white']>0 and indenty_dict['black']>0:
                gets = True
            elif indenty_dict['white']>1:
                gets = True
            else:
                pass
                #print ('indenti dict',indenty_dict)
            



            lineName = "back"
            if id < 6:
                lineName = "front"
            if gets:
                dataDict[i+1] = {'ishas':True,'pos':[realpos_x,realpos_y],'id':id,'line':lineName}
            else:
                dataDict[i+1] = {'ishas':False,'pos':None,'id':id,'line':lineName}


        if isdisplay:
            # 以更直观的方式打印出来识别信息
            self.display_moster_status(dataDict)

        return dataDict
    
    def display_moster_status(self,data):
        display_data = {}
        for key,val in data.items():
            newkey = 'slot'+str(key)
            dis_str = '[无] --- '
            if val['ishas']:
                dis_str = '存活 --- '

            display_data[newkey] = dis_str

        line1 = '= 6 ====== 7 ====== 8 ====== 9 ====== 10 ==='
        line2 = '%(slot6)s%(slot7)s%(slot8)s%(slot9)s%(slot10)s' % display_data
        line3 = '= 1 ====== 2 ====== 3 ====== 4 ====== 5 ===='
        line4 = '%(slot1)s%(slot2)s%(slot3)s%(slot4)s%(slot5)s' % display_data
        print ('windows size:',self.win.size[0])
        print(line1)
        print(line2)
        print(line3)
        print(line4)

    # def isSimilarColor(self,input_color,regular_color,limit = 10):
    #     # print ('input',input_color)
    #     # print ('regular_color',regular_color)
    #     for i in range(len(input_color)):
    #         if input_color[i] not in range(regular_color[i]-limit,regular_color[i]+limit):
    #             return False
    #     return True

    def blue_sync(self,clr):
        '''
        description: 根据输入值，判断蓝槽当前是有蓝还是没蓝，或者是空数据
        return {*}
        '''        
        good = (125, 183, 255)
        empty = (0, 0, 1)
        isgood = self.win.isSimilarColor(clr,good)
        if isgood:
            return 'good'
        else:
            isempty = self.win.isSimilarColor(clr,empty)
            if isempty:
                return 'empty'
        return 'other'


    def blue_percent(self,im):
        pos_100 = (30,3)
        pos_90 = (27,3)
        pos_75 = (22,3)
        pos_60 = (18,3)
        pos_50 = (15,3)
        pos_40 = (12,3)
        pos_25 = (7,3)
        pos_10 = (3,3)
        pos_0 = (1,3)
        val_50 = im.getpixel(pos_50)

        ans_50 = self.blue_sync(val_50)
        if ans_50 == 'good':
            val_75 = im.getpixel(pos_75)
            ans_75 = self.blue_sync(val_75)
            if ans_75 == 'good':
                val_90 = im.getpixel(pos_90)
                ans_90 = self.blue_sync(val_90)
                if ans_90 == 'good':
                    val_100 = im.getpixel(pos_100)
                    ans_100 = self.blue_sync(val_100)
                    if ans_100 == 'good':
                        return 100
                    else:
                        return 90   
                else:
                    return 75         
            else:
                val_60 = im.getpixel(pos_60)
                ans_60 = self.blue_sync(val_60)
                if ans_60 == 'good':
                    return 60
                else:
                    return 50
        else:            
            val_25 = im.getpixel(pos_25)
            ans_25 = self.blue_sync(val_25)
            if ans_25 == 'good':
                val_40 = im.getpixel(pos_40)
                ans_40 = self.blue_sync(val_40)
                if ans_40 == 'good':
                    return 40
                else:
                    return 25
            else:
                val_10 = im.getpixel(pos_10)
                ans_10 = self.blue_sync(val_10)
                if ans_10 == 'good':
                    return 10
                else:
                    val_0 = im.getpixel(pos_0)
                    ans_0 = self.blue_sync(val_0)
                    if ans_0 == "good":
                        return 0
                    else:
                        return 5


    def red_sync(self,clr):        
        '''
        description: 根据输入值，判断血槽当前是有血还是没血，或者是空数据
        return {*}
        '''        
        good = (255, 0, 0)
        empty = (84, 84, 84)
        isgood = self.win.isSimilarColor(clr,good)
        if isgood:
            return 'good'
        else:
            isempty = self.win.isSimilarColor(clr,empty)
            if isempty:
                return 'empty'
        return 'other'


    def red_percent(self,im):
        pos_100 = (30,1)
        pos_90 = (27,1)
        pos_75 = (22,1)
        pos_60 = (18,1)
        pos_50 = (15,1)
        pos_40 = (12,1)
        pos_25 = (7,1)
        pos_10 = (3,1)
        pos_0 = (1,1)
        val_50 = im.getpixel(pos_50)

        ans_50 = self.red_sync(val_50)
        if ans_50 == 'good':
            val_75 = im.getpixel(pos_75)
            ans_75 = self.red_sync(val_75)
            if ans_75 == 'good':
                val_90 = im.getpixel(pos_90)
                ans_90 = self.red_sync(val_90)
                if ans_90 == 'good':
                    val_100 = im.getpixel(pos_100)
                    ans_100 = self.red_sync(val_100)
                    if ans_100 == 'good':
                        return 100
                    else:
                        return 90   
                else:
                    return 75         
            else:
                val_60 = im.getpixel(pos_60)
                ans_60 = self.red_sync(val_60)
                if ans_60 == 'good':
                    return 60
                else:
                    return 50
        else:            
            val_25 = im.getpixel(pos_25)
            ans_25 = self.red_sync(val_25)
            if ans_25 == 'good':
                val_40 = im.getpixel(pos_40)
                ans_40 = self.red_sync(val_40)
                if ans_40 == 'good':
                    return 40
                else:
                    return 25
            else:
                val_10 = im.getpixel(pos_10)
                ans_10 = self.red_sync(val_10)
                if ans_10 == 'good':
                    return 10
                else:
                    val_0 = im.getpixel(pos_0)
                    ans_0 = self.red_sync(val_0)
                    if ans_0 == "good":
                        return 1
                    else:
                        return 5

    def is_need_heel(self,line=51):
        '''
        description: 在战斗animation阶段判断是否有补血的必要
        return {*}
        '''        
        pyautogui.press('home')
        positiondata = self.humanState()
        list_val = []
        for i in positiondata.values():
            if i['ishas']:
                list_val.append(i)
        list_val.sort(key=lambda x:str(x['red']),reverse=True)
        lowest = 100
        for i in list_val:
            keyval = int(i['red'])
            if keyval < lowest:
                lowest = keyval
        print ('当前最低为 ',lowest,'/',line)
        if lowest< line:
            print('激活补血')
            return True
        else:
            print ('没有触发')
            return False

    def humanState(self,checkred=True,checkblue=True,isDisplay=True):

        key_color = [
            (255, 140, 140),
            (255, 0, 0),
            (105, 86, 37)
            ]

        if self.win.size[0] > 700:
            slots = {
                1:[433,523],  #416,565
                2:[514,479],
                3:[595,435], # ok
                4:[676,391],
                5:[758,348],
                6:[358,466], # 341,506
                7:[438,421], # 421 462
                8:[518,376],
                9:[596,330],
                10:[676,285]        
            }
        else:
            slots = {
                1:[345,415],
                2:[410,380],
                3:[475,345],
                4:[540,310],
                5:[605,275],
                6:[285,370],
                7:[349,334],
                8:[413,298],
                9:[476,261],
                10:[540,225]        
            }
        dataDict_temp = {}
        length = 31
        # height = 3 +47
        height = 5
        for i in range(10):
            isvalid = False
            # 判断是否是有效位置的检查项
            id= i+1
            
            realpos_x = self.win.localx(slots[id][0]) -17
            realpos_y = self.win.localy(slots[id][1]) + 16#17 # -25

            bbox_orig = (realpos_x,realpos_y,length,height)
            im = pyautogui.screenshot(region=bbox_orig)
            if False:
                im.save('d:/crossgate/humanstatus_%i.png' % i)
            get_list = []
            for px,clr in enumerate(key_color):
                get_clr = im.getpixel((0,px))
                # print (get_clr)
                gets = self.win.isSimilarColor(clr,get_clr)
                get_list.append(gets)
            uni_get_list = list(set(get_list))
            if len(uni_get_list) == 1 and uni_get_list[0] == True:
                isvalid = True
            
            red_pct = '__'
            blue_pct = '__'

            if isvalid:
                if checkred:
                    red_pct = str(self.red_percent(im)).zfill(2)
                else:
                    red_pct = '__'
                if checkblue:
                    blue_pct = str(self.blue_percent(im)).zfill(2)
                else:
                    blue_pct = '__'

            dataDict_temp[str(i+1)] = [isvalid,red_pct,blue_pct]
        if isDisplay:
            self.display_human_status(dataDict_temp)

        dataDict = {}
        for i in range(10):
            id= i+1
            #print ('pos',id)
            realpos_x = self.win.localx(slots[id][0]) 
            realpos_y = self.win.localy(slots[id][1])
            current_slot = dataDict_temp[str(id)]
            dataDict[id] = {'ishas':current_slot[0],'pos':[realpos_x,realpos_y],'id':id,'red':current_slot[1],'blue':current_slot[2]}

        return dataDict

    def display_human_status(self,data):
        display_data = {}
        for key,val in data.items():
            newkey = 'slot'+key
            dis_str = 'None --- '
            if val[0]:
                dis_str = '%s/%s' % (val[1],val[2])
                if len(dis_str) == 5:
                    dis_str = dis_str + ' -- '
                else:
                    dis_str = dis_str + ' - '

            display_data[newkey] = dis_str

        line1 = '= 6 ====== 7 ====== 8 ====== 9 ====== 10 ==='
        line2 = '%(slot6)s%(slot7)s%(slot8)s%(slot9)s%(slot10)s' % display_data
        line3 = '= 1 ====== 2 ====== 3 ====== 4 ====== 5 ===='
        line4 = '%(slot1)s%(slot2)s%(slot3)s%(slot4)s%(slot5)s' % display_data
        print ('windows size:',self.win.size[0])
        print(line1)
        print(line2)
        print(line3)
        print(line4)
    


    def isBattle(self):
        '''
        description: 快速查询是否在战斗状态
        return {*}
        '''        
        check = self.win.status
        if check != 'peace':
            return True
        else:
            return False




    def attack(self):
        if self.win.size[0]> 700:
            pos = [550,30] 
        else:
            pos = [390,30]
        offsetx = int(random.random()*10)
        localx = self.win.localx(pos[0]) + offsetx
        localy = self.win.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        pyautogui.click()

        positiondata  = self.mosterState()
        list_val = []
        for i in positiondata.values():
            if i['ishas']:
                list_val.append(i)
        rdseed = random.random()
        if rdseed >= 0.5:
            list_val.sort(key=lambda x:str(x['id']),reverse=True)

        for tg in list_val:
            x = tg['pos'][0]
            y = tg['pos'][1]
            pyautogui.moveTo(x,y)
            pyautogui.click()
        return True
    
    def escape(self):    
        if self.win.size[0]> 700:
            pos = [750,50]
        else:
            pos = [590,50]
        offsetx = int(random.random()*10)
        localx = self.win.localx(pos[0]) + offsetx
        localy = self.win.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        pyautogui.click()

    def guard(self): 
        if self.win.size[0]> 700:
            pos = [550,50] 
        else:
            pos = [390,50]
        offsetx = int(random.random()*10)
        localx = self.win.localx(pos[0]) + offsetx
        localy = self.win.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        pyautogui.click()


    def action(self,target={'human':[1,'enermy'],'pet':[1,'enermy']},repeat=5,isCloseCombat=False):
        # self.placeCursor('center')
        self.win.resetMouse()
        self.win.set_to_top()
        # 聚焦到当前窗口
        petok = False
        humanok = False
        for i in range(repeat):

            step_check = self.win.status
            if step_check == 'pet':
                
                check_pet = self.petCastSkill(target['pet'][0],target=target['pet'][1],repeat=repeat)
                if check_pet:
                    petok = True
                    logger.info('pet cast success')
            elif step_check == 'battle_sel':
                check = self.humanCastSkill(target['human'][0],target=target['human'][1])
                if check:
                    humanok = True
                    logger.info('human cast success')
            else:
                if humanok and petok:
                    return True
            
        return False


    def isPetSkillWindow(self):
        self.win.resetMouse()
        onmark = 'pet_skill_start.png'
        onPath = '%s\\%s' % (self.sourcepath,onmark)

        # for i in marks
        ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect)

        if ans:
            logger.info('宠物技能面板已经打开了')
            return True
        else:
            logger.info('宠物技能面板')
            return False

    def isPetSkillSelected(self):
        '''
        description: 查询当前宠物技能窗口是否打开的状态
        return {*}
        '''

        win_width = self.win.rect[2]
        x = (self.win.rect[0]) + win_width-280
        y = (self.win.rect[1]) + 50 +3 +25
        
        length = 20
        dataArea = (x,y,length,1+10)
        
        im = pyautogui.screenshot(region=dataArea)
        # im.save('d:/crossgate/ispetskillselected.png')
        clr1 = (250, 247, 237)
        clr2 = (0,0,0)
        mark1 = False
        mark2 = False
        for i in range(length):
            pt1 = im.getpixel((i,0))
            if not mark1:
                mark1 = self.win.isSimilarColor(pt1,clr1)
            if not mark2:
                mark2 = self.win.isSimilarColor(pt1,clr2)
            if mark1 and mark2:
                return 'Sel'
        if mark1 and (not mark2):
            return 'wait'
        return 'error'


    def petCastSkill(self,slot,target='enermy',repeat=5):
        petwinpic = "%s\\pet_skill_start.png" % self.sourcepath
        isSel = self.isPetSkillSelected()
        if isSel == 'error':
            logger.warn('pet skill not seleted')
            #return False
        
        if isSel == 'wait':
            # ans = None
            isFoundWindow = False
            for i in range(repeat):
                #try:
                ans = pyautogui.locateCenterOnScreen(petwinpic,region = self.win.rect)
                # except:
                #     ans = None
                #     time.sleep(0.2)
                # finally:
                if not ans:
                    self.win.set_to_top()
                else:
                    isFoundWindow = True
                    break
                
            if not isFoundWindow:
                logger.warn('没有找到宠物技能窗口')
                return False
            else:
                offset_x = int(random.random()*25)
                offset_y = (slot+1) * self.skillSlotHeight #+16
                pos_x = ans[0] + offset_x
                pos_y = ans[1] + offset_y
                pyautogui.moveTo(pos_x,pos_y,duration = 0.2)
                pyautogui.click()
                # 使用技能


        if target == 'enermy':
            positiondata  = self.mosterState()
            list_val = []
            for i in positiondata.values():
                if i['ishas']:
                    list_val.append(i)
            rdseed = random.random()
            if rdseed >= 0.5:
                list_val.sort(key=lambda x:str(x['id']),reverse=True)

            for tg in list_val:
                x = tg['pos'][0]
                y = tg['pos'][1]
                pyautogui.moveTo(x,y)
                pyautogui.click()
        return True
                
    
    def humanCastSkill(self,slot,level=0,target='enermy'):
        '''
        description: 人物施展技能，对敌人按照随机排序，对自身按照血线排序
        param {*} self
        param {*} slot 指定使用几号位技能
        param {*} level 默认0，使用最高级技能
        param {*} target 指定敌人还是指定自身。
        return {*}
        '''

        if self.win.size[0]>700:
            # print ('big')
            pos = [600,30]
        else:
            # print ('small')
            pos = [440,30]
        offsetx = int(random.random()*10)
        localx = self.win.localx(pos[0]) + offsetx
        localy = self.win.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        for i in range (3):
            pyautogui.click()
        
        try:
            self.skillSlot(slot,withclick=1)
        except:
            # 在这里做一个未检测到图像时的反馈
            logger.warning('没有找到战斗中技能窗口的位置')
            #return False
        finally:
            time.sleep(0.2)


        # self.skillSlot_maxlevel(querylevel=level,withclick=True,isValue=True,isPreview=False)
        
        try:
            self.skillSlot_maxlevel(querylevel=level,withclick=True,isValue=True,isPreview=False)
        
        except:
            # 在这里做一个未检测到图像时的反馈
            logger.warning('没有找到战斗中最高等级技能的位置')
            #return False
        finally:
            time.sleep(0.2)
            
        if target == 'enermy':
            positiondata  = self.mosterState()
            list_val = []
            for i in positiondata.values():
                if i['ishas']:
                    list_val.append(i)
            rdseed = random.random()
            if rdseed >= 0.5:
                list_val.sort(key=lambda x:str(x['id']),reverse=True)

            tg = list_val[0]
            x = tg['pos'][0]
            y = tg['pos'][1]
        else:
            positiondata = self.humanState()
            list_val = []
            for i in positiondata.values():
                if i['ishas']:
                    list_val.append(i)
            # list_val.sort(key=lambda x:str(x['red']),reverse=False)
            # print (list_val)
            lowest = 100
            for i in list_val:
                keyval = int(i['red'])
                if keyval < lowest:
                    lowest = keyval
                    x = i['pos'][0]
                    y = i['pos'][1]
        returnVal = True
        try:
            interval = random.random()
            if interval < 0.2:
                interval = 0.2
            offset = (interval-0.5)*10
            # 对选择怪物的功能进行偏移，避免重复度过高
            pyautogui.moveTo(x+offset,y-offset)
            pyautogui.click(clicks=2,interval=interval)
        except:
            print ('没有获取到人物的信息，施展失败')
            returnVal = False

        finally:
            return returnVal
        

    def skillSlot(self,slotnum,withclick=False):
        '''
        description: 获取技能选择界面的位置（slot 0）以及slot的坐标
        return {*}
        '''        
        offset_y = slotnum * self.skillSlotHeight + 3
        offset_x = int(random.random()*5)

        pos_x = 0
        pos_y = 0
        file = '%s\\skill_start2.png' % (self.sourcepath)
            
        if os.path.exists(file):  
            ans = pyautogui.locateCenterOnScreen(file,region = self.win.rect)  

            if ans:
                if slotnum == 0:
                    # 0号槽指的是窗体本身的识别位置
                    return ans
                
                self.lastSkillWindowPos = ans
                pos_x = ans[0] + offset_x
                pos_y = ans[1] + offset_y
                pyautogui.moveTo(pos_x,pos_y,duration=0.1)
                if withclick:
                    # time.sleep(0.2)
                    pyautogui.click()
                
                return [pos_x,pos_y]
            else:
                logger.info('没找到人物技能识别图像')
                return None
        else:
            logger.warn(file)
            logger.warn('没找到文件')
            return None


    def skillSlot_maxlevel(self,querylevel = 0,istest=False,isValue = False,withclick=False,isPreview=False):
        
        file = '%s\\skill_level_page.png' % (self.sourcepath)
        valid_color = [84,82,67]
        hover_color = [219, 128, 128]

        

        ans = pyautogui.locateCenterOnScreen(file,region = self.win.rect)  
        print('ans',ans)
        if istest:
            logger.warn('filepath in skillslot_maxlevel')
            logger.warn(file)
            im = pyautogui.screenshot(region = self.win.rect)
            im.save('d:/crossgate/skillslot_max_scan_area.png')

        if ans:
            if istest:
                region = (ans[0]+93,ans[1]+10,1+20,170)
            else:
                region = (ans[0]+93,ans[1]+10,1,170)
            im = None
            im = pyautogui.screenshot(region=region)# PIL.ImageGrab.grab(region)
            
            maxlevel = 1
            maxoffset = 0
            if querylevel>0:
                loopcount = querylevel
            else:
                loopcount = 10

            for y in range(loopcount):         
                they = (y*self.skillSlotHeight)+int(self.skillSlotHeight*0.5)
                getcolor = im.getpixel((0,they))
                if istest:
                    im.putpixel((0,they),(255,0,0))
                    im.save('d:/crossgate/maxlevel.png')
                logger.debug('the color is')
                logger.debug(getcolor)
                isvalid = self.win.isSimilarColor(getcolor,valid_color)
                ishover = self.win.isSimilarColor(getcolor,hover_color)
                if ishover or isvalid:
                    maxlevel = y+1
                    maxoffset = they + self.skillSlotHeight
                else:
                    break
            if istest:
                strtime = str(time.time())
                im.save("d:/crossgate/skillslot_max.png")


            logger.info('技能最高等级是%i级' % maxlevel)
            target_x = ans[0] + int(random.random()*10)
            target_y = ans[1]+maxoffset # + self.offset_y(val=1)
            if withclick:
                pyautogui.moveTo(target_x,target_y,duration=0.1)
                time.sleep(0.1)
                pyautogui.click()

            if isPreview:
                pyautogui.moveTo(target_x,target_y,duration=0.1)

            if isValue:
                logger.info('返回坐标')
                return [target_x,target_y]
            else:
                logger.info('返回级数')
                return maxlevel
        else:
            logger.warn('等级选择页面没有打开')
            return False



    def serialize(self):
        if self.isBattle():
            self.humanState()
            # self.mosterState()