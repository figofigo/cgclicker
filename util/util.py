#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-03-30 23:12:01
LastEditTime: 2024-04-01 02:59:47
LastEditors: figo - uz
Description: 
copyright: figo software 2020-2021
'''

import keyboard
import time
import random
import math
import os
import pyautogui

def timeit(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f'{func.__name__} 运行时间为：{end - start} 秒')
        return result
    return wrapper

class basic():

    def __init__(self):
        super(basic,self).__init__()
        # self.testPath = '%s/resources/test' % self.root_path()
        # self.wintype = 0
        # self.cg_path = ''
        # 魔力宝贝的启动路径
        pass
    def root_path(self):
        if 'CGCLICKER' in os.environ.keys():
            rootPath = os.environ['CGCLICKER']
        else:
            # raise RuntimeError ('start function error,"CGCLICKER" not in environment')
            filePath = os.path.abspath(__file__)
            sp_filepath = filePath.split(os.sep)
            rootPath = os.sep.join(sp_filepath[:-2])
        return rootPath

    def getDistance(self,pos1,pos2):
        # 计算两个坐标之间的距离
        dis = math.sqrt(math.pow(pos2[0]-pos1[0],2) + math.pow(pos2[1]-pos1[1],2))
        return int(dis)



    def waitbreak(self):
        if keyboard.is_pressed('alt'): #if key 'alt' is pressed 
            raise KeyboardInterrupt ('手动停止自动循环')

    def wait(self,lenth):
        rnd = random.random()
        time.sleep(lenth + rnd)

        # print (others,'/',checklength)
        # im.save('d:/modline.png')

    # def set_rect(self,m):
    #     self.rect = self.real_rect(m)

    def isSimilarColor(self,input_color,regular_color,limit = 10):
        # print ('input',input_color)
        # print ('regular_color',regular_color)
        for i in range(len(input_color)):
            if input_color[i] not in range(regular_color[i]-limit,regular_color[i]+limit):
                return False
        return True
            
    def offset_x(self,val = 10,noneg = False):
        if noneg:
            seed = random.random()
        else:
            seed = random.random()-0.5

        return int(val*seed)

    def offset_y(self,val = 5,noneg = False):
        if noneg:
            seed = random.random()
        else:
            seed = random.random()-0.5

        return int(val*seed)

    def say(self,words):
        # self.resetMouse()
        # pyautogui.click()
        pyautogui.typewrite(words)
        time.sleep(0.3)
        pyautogui.press('enter')
        time.sleep(0.1)
        pyautogui.press('enter')

    def reset_map_window_pos(self):
        pos = [565,35]
        if self.is_big_window():
            pos = [715,35]
        statuspath = '%s\\resources\\where' % self.root_path()
        onmark = 'ditu.png'
        onPath = '%s\\%s' % (statuspath,onmark)
        
        for repeat in range(5):
            self.resetMouse()
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect) 
            except:
                ans = None
            finally:
                targetpos = [self.rect[0] + pos[0],self.rect[1]+pos[1]]

            
            
            if not ans:
                isfoundopenbag = False
                self.resetMouse()
                pyautogui.rightClick()
                for i in range(3):       
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('s') 
                    pyautogui.keyUp('ctrl')
                    for k in range(5):
                        ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)  
                        if ans:
                            isfoundopenbag = True
                            break
                        else:
                            time.sleep(0.1)
                    if isfoundopenbag:
                        break

            dis = math.sqrt(math.pow(targetpos[0]-ans[0],2)+math.pow(targetpos[1]-ans[1],2))
            if dis <2:
                return True
            else:
                pyautogui.moveTo(ans[0],ans[1])
            
                pyautogui.dragTo(self.rect[0]+pos[0],self.rect[1]+pos[1],duration=1,mouseDownUp=True)
        return True
    
    def reset_skill_window_pos(self):
        pos = [self.rect[2]-235,self.rect[3]-335]
        offset = [0,-10]
        statuspath = '%s\\resources\\battle' % self.root_path()
        onmark = 'skill_start2.png'
        onPath = '%s\\%s' % (statuspath,onmark)
        
        for repeat in range(5):
            self.resetMouse()
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect) 
            except :
                ans = None
            finally:
                pass

            targetpos = [self.rect[0] + pos[0],self.rect[1]+pos[1]]

            
            
            if not ans:
                isfoundopenbag = False
                self.resetMouse()
                pyautogui.rightClick()
                for i in range(3):       
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('w') 
                    pyautogui.keyUp('ctrl')
                    for k in range(5):
                        try:
                            ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)
                        except :
                            ans = None
                        finally:
                            if ans:
                                isfoundopenbag = True
                                break
                            else:
                                time.sleep(0.1)
                    if isfoundopenbag:
                        break

            dis = math.sqrt(math.pow(targetpos[0]-ans[0],2)+math.pow(targetpos[1]-ans[1],2))
            if dis <2:
                pyautogui.keyDown('ctrl') 
                pyautogui.press('w') 
                pyautogui.keyUp('ctrl')
                return True
            else:
                pyautogui.moveTo(ans[0]+offset[0],ans[1]+offset[1])
            
                pyautogui.dragTo(self.rect[0]+pos[0]+offset[0],self.rect[1]+pos[1]+offset[1],duration=1,mouseDownUp=True)
        return True
    
    def close_brief(self):
        pyautogui.click(400,300,duration=0.5)
    
    def set_path(self,val):
        self.cg_path = val


    def load_chat(self,istest = False):
        log_dir = ''
        if istest:
            log_dir = r'D:\Program Files (x86)\易玩通\魔力宝贝6.0\Log'
        else:
            cgdir = os.path.dirname(self.cg_path)
            log_dir = f'{cgdir}/Log'
        files = os.listdir(log_dir)
        files.sort(reverse=True)
        file_path = f'{log_dir}/{files[0]}'
        alllines = []
        print ('start read file',file_path)
        with open(file_path, 'r', encoding='gbk', errors='ignore') as f:
            alllines = [line for line in f]

        if istest:
            print (alllines)
        return alllines
    
    def isMigongshuaxin(self):
        alllines = self.recent_chat()
        for i in alllines:
            if i['type'] == 'anno':
                if '你感觉到一股不可思议的力量' in i['chat']:
                    print ('迷宫刷新，时间是',i['time_str'])
                    return True
        return False

    def analyze_chat(self,val):
        datadict = {}
        if '丂' in val:
            sptime = val.split('丂')
            strtime = sptime[0]
            rest_string = sptime[1]
            datadict['time_str'] = strtime
            if ':' in rest_string:
                spval = rest_string.split(':')
                user_sp = spval[0].split(']')
                datadict['type'] = 'chat'
                datadict['chat'] = spval[-1].replace('\n','')
                datadict['user'] = user_sp[-1]
                datadict['range'] = user_sp[0][1:]
            else:
                datadict['type'] = 'anno'
                datadict['chat'] = rest_string.replace('\n','')
                datadict['user'] = 'system'
                datadict['range'] = 'system'
        else:
            datadict['time_str'] = 'none'
            datadict['type'] = 'other'
            datadict['chat'] = val.replace('\n','')
            datadict['user'] = 'system'
            datadict['range'] = 'system'

        return datadict

    def recent_chat(self,length = 3):
        lines = self.load_chat()
        returnlist = []
        for i in lines[-length:]:
            data = self.analyze_chat(i)
            returnlist.append(data)
        return returnlist

    def test(self):
        pet_x = (self.rect[0]) + 800-180
        pet_y = (self.rect[1]) + 55
        pet_dataArea = (pet_x,pet_y,1,3)
        pyautogui.moveTo(pet_x,pet_y)
        # pet_im = pyautogui.screenshot(region=pet_dataArea)
        # pet_im.save('d:/pet_im.png')

if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    bc = basic()
    bc.set_rect(m)
    # bc.set_path(cgw.main['path'])
    ans = bc.load_chat()
    print (ans)
    # pos = bc.test()
    # print (pos)