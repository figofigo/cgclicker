#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-04-09 00:51:30
LastEditTime: 2024-04-09 02:30:59
LastEditors: figo - uz
Description: 监控聊天系统中最新的数据
copyright: figo software 2020-2021
'''

import os


class dialog_moniter():
    def __init__(self,window):
        self.win = window

    @property
    def path(self):
        return self.win.path

    
    def load_chat(self,istest = False):
        basepath = os.path.dirname(self.path)
        directory = r'%s\Log' % basepath

        # 获取目录下所有文件的路径
        files = [os.path.join(directory, f) for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]

        # 根据文件的修改时间对文件进行排序
        files_sorted = sorted(files, key=lambda x: os.path.getmtime(x),reverse=True)


        file_path = files_sorted[0]
        alllines = []
        # print ('start read file',file_path)
        with open(file_path, 'r', encoding='gbk', errors='ignore') as f:
            alllines = [line for line in f]
        # print (alllines)
        # if istest:
        #     print (alllines)
        return alllines
    
    def isMigongshuaxin(self):
        alllines = self.recent_chat(length=15)
        for i in alllines:
            if i['type'] == 'anno':
                if '你感觉到一股不可思议的力量' in i['chat']:
                    print ('迷宫刷新，时间是',i['time_str'])
                    return True
        return False

    def analyze_chat(self,val):
        datadict = {}
        if '丂' in val:
            sptime = val.split('丂')
            strtime = sptime[0]
            rest_string = sptime[1]
            datadict['time_str'] = strtime
            if ':' in rest_string:
                spval = rest_string.split(':')
                user_sp = spval[0].split(']')
                datadict['type'] = 'chat'
                datadict['chat'] = spval[-1].replace('\n','')
                datadict['user'] = user_sp[-1]
                datadict['range'] = user_sp[0][1:]
            else:
                datadict['type'] = 'anno'
                datadict['chat'] = rest_string.replace('\n','')
                datadict['user'] = 'system'
                datadict['range'] = 'system'
        else:
            datadict['time_str'] = 'none'
            datadict['type'] = 'other'
            datadict['chat'] = val.replace('\n','')
            datadict['user'] = 'system'
            datadict['range'] = 'system'

        return datadict

    def recent_chat(self,length = 10):
        lines = self.load_chat()
        returnlist = []
        for i in lines[-length:]:
            data = self.analyze_chat(i)
            returnlist.append(data)
        return returnlist
