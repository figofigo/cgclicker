#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-11-15 20:55:28
LastEditTime: 2024-05-12 13:54:31
LastEditors: figo - uz
Description: 背包模块的功能
copyright: figo software 2020-2021
'''

import pyautogui
import time
import os
import random
import logging
import sys
sys.path.append('.')

from util import util

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('item_system')
logger.setLevel(logging.INFO)

class itemSystem():
    def __init__(self,win):
        super(itemSystem,self).__init__()
        self.win = win
        # self.rect = self.win.rect
        # 截图检查的时候，确定一下区域
        self.defined_window_pos = [125,self.win.size[1]-270]

        self.item_use_range = 10
        self.last_item_check = []

        self.locatepath = '%s\\resources\\location' % self.win.root_path()
        self.statuspath = '%s\\resources\\status' % self.win.root_path()
        self.food_marks_folder = '%s\\resources\\food' % self.win.root_path()
        self.mineral_marks_folder = '%s\\resources\\mineral' % self.win.root_path()

        self.idDict = {
            1:[-100,50],
            2:[-50,50],
            3:[0,50],
            4:[50,50],
            5:[100,50],
            6:[-100,100],
            7:[-50,100],
            8:[0,100],
            9:[50,100],
            10:[100,100],
            11:[-100,150],
            12:[-50,150],
            13:[0,150],
            14:[50,150],
            15:[100,150],
            16:[-100,200],
            17:[-50,200],
            18:[0,200],
            19:[50,200],
            20:[100,200]
        }
        # 这个是每一个slot的偏移量

        self._food_marks = []
        self._mineral_marks = []

    @property
    def pos(self):
        # statuspath = '%s\\resources\\status' % self.win.root_path()
        onmark = 'itemBag.png'
        onPath = '%s\\%s' % (self.statuspath,onmark)
        
        self.win.resetMouse([50,10])
        try:
            ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect) 
            return ans
        except:
            pass
        finally:
            pass
        return None
    

    def reset_window_pos(self,pos = []):
        '''
        description: 将地图窗口移动到指定位置
        return {*}
        '''     
        thepos = self.pos
        print ('thepos',thepos)
        if pos:
            tx = self.win.localx(pos[0])
            ty = self.win.localy(pos[1])
        else:
            msp = self.defined_window_pos
            tx=self.win.localx(msp[0])
            ty=self.win.localy(msp[1])


        isSuccess = False
        for i in range(5):
            logger.info('moveloop[%i] ' % i)
            baseinfo = self.pos
            if not baseinfo:
                pyautogui.moveRel(50,50)
                time.sleep(0.2)
                continue
            sx = baseinfo[0]
            sy = baseinfo[1]

            offset_x = tx-sx
            offset_y = ty-sy

            if (offset_x + offset_y) == 0:
                logger.info('done')
                isSuccess = True
                break
            else:
                logger.info('offset [%i,%i]' % (offset_x ,offset_y))
                pyautogui.moveTo(sx,sy,duration=0.2)
                pyautogui.dragRel(offset_x,offset_y,duration=1)
        return isSuccess




    def eat_operate(self,char = 0,slot = 0,isdebug = False,isfast = False):
        # 吃食物时候点击的两次操作
        check_length = 50
        mid_x = self.win.pos[0] + int(self.win.size[0]*0.5)
        mid_y = self.win.pos[1] + int(self.win.size[1]*0.5)
        # print ('mid',mid_x,mid_y)
        # region = (mid_x-120,mid_y-35,check_length,100)
        region = (mid_x-120,mid_y-35,check_length,check_length)


        sel_char_im = pyautogui.screenshot(region = region)
        if isdebug:
            testsave = 'D:/crossgate/sel_char_im.png'
            sel_char_im.save(testsave)

        sel_char_im_colorlist = []

        
        sel_char_im_colorlist.append (sel_char_im.getpixel((0,0)))
        sel_char_im_colorlist.append (sel_char_im.getpixel((0,check_length-1)))
        sel_char_im_colorlist.append (sel_char_im.getpixel((check_length-1,check_length-1)))

        total = 0
        for sig_color in sel_char_im_colorlist:
            print('sigcolor_stage1',sig_color)
            mod_color = list(sig_color)
            mod_color[1] = 0
            # 屏蔽绿色值
            total += sum(mod_color)


        if total<10:
            logger.info('[请选择对象]界面已经打开')
        else:            
            print ('colorlist',sel_char_im_colorlist)
            print ('total',total)
            return False
        pyautogui.moveTo(mid_x,mid_y-120,duration=0.1)
        # 重置鼠标位置


        slot0_x = mid_x-30
        slot0_y = mid_y -30

        if self.win.status == 'peace':
            pyautogui.click(slot0_x,slot0_y,clicks=2,interval=0.05)
            # pyautogui.moveTo(slot0_x,slot0_y)
            logger.debug('一阶段点击结束')
            
        else:
            logger.warning('一阶段点击时遭遇战斗，取消')
            return False

        if isfast:
            logger.info('进行快速补血模式')
            for i in range(3):
                pyautogui.click(slot0_x,slot0_y,clicks=1,interval=0.1)
                time.sleep(0.1)
            return True
        else:
            logger.info('进行普通补血模式')
            region2 = (mid_x-144,mid_y-80,10,10)
            for secloop in range(5):
                sel_char_im2 = pyautogui.screenshot(region = region2)

                # testsave = 'D:/crossgate/sel_char_imgggg.png'
                # sel_char_im2.save(testsave)

                sel_char_im_colorlist = []

                
                sel_char_im_colorlist.append (sel_char_im2.getpixel((0,0)))
                sel_char_im_colorlist.append (sel_char_im2.getpixel((0,9)))
                sel_char_im_colorlist.append (sel_char_im2.getpixel((9,9)))

                total = 0
                for sig_color in sel_char_im_colorlist:
                    print('sigcolor_stage2',sig_color)
                    mod_color = list(sig_color)
                    mod_color[1] = 0
                    # 屏蔽绿色值
                    total += sum(mod_color)
                if total<=10 :
                    logger.info('[二阶段界面]界面已经打开')
                    time.sleep(0.5)
                    pyautogui.click(slot0_x,slot0_y,clicks=2,interval=0.05)
                    return True
                elif 250<=total<=270:
                    logger.info('[二阶段界面]界面已经打开 - hover')
                    time.sleep(0.5)
                    pyautogui.click(slot0_x,slot0_y,clicks=2,interval=0.05)
                    return True
                else:            
                    print ('colorlist',sel_char_im_colorlist)
                    print ('total',total)
                    logger.warning('2阶段窗口迟迟没能打开，可能是卡了')
                    return False
        
            
    @util.timeit
    def eat(self,blue_or_red = 'red',isfast=False):
        '''
        description: 吃食物的功能
        param {*} self
        param {*} blue_or_red 是否使用食物，调试用
        param {*} isfast 吃食物的选择画面是否使用快速模式，快速模式有可能出错给队长吃
        return {*}
        '''   
        self.win.resetMouse()
        target_data = []
        for ii in range(10):
            i = ii+1
            # list_i = [i,i+5]
            # for ll in (list_i):
            data = self.slot_indentify(i)
            if blue_or_red in str(data[1]):
                target_data = data

            if target_data:
                break
        if not target_data:
            return False
        pyautogui.moveTo(target_data[2])
        pyautogui.doubleClick()

        for ___ in range(5):
            ans = self.eat_operate(isfast=isfast)
            if ans:
                self.set_bag(False)
                # 关背包
                return True
            else:
                time.sleep(1)


        return -1

    @property
    def mineral_marks(self):
        if not self._mineral_marks:
            self._mineral_marks = os.listdir(self.mineral_marks_folder)
        return self._mineral_marks
        

    @property
    def food_marks(self):
        if not self._food_marks:
            self._food_marks = os.listdir(self.food_marks_folder)
        return self._food_marks

    def slot_indentify(self,slot,preset='blue600.png',pos = None,autosave=False):
        if not pos:
            pos = self.pos
        indentify_data = [slot,None,None]
        
        if not pos:
            return indentify_data

        offsetx = self.idDict[slot][0] + 5
        offsety = self.idDict[slot][1]
        targetx = pos.x+offsetx
        targety = pos.y+offsety
            # 根据偏移字典和basepos计算出来slot的位置

        qregion = (targetx-5,targety-5,10,10)
        # print (qregion)
        # im = pyautogui.screenshot(region=qregion)
        if autosave:
            im = pyautogui.screenshot(region=qregion)
            savepath = self.food_marks_folder+'/' + str(slot) + '_new.png'
            # print('savepath',savepath)
            im.save(savepath)
        # im.save('c:/data/%s_check.png' % str(slot))
        self.food_marks.insert(0,preset)
        for picname in self.food_marks:
            onPath = '%s/%s' % (self.food_marks_folder,picname)
            # print (onPath)
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = qregion) 
            except:
                ans = None
            finally:
                pass

            if ans:
                indentify_data = [slot,picname,ans]
                break 
        return indentify_data

    @util.timeit
    def item_indentify(self):
        '''
        description: 根据cata的指定，去寻找line行是否有需要的内容
        return {*}
        '''        
        
        self.win.resetMouse([20,20])

        basepos = self.pos
        if not basepos:
            logger.warn('bag not open')
            return False

        returnData = []
        insertcheck = 'ampty.png'
        for i in range(self.item_use_range):
            itemslot = i+1
            if len(self.last_item_check) >= itemslot:
                # print (i,self.last_item_check[i])
                insertcheck = self.last_item_check[i][1]
            #     print ('insertcheck',insertcheck,i)
            data = self.slot_indentify(itemslot,preset=insertcheck,pos=basepos)
            # print (i,data)
            returnData.append(data)
        self.last_item_check = returnData
        return returnData


    @util.timeit
    def farm_indentify(self):
        '''
        description: 根据cata的指定，去寻找line行是否有需要的内容
        return {*}
        '''        
        
        self.win.resetMouse([20,20])

        basepos = self.pos
        if not basepos:
            logger.warn('bag not open')
            return False

        returnData = []
        insertcheck = 'ampty.png'
        for i in range(20):
            itemslot = i+1
            data = self.mineral_indentify(itemslot,preset=insertcheck,pos=basepos)
            # print (i,data)
            returnData.append(data)

        return returnData

    def mineral_indentify(self,slot,preset='tong.png',pos = None,autosave=True):
        if not pos:
            pos = self.pos
        indentify_data = [slot,None,None]
        
        if not pos:
            return indentify_data

        offsetx = self.idDict[slot][0] + 5
        offsety = self.idDict[slot][1]
        targetx = pos.x+offsetx
        targety = pos.y+offsety
            # 根据偏移字典和basepos计算出来slot的位置

        qregion = (targetx-5,targety-5,10,10)
        if autosave:
            im = pyautogui.screenshot(region=qregion)
            savepath = self.mineral_marks_folder+'/' + str(slot) + '_new.png'
            # print('savepath',savepath)
            im.save(savepath)
        
        for picname in self.mineral_marks:
            onPath = '%s/%s' % (self.mineral_marks_folder,picname)
            # print (onPath)
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = qregion) 
            except:
                ans = None
            finally:
                pass

            if ans:
                indentify_data = [slot,picname,ans]
                break 
        return indentify_data


    def set_bag(self,status):
        '''
        description: 打开包裹
        return {*}
        '''        
        if not isinstance(status,bool):
            raise ('输入数据错误，status必须是bool')
        

        for i in range(3):
            try:
                ans = self.pos  
            except:
                ans = None
            finally:
                pass

            
            if status == bool(ans):
                logger.info(f'背包设置状态为[{status}]')
                if status:
                    # 只有在开启背包的时候才尝试重置位置
                    self.reset_window_pos()
                return True
            else:
                pyautogui.keyDown('ctrl') 
                pyautogui.press('e') 
                pyautogui.keyUp('ctrl')
        return False



    def serialize(self):
        
        dt = self.reset_window_pos()

        return dt