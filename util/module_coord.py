#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-28 11:07:13
LastEditTime: 2024-06-08 13:33:56
LastEditors: figo - uz
Description: 用于定位的基础类,继承自where
copyright: figo software 2020-2021
'''
import random
import pyautogui
import time
import sys
import math
import logging

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('coordsys')
logger.setLevel(logging.INFO)

sys.path.append('.')

from util import module_map 


class coordsys(module_map.whereAmI):
    def __init__(self,win):
        super(coordsys,self).__init__(win)
        self.chadet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y"]
        self.heightCount = 13
        self.horizonSize = 64
        self.verticalSize = 48
        self.titleHeight = 25
        self.win = win
        self.rect = self.win.rect
        self.centerCode = 'M6'

        self.last_x = -1
        self.last_y = -1

    def loopWithRugular(self):
        for x,char in enumerate(self.chadet):
            for y in range(self.heightCount):
                offset_x = self.horizonSize*0.25
                offset_y = self.verticalSize *0.25
                if x%2 == 1:
                    # offset_x = self.horizonSize*0.5
                    offset_y = self.verticalSize *0.75
                pos_x = x*(self.horizonSize*0.5) + offset_x #+ self.rect[0]
                pos_y = y*self.verticalSize + offset_y #+ self.rect[1]
                print ("\"%s%i\":[%i,%i]," % (char,y,pos_x,pos_y))

    @property
    def coord_data(self):
        data = {
        "A0":[16,12],
        "A1":[16,60],
        "A2":[16,108],
        "A3":[16,156],
        "A4":[16,204],
        "A5":[16,252],
        "A6":[16,300],
        "A7":[16,348],
        "A8":[16,396],
        "A9":[16,444],
        "A10":[16,492],
        "A11":[16,540],
        "A12":[16,588],
        "B0":[48,36],
        "B1":[48,84],
        "B2":[48,132],
        "B3":[48,180],
        "B4":[48,228],
        "B5":[48,276],
        "B6":[48,324],
        "B7":[48,372],
        "B8":[48,420],
        "B9":[48,468],
        "B10":[48,516],
        "B11":[48,564],
        "B12":[48,612],
        "C0":[80,12],
        "C1":[80,60],
        "C2":[80,108],
        "C3":[80,156],
        "C4":[80,204],
        "C5":[80,252],
        "C6":[80,300],
        "C7":[80,348],
        "C8":[80,396],
        "C9":[80,444],
        "C10":[80,492],
        "C11":[80,540],
        "C12":[80,588],
        "D0":[112,36],
        "D1":[112,84],
        "D2":[112,132],
        "D3":[112,180],
        "D4":[112,228],
        "D5":[112,276],
        "D6":[112,324],
        "D7":[112,372],
        "D8":[112,420],
        "D9":[112,468],
        "D10":[112,516],
        "D11":[112,564],
        "D12":[112,612],
        "E0":[144,12],
        "E1":[144,60],
        "E2":[144,108],
        "E3":[144,156],
        "E4":[144,204],
        "E5":[144,252],
        "E6":[144,300],
        "E7":[144,348],
        "E8":[144,396],
        "E9":[144,444],
        "E10":[144,492],
        "E11":[144,540],
        "E12":[144,588],
        "F0":[176,36],
        "F1":[176,84],
        "F2":[176,132],
        "F3":[176,180],
        "F4":[176,228],
        "F5":[176,276],
        "F6":[176,324],
        "F7":[176,372],
        "F8":[176,420],
        "F9":[176,468],
        "F10":[176,516],
        "F11":[176,564],
        "F12":[176,612],
        "G0":[208,12],
        "G1":[208,60],
        "G2":[208,108],
        "G3":[208,156],
        "G4":[208,204],
        "G5":[208,252],
        "G6":[208,300],
        "G7":[208,348],
        "G8":[208,396],
        "G9":[208,444],
        "G10":[208,492],
        "G11":[208,540],
        "G12":[208,588],
        "H0":[240,36],
        "H1":[240,84],
        "H2":[240,132],
        "H3":[240,180],
        "H4":[240,228],
        "H5":[240,276],
        "H6":[240,324],
        "H7":[240,372],
        "H8":[240,420],
        "H9":[240,468],
        "H10":[240,516],
        "H11":[240,564],
        "H12":[240,612],
        "I0":[272,12],
        "I1":[272,60],
        "I2":[272,108],
        "I3":[272,156],
        "I4":[272,204],
        "I5":[272,252],
        "I6":[272,300],
        "I7":[272,348],
        "I8":[272,396],
        "I9":[272,444],
        "I10":[272,492],
        "I11":[272,540],
        "I12":[272,588],
        "J0":[304,36],
        "J1":[304,84],
        "J2":[304,132],
        "J3":[304,180],
        "J4":[304,228],
        "J5":[304,276],
        "J6":[304,324],
        "J7":[304,372],
        "J8":[304,420],
        "J9":[304,468],
        "J10":[304,516],
        "J11":[304,564],
        "J12":[304,612],
        "K0":[336,12],
        "K1":[336,60],
        "K2":[336,108],
        "K3":[336,156],
        "K4":[336,204],
        "K5":[336,252],
        "K6":[336,300],
        "K7":[336,348],
        "K8":[336,396],
        "K9":[336,444],
        "K10":[336,492],
        "K11":[336,540],
        "K12":[336,588],
        "L0":[368,36],
        "L1":[368,84],
        "L2":[368,132],
        "L3":[368,180],
        "L4":[368,228],
        "L5":[368,276],
        "L6":[368,324],
        "L7":[368,372],
        "L8":[368,420],
        "L9":[368,468],
        "L10":[368,516],
        "L11":[368,564],
        "L12":[368,612],
        "M0":[400,12],
        "M1":[400,60],
        "M2":[400,108],
        "M3":[400,156],
        "M4":[400,204],
        "M5":[400,252],
        "M6":[400,300],
        "M7":[400,348],
        "M8":[400,396],
        "M9":[400,444],
        "M10":[400,492],
        "M11":[400,540],
        "M12":[400,588],
        "N0":[432,36],
        "N1":[432,84],
        "N2":[432,132],
        "N3":[432,180],
        "N4":[432,228],
        "N5":[432,276],
        "N6":[432,324],
        "N7":[432,372],
        "N8":[432,420],
        "N9":[432,468],
        "N10":[432,516],
        "N11":[432,564],
        "N12":[432,612],
        "O0":[464,12],
        "O1":[464,60],
        "O2":[464,108],
        "O3":[464,156],
        "O4":[464,204],
        "O5":[464,252],
        "O6":[464,300],
        "O7":[464,348],
        "O8":[464,396],
        "O9":[464,444],
        "O10":[464,492],
        "O11":[464,540],
        "O12":[464,588],
        "P0":[496,36],
        "P1":[496,84],
        "P2":[496,132],
        "P3":[496,180],
        "P4":[496,228],
        "P5":[496,276],
        "P6":[496,324],
        "P7":[496,372],
        "P8":[496,420],
        "P9":[496,468],
        "P10":[496,516],
        "P11":[496,564],
        "P12":[496,612],
        "Q0":[528,12],
        "Q1":[528,60],
        "Q2":[528,108],
        "Q3":[528,156],
        "Q4":[528,204],
        "Q5":[528,252],
        "Q6":[528,300],
        "Q7":[528,348],
        "Q8":[528,396],
        "Q9":[528,444],
        "Q10":[528,492],
        "Q11":[528,540],
        "Q12":[528,588],
        "R0":[560,36],
        "R1":[560,84],
        "R2":[560,132],
        "R3":[560,180],
        "R4":[560,228],
        "R5":[560,276],
        "R6":[560,324],
        "R7":[560,372],
        "R8":[560,420],
        "R9":[560,468],
        "R10":[560,516],
        "R11":[560,564],
        "R12":[560,612],
        "S0":[592,12],
        "S1":[592,60],
        "S2":[592,108],
        "S3":[592,156],
        "S4":[592,204],
        "S5":[592,252],
        "S6":[592,300],
        "S7":[592,348],
        "S8":[592,396],
        "S9":[592,444],
        "S10":[592,492],
        "S11":[592,540],
        "S12":[592,588],
        "T0":[624,36],
        "T1":[624,84],
        "T2":[624,132],
        "T3":[624,180],
        "T4":[624,228],
        "T5":[624,276],
        "T6":[624,324],
        "T7":[624,372],
        "T8":[624,420],
        "T9":[624,468],
        "T10":[624,516],
        "T11":[624,564],
        "T12":[624,612],
        "U0":[656,12],
        "U1":[656,60],
        "U2":[656,108],
        "U3":[656,156],
        "U4":[656,204],
        "U5":[656,252],
        "U6":[656,300],
        "U7":[656,348],
        "U8":[656,396],
        "U9":[656,444],
        "U10":[656,492],
        "U11":[656,540],
        "U12":[656,588],
        "V0":[688,36],
        "V1":[688,84],
        "V2":[688,132],
        "V3":[688,180],
        "V4":[688,228],
        "V5":[688,276],
        "V6":[688,324],
        "V7":[688,372],
        "V8":[688,420],
        "V9":[688,468],
        "V10":[688,516],
        "V11":[688,564],
        "V12":[688,612],
        "W0":[720,12],
        "W1":[720,60],
        "W2":[720,108],
        "W3":[720,156],
        "W4":[720,204],
        "W5":[720,252],
        "W6":[720,300],
        "W7":[720,348],
        "W8":[720,396],
        "W9":[720,444],
        "W10":[720,492],
        "W11":[720,540],
        "W12":[720,588],
        "X0":[752,36],
        "X1":[752,84],
        "X2":[752,132],
        "X3":[752,180],
        "X4":[752,228],
        "X5":[752,276],
        "X6":[752,324],
        "X7":[752,372],
        "X8":[752,420],
        "X9":[752,468],
        "X10":[752,516],
        "X11":[752,564],
        "X12":[752,612],
        "Y0":[784,12],
        "Y1":[784,60],
        "Y2":[784,108],
        "Y3":[784,156],
        "Y4":[784,204],
        "Y5":[784,252],
        "Y6":[784,300],
        "Y7":[784,348],
        "Y8":[784,396],
        "Y9":[784,444],
        "Y10":[784,492],
        "Y11":[784,540],
        "Y12":[784,588]}
        return data

    def moveTo(self,x,y,shackx=4,shacky=4,withclick=False,q=False,duration=0,withhold=False):

        #def new_coordsys(self,x,y):
        '''
        description: 重新定义了一下坐标系，不再需要对应表了
        return {*}
        '''        
        xsize = 32
        ysize = 48*0.5
        win_offset = [0,0]
        center_x = int(self.rect[2]*0.5)+int(win_offset[0]*0.5)
        center_y = int(self.rect[3]*0.5)+int(win_offset[1]*0.5)
        # 获取到中心位置


        ew_x = (x*xsize)
        ew_y = (x*ysize)*-1


        ns_x = (y*xsize)
        ns_y = (y*ysize)

        tar_x = center_x + ew_x + ns_x
        tar_y = center_y + ew_y + ns_y - (ysize)

        if q:
            return [tar_x,tar_y]

        horOffset = 0
        if shackx:
            xrnd = random.random()
            horOffset = (xrnd-0.5)* shackx

        verOffset = 0
        if shacky:
            yrnd = random.random()
            verOffset = (yrnd-0.5)* shacky

        targetX = tar_x + horOffset
        targetY = tar_y + verOffset

        if withhold:
            pyautogui.dragTo(targetX,targetY,duration=duration)


        else:
            localize_x = self.win.localx(targetX)
            localize_y = self.win.localy(targetY)
            
            pyautogui.moveTo(localize_x,localize_y,duration=duration)

            if withclick:

                pyautogui.click() 

        return True




    def walkTo(self,direction,distance=1,button='left',clicks = 1,interval = 0.5,wait=0.2):
        '''
        description: 根据角色自身位置的移动系统
        direction {str} 移动方向 [n,en,e,es,ws,w,wn]
        distance {int} 移动距离
        button {str} 按钮 left,right,lefthold
        clicks {int} 鼠标点击次数
        interval {float} 鼠标点击间隔
        wait {float} 每个动作之间的等待时间
        return {*}
        '''        
        rotateDict = {
            'n':[0,-1],
            'en':[1,-1],
            'e':[1,0],
            'es':[1,1],
            's':[0,1],
            'ws':[-1,1],
            'w':[-1,0],
            'wn':[-1,-1],

        }
        if len(direction) > 1:
            if direction in ['en','ne']:
                direction = 'en'
            elif direction in ['es','se']:
                direction = 'es'
            elif direction in ['ws','sw']:
                direction = 'ws'
            elif direction in ['wn','nw']:
                direction = 'wn'
            else:
                direction = direction

        code = rotateDict[direction]
        self.moveTo(code[0]*distance,code[1]*distance,withclick=False)

        if button in ['left','right']:
            logger.debug('code 是 %s,操作是点击[%i]次鼠标[%s]' % (code,clicks,button))
            if clicks == 0:
                time.sleep(wait)
            else:
                pyautogui.click(button=button, clicks=clicks, interval=interval,duration=wait) 
        elif button in ['lefthold']:
            logger.debug('code 是 %s,操作[ %s ]' % (code,button))
            pyautogui.mouseDown()
        else:
            logger.debug('code 是 %s,操作是返回数值' %code)
            value = self.moveTo(code,q=True)
            return value

        return True

    def getDistance(self,pos1,pos2):
        
        dis = math.sqrt(math.pow(pos2[0]-pos1[0],2) + math.pow(pos2[1]-pos1[1],2))
        return int(dis)

    def normalize(self,target):
        x = target[0]
        y = target[1]
        mod = math.sqrt(math.pow(x,2)+math.pow(y,2))
        return float(x)/mod,float(y)/mod
    
    def check(self,target,validrange=6,mouse_action='hover'):
        if isinstance(target,list) and len(target) == 2:
            pos = self.pos(f=False)
            if sum(pos) <= 0:
                logger.warn('获取坐标失败')
                return False
            
            distance = self.getDistance(pos,target)
            if distance > validrange:
                logger.warn(f'目标坐标超过合法值{validrange}，当前是{distance}')
                return False
                
            logger.debug('当前开始坐标是')
            logger.debug(pos)
            # 获取坐标。获取不到就抛异常
            offsetx = target[0] - pos[0]
            offsety = target[1] - pos[1]
            
            logger.debug('将要进行检查')
            self.moveTo(offsetx,offsety,withclick=False,duration=0.1,withhold=False)
        
    def hanging(self,direction,times=10):
        for __ in range(times):
            for drc in direction:
                pyautogui.mouseDown()
                self.walkTo(drc,distance=2,clicks=0,button='left',wait=0.7)
            if self.win.status != 'peace':
                pyautogui.mouseUp()
                return False
        return True

    def goto(self,target,check_count=100,duration=0.1,limitrange = 0,blur=0,maplimit='',validtimes=3):
        '''
        description: 最常用的goto功能
        param {*} self
        param {list} target 目标坐标
        param {int} check_count 检查是否到达的次数
        param {float} duration 移动时的鼠标移动时间
        param {int} limitrange 默认为0，如果指定数值则会覆盖自动数值。指定了一次移动最远的距离。
        param {int} blur 距离目标为blue数值时，视为到达，默认为0
        param {*} validrange 对于一些长距离移动目标，可以指定这个属性避免报错
        return {*}
        '''
        if limitrange <1 :
            if self.win.size[-1]>500:
                limitrange = 7
            else:
                limitrange = 5
        validrange = limitrange *validtimes

        # 根据窗口大小，自适应点击范围限制
        logger.info(f'前往坐标{target}，blur为{blur}')

        if isinstance(target,list) and len(target) == 2:
            poslist = []
            for __ in range(check_count):
                pos = self.coord()
                if not pos:
                    logger.warn('获取当前所在坐标 - 【失败】')
                    return False
                else:
                    logger.debug(f'目标坐标{target}，当前是{pos}')
                
                distance = self.getDistance(pos,target)
                if distance > validrange:
                    logger.warn(f'目标坐标超过合法值{validrange}，当前是{distance}')
                    return False
                if distance <= blur:
                    logger.debug('到达了')
                    return True

                if maplimit:
                    is_valid_map = self.searchFunction(maplimit)
                    if not is_valid_map:
                        logger.warn(f'当前移动限定在{maplimit},目前不在指定地图')
                        return False


                logger.info(f'当前坐标是{pos}')

                poslist.append(pos)
                if len(poslist) > 3:
                    if poslist[-1] == poslist[-2] and poslist[-2] == poslist[-3]:
                        logger.warn('卡墙了，一动也不动了')
                        return True
                # 获取坐标。获取不到就抛异常
                offsetx = target[0] - pos[0]
                offsety = target[1] - pos[1]
                if distance>limitrange:
                    logger.debug('超出距离 %i' % (distance-limitrange))
                    normalx,normaly = self.normalize([offsetx,offsety])
                    offsetx = int(normalx*limitrange)
                    offsety = int(normaly*limitrange)
                
                logger.debug('将要进行移动')
                self.moveTo(offsetx,offsety,withclick=True,duration=duration,withhold=False)
                
                if limitrange <= distance:
                    time.sleep(1)
                else:
                    if check_count > 1:
                        time.sleep(1)
                        for i in range(100):
                            ans = self.ismove()
                            if not ans:
                                break
                    

        
        return False



            # 看来，要重新规划坐标系了


    def ismove(self):
        
        if self.win.status == 'peace':

            offsets = {'y':65,'x':-43}
            try:
                x = self.get_number(offsets['x'])
            except:
                x = -1

            try:
                y = self.get_number(offsets['y'])
            except:
                y = -1

            if x != self.last_x:
                x_true = False
                self.last_x = x
            else:
                if x < 0:
                    x_true = False
                else:
                    x_true = True

            
            if y != self.last_y:
                y_true = False
                self.last_y = y
            else:
                if y < 0:
                    y_true = False
                else:
                    y_true = True

            if x_true and y_true:
                return False
            else:
                return True
        else:
            return False

    def get_direction_of_random_map(self,istest=False):
        map_pos = self.pos
        center_x = map_pos[0]-13
        center_y = map_pos[1]+74
        length = 50
        height = 50
        bbox_orig = (int(center_x-(length*0.5)),int(center_y-(height*0.5)),length,height)
        im = pyautogui.screenshot(region=bbox_orig)
        pixels = im.load()
        dir_dict = {'w':{'val':[-1,0.5],'color':(0,255,0)},
                    'e':{'val':[1,-0.5],'color':(255,255,0)},
                    'n':{'val':[-1,-0.5],'color':(0,255,255)},
                    's':{'val':[1,0.5],'color':(0,0,255)}}
        
        lenth_dict = {}
        for key,val in dir_dict.items():
            direction = val['val']
            length_count = 2
            for step in range(2,25):
                sc_center_x = length*0.5
                sc_center_y = height*0.5
                draw_x = sc_center_x + (direction[0]*step)
                draw_y = sc_center_y + (direction[1]*step)

                sum_val = sum(pixels[draw_x,draw_y])
                
                if sum_val>5:
                    if istest:
                        # print(key,step,sum_val)
                        pixels[draw_x,draw_y] = val['color']
                    length_count+=1
                else:
                    break
            lenth_dict[key] = length_count

        if istest:
            im.save(r'd:/gg.png')
            print(lenth_dict)
        im.close()

        maxval = 0
        default_dir = ''
        for key,val in lenth_dict.items():
            if val > maxval:
                default_dir = key
                maxval = val
        return default_dir

    def serialize(self):
        for i in range(5):
            self.goto([225,90],check=2)


            
if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    test = coordsys(m)
    ans = test.pos()
    print (ans)
    # rlt = test.codeOperation([1,1])
    # test.goto([215,118])
    # test.moveTo('A7')
    # test.moveTo(0,2)
    # test.walkTo('wn',distance=3)