#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-03-30 23:00:14
LastEditTime: 2024-05-26 17:19:32
LastEditors: figo - uz
Description: 
copyright: figo software 2020-2021
'''


import sys
sys.path.insert (0,r'C:\gitlab\cgclicker')
import os
import math
import win32gui

from bin import win_status
from util import dialog_moniter

windows_list = []
class cgwindows():

    def __init__(self):
        self.wins = []
        
    def update(self):
        '''
        description: 最接近0,0位置的是main窗口
        return {*}
        '''
        global windows_list
        windows_list = []

        
        win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), windows_list)
        pureData = []

        for window in windows_list:
            classname = win32gui.GetClassName(window)
            if classname == '魔力宝贝':
                win_handles = win_status.win_status(window)
                win_handles.set_handle(window)
                self.wins.append(win_handles)

        self.wins.sort(key=lambda x:x.distance)
        for count,i in enumerate(self.wins):
            i.set_windows_id(count)

        return True

    @property
    def windows(self):
        self.update()
        return self.wins

    # def getPixel(self,dc,x,y):
    #     long_colour = win32gui.GetPixel(dc, x, y)
    #     i_colour = int(long_colour)
    #     return [(i_colour & 0xff), ((i_colour >> 8) & 0xff), ((i_colour >> 16) & 0xff)]


if __name__ == "__main__":
    import sys
    sys.path.insert (0,r'C:\gitlab\cgclicker')
    cgw = cgwindows()
    # cgw.windows[0].testfn()
    # cgw.update()
    for wd in cgw.windows:
        
        print(wd.serialize())
    # ddm = dialog_moniter.dialog_moniter(cgw.windows[0])
    # print (ddm.isMigongshuaxin())
