#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-29 05:58:29
LastEditTime: 2024-05-04 00:28:26
LastEditors: figo - uz
Description: 宠邮相关的邮件系统
inhert: basic.basic
copyright: figo software 2020-2021
'''
import random
import os
import sys
sys.path.append('.')
import time
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('mailsys')
logger.setLevel(logging.INFO)

import pyautogui
from util import module_bag

class mailsys():
    def __init__(self,win) :
        super(mailsys,self).__init__()
        self.win = win

        rootPath = self.win.root_path()
        self.sourcepath = '%s\\resources\\location' % rootPath

        self.mingpian_static_pos = [170,80]
        self.xinjian_static_pos = [470,80]

        self.bag = module_bag.itemSystem(self.win)


    def set_mingpian(self,status):
        '''
        description: 打开名片
        return {*}
        '''        
        if not isinstance(status,bool):
            raise ('输入数据错误，status必须是bool')
        

        for i in range(3):
            try:
                ans = self.mingpian_pos  
            except:
                ans = None
            finally:
                pass

            
            if status == bool(ans):
                logger.info(f'背包设置状态为[{status}]')
                self.reset_mingpian_pos()
                return True
            else:
                pyautogui.keyDown('ctrl') 
                pyautogui.press('t') 
                pyautogui.keyUp('ctrl')
        return False


    def write_mail(self,message="cg_assist  "):
        if self.xinjian_pos:
            pyautogui.moveTo(self.xinjian_pos)
            pyautogui.moveRel(0,80)
            pyautogui.click()
            pyautogui.typewrite(message)

    def as_mail(self):
        for i in range(3):
            base_pos = self.xinjian_pos
            if (not base_pos) :
                base_pos = self.pet_xinjian_pos
                if (not base_pos):
                    return True
            x,y = base_pos
            newpos = [x+70,y+15]
            pyautogui.moveTo(newpos)
            pyautogui.click()


    def as_pet_mail(self):
        for i in range(3):
            base_pos = self.xinjian_pos
            pyautogui.moveTo(base_pos)
            relpos = [-50,15]
            pyautogui.moveRel(relpos)
            pyautogui.click()
            time.sleep(1)
            if self.pet_xinjian_pos:
                self.reset_pet_xinjian_pos()
                return True
        return False


    def send_mail(self,slot=1):
        for i in range(3):
            base_pos = self.mingpian_pos
            pyautogui.moveTo(base_pos)
            relpos = [80,110]
            pyautogui.moveRel(relpos)
            pyautogui.click()
            time.sleep(1)
            if self.xinjian_pos:
                self.reset_xinjian_pos()
                self.write_mail()
                return True
        return False

    def reset_pet_xinjian_pos(self,pos = []):
        '''
        description: 将地图窗口移动到指定位置
        return {*}
        '''     
        thepos = self.pet_xinjian_pos
        print ('thepos',thepos)
        if pos:
            tx = self.win.localx(pos[0])
            ty = self.win.localy(pos[1])
        else:
            msp = self.xinjian_static_pos
            print ('msp',msp)
            tx=self.win.localx(msp[0])
            ty=self.win.localy(msp[1])


        isSuccess = False
        for i in range(5):
            logger.info('moveloop[%i] ' % i)
            baseinfo = self.pet_xinjian_pos
            if not baseinfo:
                pyautogui.moveRel(50,50)
                time.sleep(2)
                continue
            sx = baseinfo[0]
            sy = baseinfo[1]

            offset_x = tx-sx
            offset_y = ty-sy

            if (offset_x + offset_y) == 0:
                logger.info('done')
                isSuccess = True
                break
            else:
                logger.info('offset [%i,%i]' % (offset_x ,offset_y))
                pyautogui.moveTo(sx,sy,duration=0.2)
                pyautogui.dragRel(offset_x,offset_y,duration=1)
        return isSuccess

    def reset_xinjian_pos(self,pos = []):
        '''
        description: 将地图窗口移动到指定位置
        return {*}
        '''     
        thepos = self.xinjian_pos
        print ('thepos',thepos)
        if pos:
            tx = self.win.localx(pos[0])
            ty = self.win.localy(pos[1])
        else:
            msp = self.xinjian_static_pos
            print ('msp',msp)
            tx=self.win.localx(msp[0])
            ty=self.win.localy(msp[1])


        isSuccess = False
        for i in range(5):
            logger.info('moveloop[%i] ' % i)
            baseinfo = self.xinjian_pos
            if not baseinfo:
                pyautogui.moveRel(50,50)
                time.sleep(2)
                continue
            sx = baseinfo[0]
            sy = baseinfo[1]

            offset_x = tx-sx
            offset_y = ty-sy

            if (offset_x + offset_y) == 0:
                logger.info('done')
                isSuccess = True
                break
            else:
                logger.info('offset [%i,%i]' % (offset_x ,offset_y))
                pyautogui.moveTo(sx,sy,duration=0.2)
                pyautogui.dragRel(offset_x,offset_y,duration=1)
        return isSuccess

    def reset_mingpian_pos(self,pos = []):
        '''
        description: 将地图窗口移动到指定位置
        return {*}
        '''     
        thepos = self.mingpian_pos
        print ('thepos',thepos)
        if pos:
            tx = self.win.localx(pos[0])
            ty = self.win.localy(pos[1])
        else:
            msp = self.mingpian_static_pos
            tx=self.win.localx(msp[0])
            ty=self.win.localy(msp[1])


        isSuccess = False
        for i in range(5):
            logger.info('moveloop[%i] ' % i)
            baseinfo = self.mingpian_pos
            if not baseinfo:
                pyautogui.moveRel(50,50)
                time.sleep(0.2)
                continue
            sx = baseinfo[0]
            sy = baseinfo[1]

            offset_x = tx-sx
            offset_y = ty-sy

            if (offset_x + offset_y) == 0:
                logger.info('done')
                isSuccess = True
                break
            else:
                logger.info('offset [%i,%i]' % (offset_x ,offset_y))
                pyautogui.moveTo(sx,sy,duration=0.2)
                pyautogui.dragRel(offset_x,offset_y,duration=1)
        return isSuccess


    @property
    def mingpian_pos(self):
        '''
        description: 地图模块的坐标
        return {*}
        '''   
        onmark = 'mingpian.png'
        onPath = '%s\\%s' % (self.sourcepath,onmark)

        try:
            ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect)
        except:
            return False
        finally:
            pass
        return ans


    @property
    def xinjian_pos(self):
        '''
        description: 地图模块的坐标
        return {*}
        '''   
        onmark = 'xinjian.png'
        onPath = '%s\\%s' % (self.sourcepath,onmark)

        try:
            ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect)
        except:
            return False
        finally:
            pass
        return ans

    @property
    def pet_xinjian_pos(self):
        '''
        description: 地图模块的坐标
        return {*}
        '''   
        onmark = 'pet_xinjian.png'
        onPath = '%s\\%s' % (self.sourcepath,onmark)

        try:
            ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect)
        except:
            return False
        finally:
            pass
        return ans


    def load_pet_mail_item(self,itemmark):
        basepos = self.pet_xinjian_pos
        if not basepos:
            return False
        x,y = basepos
        targetpos = [x+70,y+200]
        #pyautogui.moveTo(newpos)

        

        data = self.bag.farm_indentify()
        for i in data:
            if i[1] == itemmark:
                pyautogui.click(i[2])
                time.sleep(1)
                pyautogui.click(targetpos)
                self.as_mail()
                
                return True
            
    def send_pet_mail(self,itemname):
        self.set_mingpian(True)
        self.send_mail()
        self.as_pet_mail()
        self.load_pet_mail_item(itemname)


    def serialize(self):
        self.send_pet_mail('tong.png')
        # print (self.mingpian_pos)
        # print (self.win.localpos(self.mingpian_static_pos))


if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    print (m)
    whr = mailsys(m)
    # whr.logNumber()
    gets = whr.pos()
    
    # gets = whr.pos()
    print (gets)