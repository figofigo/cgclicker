#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-05-25 20:31:30
LastEditTime: 2024-06-02 22:02:34
LastEditors: figo - uz
Description: 
copyright: figo software 2020-2021
'''



import pyautogui
import time
import os
import random
import logging
import sys
sys.path.append('.')

from util import util

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('module_duihua')
logger.setLevel(logging.INFO)

class module_duihua():
    def __init__(self,win):
        super(module_duihua,self).__init__()
        self.win = win
        # self.rect = self.win.rect

        self.locatepath = '%s\\resources\\duihua' % self.win.root_path()

        self.rest_pos = [5,5]
        # 这个是每一个slot的偏移量
    def isduihua(self):
        marklist = ["left_up.png","right_dn.png"] # ["right_up.png","left_dn.png"]
        poslist = []
        for onmark in marklist:
            #onmark = 'title.png'
            onPath = '%s\\%s' % (self.locatepath,onmark)
            ans = None
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.win.rect) 
                
            except:
                pass
            finally:
                poslist.append(ans)
        if None in poslist:
            logger.warn('数据缺失')
            return False
        else:
            width = poslist[1][0] - poslist[0][0]
            height =poslist[1][1] - poslist[0][1]
            self.rest_pos = [poslist[1][0] + 10,poslist[1][1] + 10]
            pyautogui.moveTo(self.rest_pos)
            region = (poslist[0][0],poslist[0][1],width,height)
            logger.info(region)
            return region

    def wait_duihua(self,loops=100):
        for i in range(loops):
            ans = self.isduihua()
            if ans:
                return True
            else:
                time.sleep(0.1)
        return False

    def hospital_heal_main(self,selection):
        '''
        description: 补血主界面的操作
        selection {str} "both","red","pet","cancel" 三个选项
        return {*}
        '''        
        # 补血选择的位置需要随机化。容易掉线
        isstart = self.isduihua()
        if isstart:
            onmark = 'hospital_heal_main.png'
            onPath = '%s\\%s' % (self.locatepath,onmark)
            ans = None
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = isstart) 
            except:
                pass
            finally:
                pass

            if ans:
                if selection == 'q':
                    return True                
                elif selection == 'both':
                    click_pos = [ans[0],ans[1]+40]
                    click_pos = self.win.wrangle_pos(click_pos)
                elif selection == 'red':
                    click_pos = [ans[0],ans[1]+80]
                    click_pos = self.win.wrangle_pos(click_pos)
                elif selection == 'pet':                    
                    click_pos = [ans[0],ans[1]+120]
                    click_pos = self.win.wrangle_pos(click_pos)
                else:                    
                    click_pos = [ans[0],ans[1]+160]
                    click_pos = self.win.wrangle_pos(click_pos)

                pyautogui.moveTo(click_pos)
                time.sleep(0.1)
                pyautogui.click(click_pos,duration=0.4)
                pyautogui.moveTo(self.rest_pos)
                return True  
            else:
                logger.warn('没有找到医院恢复对话框的标记')
                return False
        else:
            logger.warn('对话窗口没有打开')
            return False

    def select_yes(self):
        self.wait_duihua()
        gets = self.select_function('shi.png')
        return gets
    
    def select_next(self):
        self.wait_duihua()
        gets = self.select_function('next.png')
        return gets
    
    def select_no(self):
        self.wait_duihua()
        gets = self.select_function('fou.png')
        return gets
    
    def select_ok(self):
        self.wait_duihua()
        gets = self.select_function('queding.png')
        return gets
    
    def select_cancel(self):
        self.wait_duihua()
        gets = self.select_function('quxiao.png')
        return gets

    def select_function(self,onmark):
        
        isstart = self.isduihua()
        if isstart:
            # onmark = 'hospital_heal_main.png'
            onPath = '%s\\%s' % (self.locatepath,onmark)
            ans = None
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = isstart) 
            except:
                pass
            finally:
                pass

            if ans:
                clickpos = self.win.wrangle_pos([ans.x,ans.y])
                pyautogui.click(clickpos,duration=0.5)
                pyautogui.moveTo(self.rest_pos)
                logger.info('select %s ... done' % onmark)
                return True
            else:
                logger.warn('select %s ... fail' % onmark)
        else:
            logger.warn('对话窗口没有打开')
        return False
    
    def hospital_heal(self):        
        '''
        description: 医院回血的主入口
        param {*} self
        return {*}
        '''
        check = self.hospital_heal_main('q')
        if check:
            self.hospital_heal_main('both')
            self.select_yes()
            self.select_ok()
            self.wait_duihua()

            self.hospital_heal_main('red')
            self.select_yes()            
            self.select_ok()
            self.wait_duihua()

            self.hospital_heal_main('pet')
            self.select_yes()
            self.select_ok()
            self.wait_duihua()
            
            self.hospital_heal_main('cancel')


    def serialize(self):
        pass

if __name__ == "__main__":
    duihua = module_duihua()
    duihua.isduihua()