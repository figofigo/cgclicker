

<!--
 * @Author: figo
 * @Date: 2022-12-04 20:22:10
 * @LastEditTime: 2022-12-04 22:06:51
 * @LastEditors: figo - uz
 * @Description: 
 * @FilePath: \cgclicker\README.md
 * @copyright: figo software 2020-2021
-->
# 魔力宝贝点击器  
## 有些话，作者想先说清楚  
1. 这些功能都是不要钱的  
2. 这是一个基于视觉算法制作的游戏辅助外挂。  
3. 他的稳定性不好  
4. 使用了win32库，但是没有木马功能，请自行鉴别    
5. 这是一个违反易网通运营规则的辅助，有封号风险。  


请自行选择是否要使用，作者并**不会**为你负责。  
作者只是**为了自己爽**才写的代码。
  
- - -  
- - -  
  
## 环境配置  

代码基于python3.7运行  
https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe  

python安装时，其他的都按照默认安装  
选择路径的那一步，一定要勾选  
+ 将python加入PATH  
  
## 第三方包  
+ pyautogui  
+ pywin32  
+ pyutils  
+ keyboard

安装方法：  
按win+R键打开“运行”，输入cmd后回车  
在命令行中输入下面三行命令  
pip install pyautogui  
pip install pywin32  
pip install pyutils    
pip install keyboard  

- - -  
- - -  
## 第一次运行  
1. 在画面右上角橙色按钮中，下载代码  
2. 解压缩代码到c盘gitlab文件夹中，没有的话请自行创建,结构如下  
C:  
|- gitlab  
|&emsp;|- cgclicker  
|&emsp;|&emsp;|- action  
|&emsp;|&emsp;|- bin  
|&emsp;|&emsp;|- resouces  
|&emsp;|&emsp;|- start.py  
|&emsp;|&emsp;|- path.py  
................  

3. 打开cmd,输入命令进入文件夹  
   cd c:/gitlab/cgclicker  
4. 输入启动命令，开启功能  
   具体命令见“命令列表”  
5. 中断的方法： 
   + 按住alt键，直到代码停止  
   + 选择cmd窗口，按ctrl+c 强制结束功能

- - -  
- - -  
## 命令列表  

### 队员保持功能：
+ 战斗时，在动画阶段刷新自动战斗  
+ 非战斗时，识别当前魔法量，低于设定百分比时吃料理（面包，法面，锅子）  
+ 非战斗时，识别是否处在护士对话界面，若对话则自动执行补血  

**python start.py maintain <补血百分比> <补蓝百分比>**  
**python start.py maintain 0.1 0.2**
* * *  
### 领队功能(更新后失效，修复中）：
+ 自动使用人物1号技能的最高级别  
+ 使用过程中不能碰鼠标
+ 按住alt中断功能  
  
**python start.py leader <变量>**  

变量列表：  
banshan - 在半山带队，到达半山之后，以坐标70.63为中心随机移动  
jinzhan - 在当前位置，绕四方格子，距离为3，攻击时优先前排  
moren - 在当前位置，绕四方格子，距离为3  
* * *  
### 烧技能功能(更新后失效，修复中）：
+ 目前只有西尔维，在门口转圈。我还去不了火洞,就没写  
+ 功能现在比较稳了，但是还是有问题  
+ 使用过程中不能动鼠标和键盘  
+ 无目标指的是 <圣盾/防御/什么都不做/魔法防御>  
+ 烧技能的时候最好穿一件5闪躲的国民衣
+ 战斗时，技能选择框一定要放右边不要挡住左边怪物，会影响识别  
+ 自动战斗一定要放在左下角，但是不要出框，以便识别。  
  

**python start.py skill xierwei <变量>**  

变量列表：  
wushi - 人物 <技能1> 对己方，宠物 <技能2> 无目标，低于300血或100蓝回补  
moshu - 人物 <技能1> 对敌方，宠物 <技能2> 无目标，低于500血或300蓝回补  
gongjian - 人物 <技能1> 对敌方，宠物 <技能1> 对敌方，低于300血或100蓝回补  
jianshi - 人物 <技能1> 对敌方【近战优先】，宠物 <技能1> 对敌方，低于300血或100蓝回补 
moren - 人物 <技能1> 对敌方【近战优先】，宠物 <技能2> 无目标，低于300血或100蓝回补 

- - -  
- - -  

## 反馈给我的问题：
无
