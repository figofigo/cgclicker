#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-04-29 05:49:06
LastEditTime: 2025-02-17 23:46:40
LastEditors: figo - uz
Description: 
FilePath: \cgclicker\start.py
copyright: figo software 2020-2021
'''

import sys
import math
import logging
sys.path.append('.')
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('cgclicker')
logger.setLevel(logging.DEBUG)
import time
import random

import sys
sys.path.insert (0,r'C:\gitlab\cgclicker')
from action import maintain_new as mt

from action import pet_mail 

from action import huodong_skill

def main(args):
    if args[0] == 'leader':
        pass
    elif args[0] == 'skill':
        if args[1] == 'huodong':
            skillbag = huodong_skill.skill_huodong()
            if args[2] == 'card':
                skillbag.card()
            elif args[2] == 'vellage':
                skillbag.vellage()
            else:
                skillbag.main()
    elif args[0] == 'petmail':
        mtcls = pet_mail.petmail_sys(args[1])
        mtcls.main()
  
    elif args[0] == 'maintain':
        mtcls = mt.maintain(redlow=float(args[1]),bluelow=float(args[2]))
        mtcls.main()
        # mt.start(food = int())
    else:
        pass

if __name__ == '__main__':
    main(sys.argv[1:])




    # import os
    # import re
    # import time
    # def edit_renderpath(inputstr):
    #     date_time = time.strftime("%Y_%m_%d_%H_%M",time.localtime())
    #     spstr = inputstr.split('-')
    #     seq = str(spstr[0]).zfill(2)
    #     shot = str(spstr[1]).zfill(3)
    #     sub = str(spstr[2]).zfill(2)
        
    #     inputfile = f"S:/pipeData/pipePrj/LABILE/shots/E01S{seq}/E01S{seq}_C{shot}_{sub}/LIGHTING/default/Main/LAB_lgt_E01S{seq}_C{shot}_{sub}.ma"
    #     # outputfile =f"S:/pipeData/pipePrj/LABILE/shots/E01S{seq}/E01S{seq}_C{shot}_{sub}/LIGHTING/default/Main/LAB_lgt_E01S{seq}_C{shot}_{sub}_renderbus.ma"
    #     outputfile =f"T:/cache/renderbus/LABILE/{date_time}/LAB_lgt_E01S{seq}_C{shot}_{sub}_renderbus.ma"
    #     # mainfolder = f"S:/pipeData/pipePrj/LABILE/shots/E01S{seq}/E01S{seq}_C{shot}_{sub}/LIGHTING/default/Main"
    #     outputfolder = f"T:/cache/renderbus/LABILE/{date_time}"
    #     if not os.path.exists(outputfolder):
    #         os.makedirs(outputfolder)
    #     if os.path.exists(inputfile):
    #         alllines = []
    #         newlines = []
    #         print('正在读取文件')
    #         print(inputfile)
    #         with open(inputfile,'r') as f:
    #             alllines = f.readlines()
    #             print('开始修改')
    #             for line in alllines:
    #                 if ('.imageFilePrefix' in line) or ("setAttr \".ifp\"" in line):
    #                     line ="	setAttr \".imageFilePrefix\" -type \"string\" \"<scene>\\<renderLayer>\\<renderLayer>\";\n"
    #                 if 'D:/s08/ENV_FILL.ma' in line:
    #                     line = line.replace('D:/s08/ENV_FILL.ma',"S:/pipeData/pipePrj/LABILE/shots/E01S08/E01S08_CEnv/ENVLAYOUT/default/Main/ENV_FILL.ma")
    #                 if 'renderLayerManager.rlmi' in line:
    #                     line = "connectAttr \"renderLayerManager.rlmi[0]\" \"defaultRenderLayer.rlid\";\n"
                        
    #                 newlines.append(line)
    #         print('开始保存文件,需要较长的时间，请耐心等待')
    #         print('保存完成后会自动弹出文件夹')
    #         if os.path.exists(outputfile):
    #             os.remove(outputfile)
    #         with open(outputfile,'w') as f:
    #             f.writelines(newlines)
    #     print('done')
    #     return outputfolder

    # inputlist = ['9-20-00',
    # '9-22-00',
    # '13-02-00',
    # '9-28-00',
    # '13-03-00',
    # '9-26-00',
    # '9-40-00',
    # '8-27-00',
    # '9-50-00',
    # '9-51-00',
    # '9-70-00',
    # '15-01-00',
    # '15-02-00',
    # '15-04-00',
    # '16-05-00',
    # '15-03-00',
    # '15-07-00',
    # '15-06-00',
    # '16-08-00',
    # '15-08-00',
    # '15-10-00',
    # '15-09-00',
    # '16-12-00',
    # '16-10-00',
    # '16-14-00',
    # '15-11-00',
    # '16-13-00',
    # '16-02-00',
    # '09-23-00',
    # '08-41-00',
    # '16-01-00',
    # '16-03-00',
    # '16-06-00',
    # '16-04-00',
    # '16-11-00',
    # '16-11-20',
    # '16-11-40',
    # '16-11-60'
    # ]
    # allans = []
    # for inputstr in inputlist:
    #     ans = edit_renderpath(inputstr)
    #     allans.append(ans)
    # allans = list(set(allans))
    # os.startfile(allans[0])


