#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-04-30 18:21:55
LastEditTime: 2024-01-06 19:11:37
LastEditors: figo - uz
Description: 在新村（西尔维村）烧技能
copyright: figo software 2020-2021
'''

import time
import os
import sys
sys.path.append('.')
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('xierwei_skill')
logger.setLevel(logging.DEBUG)

from bin import opreate_charater as opc
from bin import walk as coordsys
from bin import hospital_heal


class xierweiSkill(opc.opreate_character):
    def __init__(self,rect,wholeLoop = 50000) :
        super(xierweiSkill,self).__init__(rect)
        self.wholeLoop = wholeLoop
        self.rect = self.real_rect(rect=rect)
        self.defaultskill = {'human':[1,'enermy'],'pet':[2,None]}
        self.limitrange = 4 if self.wintype == 640 else 6
        self.hospital = hospital_heal.Hospital(rect)
        self.coordsys = coordsys.walksys(rect,mark='xierwei')

    def gotoGate(self):
        '''
        description: 开始点在医院门口
        param {*}
        return {*}
        '''  
        self.coordsys.walkTo('wn',distance=4)

        for i in range(5):
            ans = self.coordsys.goto([51,47])
            if ans:
                break
        
        for i in range(5):
            self.coordsys.walkTo('n',distance=4)
            mapcheck = self.coordsys.isMatata()
            if mapcheck:
                logger.info('到村口了')
                return True
        return False

    def gotoWork(self):
        '''
        description: 出门不定点，但是回来的时候需要，出门先截图
        return {*}
        '''      
        # self.logReturnMark()
        self.coordsys.goto([352,198],limitrange=self.limitrange)
        
        self.coordsys.goto([353,195],limitrange=self.limitrange)

        #self.battle.battle_control(defaultskill=self.defaultskill) # 等8秒看看有没有战斗

        logger.info('到达了练功点')
        return True
        #389 320



    def backToGate(self):
        self.battleSys.setAutoBattle(False,f=1)
        #351,203
        for i in range(100):
            nowstatus = self.status()
            print ('backing gate',nowstatus)
            if nowstatus == 'battle_sel':
                self.battleSys.escape()
            elif nowstatus == 'pet':
                self.battleSys.petCastSkill(2,None)
            elif nowstatus == 'peace':                
                isok = self.coordsys.goto([351,200],limitrange=self.limitrange)
                if isok:
                    break
                # mapcheck = self.coordsys.isXierwei()
                # if mapcheck:
                #     logger.info('到了到了到了')
                #     return True
            else:
                time.sleep(0.1)
        print ('phase 2')

        for i in range(100):
            nowst = self.status()
            if nowst == 'battle_sel':
                self.battleSys.escape()
            elif nowst == 'pet':
                self.battleSys.petCastSkill(2,None)
            elif nowst == 'peace':                
                self.coordsys.walkTo('s',distance=4)
                mapcheck = self.coordsys.isXierwei()
                if mapcheck:
                    logger.info('到了到了到了')
                    return True
            else:
                pass
        return False


    def gotoHospital(self):
        '''
        description: 在村子里，去医院
        return {*}
        '''        
        for i in range(2):
            self.coordsys.walkTo('s',distance=3)
            # time.sleep(0.1)

        for i in range(10):
            isok = self.coordsys.goto([51,46])
            if isok:
                break

        logger.info('goto mid village ok')

        for i in range(5):
            self.coordsys.walkTo('es',distance=3)
            mapcheck = self.coordsys.isHospital()
            if mapcheck:
                logger.info('到了到了到了')
                return True


        logger.info('没进村啊。咋回事呢')
        return False
        
    def gotoNurse(self):
        '''
        description: 去找医生
        return {*}
        '''
        self.coordsys.walkTo('ne',distance=2)
        self.coordsys.walkTo('e',distance=3)

        for i in range(10):
            isok = self.coordsys.goto([17,12])
            if isok:
                break
                #return True
            else:
                time.sleep(1)
        logger.info('goto Nurse.finish')
        return True

    def leaveHospital(self):
        '''
        description: 离开医院
        return {*}
        '''
        self.coordsys.walkTo('ws',distance=4)
        for i in range(5):
            # print ('test',i)
            self.coordsys.goto([8,14])
            # print ('888')
            check = self.coordsys.isXierwei()
            # print ('999')
            if check:
                return True
            # print ('000')

        return False


    def refill(self):
        logger.info('等待战斗结束')
        for i in range (500):
            nowst = self.status()
            print ('wait',i,nowst)
            
            if nowst == 'battle_sel':
                self.battleSys.escape()
            elif nowst == 'pet':
                self.battleSys.petCastSkill(2,None)
            elif nowst == 'peace':
                break
            else:
                time.sleep(0.1)
        logger.info('back To Gate.start')
        self.waitbreak()
        self.backToGate()

        logger.info('back To Gate.finish')
        self.battleSys.setAutoBattle(False,renew=0)

        self.walktime_prove()
        return True


    def get_skill_train_set(self,mark):
        rtdata = {}
        if mark == 'wushi':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':400,
                        'blue_check_line':250,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'self'],'pet':[2,None]},
                        
                        
                    }
        elif mark == 'qijue':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':400,
                        'blue_check_line':250,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'self'],'pet':[2,None]},
                        
                        
                    }
        elif mark == 'moshu':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':500,
                        'blue_check_line':300,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        
                        
                    }
        elif mark == 'daixiaohao':
            rtdata= {                    
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':500,
                        'blue_check_line':300,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[1,'enermy']},
                        
                        
                    }
        elif mark == 'gongjian':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':300,
                        'blue_check_line':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[1,'enermy']},
                        
                        
                    }
        elif mark == 'jianshi':
            rtdata= {                    
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':300,
                        'blue_check_line':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        
                        
                    }
        else:
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':300,
                        'blue_check_line':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        
                        
                    }

        self.defaultskill = rtdata['defaultSkill']
        self.set_init_data(rtdata)
        return rtdata

    def peace_action(self):
        # super().peace_action()
        self.check_blue()
        self.check_red()
        self.coordsys.main()

    def battlesel_action(self):
        if self.reload_ready:
            self.battleSys.setAutoBattle(False)
            humanskill = self.defaultskill['human']
            self.battleSys.humanCastSkill(humanskill[0],target=humanskill[1])
            self.reload_ready = False

        self.battleSys.setAutoBattle(True)
        self.battleSys.reset_autobattle_pos()


    def pet_action(self):
        '''
        description: 宠物选择技能阶段，默认是使用1号技能攻击敌人
        return {*}
        '''       
        petskill = self.defaultskill['pet']
        print ('using pet skill',petskill)
        self.battleSys.petCastSkill(petskill[0],target=petskill[1]) 

    def start_work(self,mark = 'gedou',istest=False):
        '''
        description: 开始点在村口外，玛塔塔平原 351.203 
        param {*}
        return {*}
        '''   
        self.get_skill_train_set(mark)

        # self.gotoWork()  
        self.coordsys.walkTo('n',distance=4)
        self.reload_ready = True
        isLoopDone = False
        for i in range(self.wholeLoop):   
            logger.info('============================')
            logger.info(u'开始第 [%i] 轮循环' % i)
            logger.info('============================')
            
            if self.is_blue_low :
                logger.info('蓝量太少了')
                isLoopDone = True
            elif self.is_red_low:
                logger.info('血量太少了')
                isLoopDone = True
            else:
                isLoopDone = self.main()

            logger.info('singleLoop.done')
            if isLoopDone:
                logger.info('refill..start')
                self.refill()
                self.reload_ready = True
                isLoopDone = False
                self.coordsys.walkTo('n',distance=4)
                logger.info('refill...done')
            self.waitbreak()
                
    def walktime_prove(self):
        sttime = time.time()

        logger.info('back To Gate.finish')
        self.battleSys.setAutoBattle(False,renew=0)
        self.waitbreak()
        self.gotoHospital()
        
        mark1 = time.time()
        logger.info('gotoHospital use time.%s' % (str(mark1-sttime)))

        logger.info('goto Nurse.start')

        # for i in range(3):
        self.gotoNurse()
        mark2 = time.time()
        logger.info('gotoNurse use time.%s' % (str(mark2-mark1)))


        self.coordsys.walkTo(direction='n',button='right')
        ans = self.hospital.heal()
        self.check_blue()
        self.check_red()
        logger.info('nowred is %s' % str(self.red_now))
        logger.info('nowblue is %s' % str(self.blue_now))

        logger.info('leave Hospital.start')
        self.waitbreak()
        mark3 = time.time()
        logger.info('heal use time.%s' % (str(mark3-mark2)))

        self.leaveHospital()
        mark4 = time.time()
        logger.info('leaveHospital use time.%s' % (str(mark4-mark3)))

        # self.wait(1)
        self.waitbreak()
        self.gotoGate()
        mark5 = time.time()
        logger.info('gotoGate use time.%s' % (str(mark5-mark4)))

        self.waitbreak()
        logger.info('whole time use time.%s' % (str(mark5-sttime)))
        

if __name__ == "__main__":
    from bin import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    test = xierweiSkill(rect = m)
    test.get_skill_train_set('moshu')
    test.walktime_prove()
