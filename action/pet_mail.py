#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-04-09 01:45:38
LastEditTime: 2024-05-04 20:44:14
LastEditors: figo - uz
Description: 宠邮物品
copyright: figo software 2020-2021
'''

import logging
import sys
sys.path.insert (0,r'C:\gitlab\cgclicker')
from util import getArea
from util import dialog_moniter

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('petmail_sys _func')
logger.setLevel(logging.INFO)

class petmail_sys(getArea.cgwindows):
    def __init__(self,mail_item):
        super().__init__()
        self.leadwin = None
        self.mail_item = mail_item
    # def analy(self,data):
    #     action_list = []
    #     data['warn'] = []

    #     red_percent = round(data['red']/data['red_max'],2)
    #     blue_percent = round(data['blue']/data['blue_max'],2)
    #     if red_percent <= self.redlow and data['red']>=0:
    #         data['warn'].append('red low %s' % str(red_percent))
    #     if blue_percent <= self.bluelow and data['blue']>=0:
    #         data['warn'].append('blue low %s' % str(blue_percent))
    #         action_list.append('eat_blue')
        
        

    #     if 'count' in data.keys():
    #         checkline = 300
    #         if data['count'] < checkline:
    #             data['warn'].append('auto battle check [ %i/%i ],recount' % (data['count'],checkline))
    #             action_list.append('recount')
    #         else:
    #             print ('auto battle count [ %i/%i ]' % (data['count'],checkline))
    #     else:
    #         print('no [  count  ] attr')
    #     self.displaydata(data)
    #     return action_list

    # def action(self,wd,act,dt):
    #     if act:
    #         for aa in act:
    #             if aa == 'eat_blue':
    #                 logger.info('start eat blue')
    #                 self.stopwalking()
    #                 wd.set_to_top()
    #                 wd.bag.set_bag(True)
    #                 isfast = True
    #                 iseat = wd.bag.eat(blue_or_red='blue',isfast=isfast)
    #                 if iseat:
    #                     # 此处增加一个归位的功能，避免角色因为吃蓝偏移目标点
    #                     self.startwalking()
    #             elif aa =='recount':
    #                 isexitst = wd.autobattle.isAutoBattle()
    #                 if not isexitst:
    #                     wd.autobattle.setAutoBattle(True)
                        
    #                 else:
    #                     wd.autobattle.reset_count()
    #     else:
    #         return True


    # def displaydata(self,data):
    #     print ('--- ID %s (%s,%s - %s)---' % 
    #                 (data['window_id'],
    #                  str(data['rect'][0]),
    #                  str(data['rect'][1]),
    #                  str(data['distance']),
    #                  ))
    #     print ('red :',data['red'],'|',data['red_max'],round(data['red']/data['red_max'],2)*100,'%')
    #     print ('blue:',data['blue'],'|',data['blue_max'],round(data['blue']/data['blue_max'],2)*100,'%')
    #     if data['warn']:
    #         for i in data['warn']:
    #             print (i)
    # def stopwalking(self):
    #     if self.leadwin:
    #         self.leadwin.autobattle.set_autowalk(False)

    # def startwalking(self):
    #     if self.leadwin:
    #         self.leadwin.autobattle.set_autowalk(True)


    def main(self):
        loops_win = list(self.windows)

        dialog = dialog_moniter.dialog_moniter(loops_win[0])
        # self.leadwin = loops_win[0]
        # 此处加入初始地址代码
        # 此处加入游走中心位置
        for loopcount in range(99999999999):
            for wd in loops_win:
                wd.set_to_top()
                wd.mail.send_pet_mail(self.mail_item)
            # isRefresh = dialog.isMigongshuaxin()
            # if isRefresh:
            #     loops_win[0].autobattle.set_autowalk(False)
            #     # 此处增加避难代码
            # elif loopcount%100 == 0:
            #     for wd in loops_win:
            #         self.action(wd,'recount',None)
            #     # 此处增加重置自动战斗的代码
            # else:
            #     isloopok = True
            #     for wd in loops_win:
            #         dt = wd.serialize()
            #         act = self.analy(dt)
            #         if act:
            #             isloopok = False
            #             self.action(wd,act,dt)
            #     if isloopok:
            #         self.startwalking()


if __name__ == '__main__':
    mt = petmail_sys()
    mt.main()