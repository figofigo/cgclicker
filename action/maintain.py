#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-30 16:38:16
LastEditTime: 2024-01-22 01:04:39
LastEditors: figo - uz
Description: 领队系统
copyright: figo software 2020-2021
'''


import time
import os
import shutil
import sys

import keyboard
sys.path.append(".")
print(sys.path)
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('maintain_sys')
logger.setLevel(logging.INFO)

# from bin import autoloop
# from bin import itemSystem
from bin import getArea
from bin import opreate_charater as opc


from functools import wraps

def timer(func):
    @wraps(func)
    def wrap(*args,**kwargs):
        sttime = time.perf_counter()
        result = func(*args,**kwargs)
        edtime = time.perf_counter()
        print ('func:%r,took:%2.4f sec' % (func.__name__,edtime-sttime))
        return result
    return wrap


class maintain(getArea.cgwindows):
    
    def __init__(self,loopcount=18000,loopwait=0.2,mark = 'default'):
        super(maintain,self).__init__()
        self.loopDict = {}
        self.loopcount = loopcount
        # 执行的次数，通常用while True，但是我们限制一个次数，18000次，每次0.2秒就是一小时
        self.loopwait = loopwait
        # 每次查询之间，固定要等待的时间
        
        self.update()
        if not self.main:
            raise RuntimeError ('没有魔力窗口启动')

        self.mark = mark


        self.loopDict['main'] = self.main
        self.loopDict_axis = {}
        if len(self.subs)>0:
            for i in range(len(self.subs)):
                self.loopDict[i] = self.subs[i]
        # 重组循环字典
    def init_character(self):
        
        for key,val in self.loopDict.items():
            theClass = opc.opreate_character(rect=val['size'])
            theClass.set_path(val['path'])
            datadict = self.get_maintain_set(self.mark)
            theClass.set_fast_eat(True)
            theClass.set_init_data(datadict)
            self.loopDict_axis[key] = (theClass)

    def get_maintain_set(self,mark):
        if mark == 'zhanfu':
            rdata = {'blue_check_line':150,
                        'red_check_line':300,
                        'ischeck_red':False,
                        'ischeck_blue':False
                        }
        else:
            rdata = {'blue_check_line':150,
                        'red_check_line':300,
                        'ischeck_red':False,
                        'ischeck_blue':True
                        }
        return rdata

    def start(self,iter=1,total=10000,food=600):
        logger.info('maitain start,iter:[%i],total:[%i]' % (iter,total))
        # self.init_character(food=1)
        self.init_character()
        #print (333)
        for i in range(total):
            print ('***** total : %i/%i *****' % (i,total))
            logger.debug(self.loopDict_axis)
            for key,val in self.loopDict_axis.items():
                
                val.main()
                if keyboard.is_pressed('alt'): #if key 'a' is pressed 
                    raise KeyboardInterrupt ('手动停止自动循环')

            #print (222)
            time.sleep(iter)
            logger.debug('one turn loop done')
            print ('\n')


def main(mark = 'default'):
    mn = maintain(mark=mark)
    mn.start()

if __name__ == '__main__':
    main()