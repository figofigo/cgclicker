#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-30 16:38:16
LastEditTime: 2024-02-16 22:08:44
LastEditors: figo - uz
Description: 领队系统
copyright: figo software 2020-2021
'''


import time
import os
import shutil
import sys

# import pyautogui
sys.path.append(".")
# print(sys.path)
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('leader_sys')
logger.setLevel(logging.DEBUG)

# from bin import autoloop
# from bin import itemSystem
from bin import getArea
from bin import opreate_charater as opc
from bin import walk


class leaderSystem(opc.opreate_character):
    def __init__(self,rect,loopcount=18000,loopwait=0.2):
        super(leaderSystem,self).__init__(rect=rect)
        self.rect = self.real_rect(rect)
        # self.loopDict = {}
        self.loopcount = loopcount
        # 执行的次数，通常用while True，但是我们限制一个次数，18000次，每次0.2秒就是一小时
        self.loopwait = loopwait
        # 每次查询之间，固定要等待的时间

        self.is_need_heel = False
        self.heal_line = 61
        # 是否需要队长补血

        self.reset_skill_window_pos()
        self.reset_item_window_pos()
        self.reset_map_window_pos()
        self.walksys = walk.walksys(rect)

        self.settingMark = 'default'
        # 标签直接传递给walksys决定随机移动的方式。
        # 同时用这个配置设置检查血魔线或者开启血魔检查
        self.xuanwu_alert = False
        
        self.set_attack_count = 0
        # 恢复攻击后的计数，超过3次之后不再主动设置攻击

    def getLeaderSet(self,mark):
        self.walksys.set_mark(mark=mark)
        if mark == 'xuanwu':
            self.gatepos = self.walksys.pos()
            rtdata={'blue_check_line':300,
                        'red_check_line':500,
                        'ischeck_red':True,
                        'ischeck_blue':True
                        }
                
        elif mark == 'migong':                
            rtdata= {'blue_check_line':150,
                        'red_check_line':300,
                        'ischeck_red':True,
                        'ischeck_blue':True
                        }
        
        else:
            rtdata= {'blue_check_line':150,
                        'red_check_line':300,
                        'ischeck_red':True,
                        'ischeck_blue':True
                        }
        return rtdata

    def out_xuanwu(self):
        if self.walksys.isXuanwuzhijing():
            self.walksys.goto([20,28])
            time.sleep(5)
            self.walksys.goto([20,29])
            time.sleep(15)
        if self.walksys.isXuanwu20():
            return True
        else:
            return False

    def exercise(self,istest = False):
        alllines = self.recent_chat(length=10)
        for i in alllines:
            # print (i)
            # if i['type'] == 'anno':
            if '你感觉到一股不可思议的力量' in i['chat']:
                print ('迷宫刷新，时间是',i['time_str'])
                self.say('xuanwu binan active')
                self.resucure_xuanwu()
                self.walksys.set_mark(mark='xuanwu')
                # 更新起始点
                return True
        if istest:
            for i in alllines:
                if i['type'] == 'chat':
                    if not self.xuanwu_alert:
                        if '演习开始' in i['chat']:
                            self.say('exercise start')
                            self.xuanwu_alert = True
                            return True
                    else:
                        logger.debug('避难开关已经激活')
        return False
    def resucure_xuanwu(self):
        '''
        description: 当玄武刷新时，从20层躲进玄武之境5分钟
        return {*}
        '''      
        self.say('zhunbei')
        for i in range(1000):
            if self.status() == 'peace':
                break
            elif self.status() in ['animation','battle_sel']:
                self.battleSys.setAutoBattle(True)
            else:
                time.sleep(1)



        for i in range(100):  
            self.walksys.goto(self.gatepos)
            if self.walksys.isXuanwuzhijing():
                break
        print ('等待开始')
        self.say('deng wu fen zhong')
        waitrange = 300
        for i in range(waitrange):
            print (f'等待第{i}/{waitrange}秒')
            if i%10 == 0:
                self.say('wait section %i' % i)
            time.sleep(1)
        # time.sleep(300)
        print ('等待结束')
        self.say('kai shi lian ji')
        for _ in range(20):
            ans = self.out_xuanwu()
            if ans:
                return True
        return False

    def peace_action(self):
        super().peace_action()
        self.walksys.main()

    def anim_action(self):
        super().anim_action()

        # 如果低于某条线，self.is_need_heel就被激活
        self.is_need_heel = self.battleSys.is_need_heel(line=self.heal_line)
        self.exercise()
        
    def battlesel_action(self):
        if self.is_need_heel:
            self.battleSys.setAutoBattle(False)
            isok = self.battleSys.humanCastSkill(1,target='self')   
            self.is_need_heel = False

            self.set_attack_count = 0
        else:
            # 如果战斗回合数小于3
            # 并且回血线高于0，则使用普通攻击
            if self.set_attack_count <= 3 and (self.heal_line>0):
                self.battleSys.attack()
                self.set_attack_count += 1
            super().battlesel_action() # 刷新自动战斗

    def start_work(self,mark = 'xuanwu'):
        '''
        description: 开始点在村口外，玛塔塔平原 351.203 
        param {*}
        return {*}
        '''   

        self.chadata = self.getLeaderSet(mark=mark)

        self.set_init_data(self.chadata)

        for i in range(self.loopcount):   
            logger.info('>> start county [%i]' % i)
            # if mark == 'xuanwu':
            #     self.exercise()

            self.main()
            self.waitbreak()


if __name__ == "__main__":
    from bin import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    default_region = (m[0],m[1],m[2]-m[0],m[3]-m[1])
    test = leaderSystem(rect=m)
    # for i in range(100000):
    #     test.gotoMidVallage()
    #     time.sleep(1)
    #     print (i)
    # print ('done')
    # test.gotoFrontOfNurse()
    # test.logReturnMark()
    # test.backToGate()
    test.set_path(cgw.main['path'])
    chadata = test.getLeaderSet(mark='default')

    test.set_init_data(chadata)
    test.exercise()
    # test.gotoFrontOfVallage()
    # if 0:
    #     for i in range(10000):
    #         test.logDoorMark()
    #         time.sleep(10)
    #         print (i)

    # test.backToGate()
    # test.gotoWork()
    # test.gotoNurse()

    # print (test.isVellage(isValue=True))

    # test.gotoGuard()
    # test.fromGuardToHospital()
    # test.leaveHospital()
    # test.refill()


