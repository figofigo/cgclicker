#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-04-30 18:21:55
LastEditTime: 2023-03-10 23:32:53
LastEditors: figo - uz
Description: 在雷克塔尔烧技能
copyright: figo software 2020-2021
'''

import time
import os
import sys
sys.path.append('.')
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('leiketaerSkill')
logger.setLevel(logging.DEBUG)

from bin import autoloop
from bin import hospital_heal

def timelog(func):
    def wrapper(*args, **kw):
        logger.info('我准备开始执行：{} 函数了:'.format(func.__name__))
        sttime = time.time()
        # 真正执行的是这行。
        resluts = func(*args, **kw)
        edtime = time.time()
        logger.info('%s 执行完毕，耗时 %s' % (func.__name__,str(edtime-sttime)))
        return resluts
    return wrapper

class leiketaerSkill(autoloop.autoloop):
    def __init__(self,rect,sigleLoop=60,wholeLoop = 500) :
        super(leiketaerSkill,self).__init__(rect)
        self.singleLoop = sigleLoop
        self.wholeLoop = wholeLoop
        self.rect = rect
        self.redline = 200
        self.blueline = 100
        self.defaultskill = {'human':[1,'enermy'],'pet':[1,'enermy']}
        self.limitrange = 4 if self.wintype == 640 else 5
        self.hospital = hospital_heal.Hospital(rect)
        self.isrun = True

    def gotoGate(self):
        '''
        description: 开始点在医院门口
        param {*}
        return {*}
        '''  
        #self.walkTo('wn',distance=4)

        for i in range(5):
            ans = self.goto([73,96])
            if ans:
                break

        for i in range(5):
            ans = self.goto([75,104])
            if ans:
                break
        
        for i in range(5):
            self.walkTo('s',distance=4)
            mapcheck = self.isKulukesi()
            if mapcheck:
                logger.info('到村口了')
                return True
        return False

    def gotoWork(self):
        '''
        description: 出门不定点，但是回来的时候需要，出门先截图
        return {*}
        '''    
        self.setMapopen(True)    
        # self.logReturnMark()
        self.goto([352,198],limitrange=self.limitrange)
        
        self.goto([353,195],limitrange=self.limitrange)

        self.battle.battle_control(defaultskill=self.defaultskill) # 等8秒看看有没有战斗

        logger.info('到达了练功点')
        return True
        #389 320



    def backToGate(self):
        self.setMapopen(True,f=1)
        self.battle.setAutoBattle(True,f=1)
        #351,203
        for i in range(20):
            isok = self.goto([575,381])
            if isok:
                break
            else:
                self.battle.battle_control(redline=self.redline,blueline=self.blueline,defaultskill=self.defaultskill,isrun=self.isrun)
            

        for i in range(10):
            self.walkTo('n',distance=2)
            mapcheck = self.isLeiketaer()
            if mapcheck:
                logger.info('到了到了到了')
                return True
            else:
                self.battle.battle_control(redline=self.redline,blueline=self.blueline,defaultskill=self.defaultskill,isrun=self.isrun)

        return False


    def gotoHospital(self):
        '''
        description: 在村子里，去医院
        return {*}
        '''        
        for i in range(2):
            self.walkTo('n',distance=3)
            # time.sleep(0.1)

        for i in range(10):
            isok = self.goto([75,95])
            if isok:
                break

        logger.info('goto mid village ok')

        for i in range(5):
            isok = self.goto([77,93])
            mapcheck = self.isHospital()
            if mapcheck:
                logger.info('到了到了到了')
                return True


        logger.info('没进村啊。咋回事呢')
        return False
        
    def gotoNurse(self):
        '''
        description: 去找医生
        return {*}
        '''
        self.walkTo('e',distance=3)

        for i in range(10):
            isok = self.goto([13,8])
            if isok:
                break
                #return True
            else:
                time.sleep(1)
        logger.info('goto Nurse.finish')
        return True

    def leaveHospital(self):
        '''
        description: 离开医院
        return {*}
        '''
        self.walkTo('w',distance=4)
        for i in range(10):
            self.goto([2,8],check=False)
            if i>2:
                check = self.isLeiketaer()
                if check:
                    return True

        return False


    def refill(self):

        logger.info('back To Gate.start')
        self.waitbreak()
        self.battle.setAutoBattle(True,f=1)
        self.backToGate()

        logger.info('back To Gate.finish')
        self.battle.setAutoBattle(False,renew=0)
        self.waitbreak()
        self.gotoHospital()
        
        logger.info('goto Nurse.start')

        # for i in range(3):
        self.gotoNurse()
        self.walkTo(direction='s',button='right')
        ans = self.hospital.heal()


        logger.info('leave Hospital.start')
        self.waitbreak()

        self.leaveHospital()

        # self.wait(1)
        self.waitbreak()
        self.gotoGate()
        self.waitbreak()
        # self.gotoWork()
        return True


    def getAutoloopSet(self,mark):
        rec52shape = 'e.s.w.n'
        rtdata = {}
        if mark == 'wushi':
            rtdata= {
                        'module':'random',
                        'direct':"578.388",
                        'distance':3,
                        'count':100,
                        'waitTime':0,
                        'redline':400,
                        'blueline':250,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'self'],'pet':[2,None]},
                        'isrun':True,
                        'isCloseCombat':False
                    }
        elif mark == 'moshu':
            rtdata= {
                        'module':'random',
                        'direct':"578.388",
                        'distance':3,
                        'count':100,
                        'waitTime':0,
                        'redline':500,
                        'blueline':300,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        'isrun':True,
                        'isCloseCombat':False
                    }
        elif mark == 'daixiaohao':
            rtdata= {
                        'module':'random',
                        'direct':"578.388",
                        'distance':3,
                        'count':80,
                        'waitTime':0,
                        'redline':500,
                        'blueline':300,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[1,'enermy']},
                        'isrun':False,
                        'isCloseCombat':False
                    }
        elif mark == 'gongjian':
            rtdata= {
                        'module':'random',
                        'direct':"578.388",
                        'distance':3,
                        'count':100,
                        'waitTime':0,
                        'redline':300,
                        'blueline':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[1,'enermy']},
                        'isrun':True,
                        'isCloseCombat':False
                    }
        elif mark == 'jianshi':
            rtdata= {
                        'module':'random',
                        'direct':"578.388",
                        'distance':3,
                        'count':100,
                        'waitTime':0,
                        'redline':300,
                        'blueline':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        'isrun':True,
                        'isCloseCombat':True
                    }
        elif mark == 'shaojineng':
            rtdata= {
                        'module':'random',
                        'direct':"578.388",
                        'distance':3,
                        'count':100,
                        'waitTime':0,
                        'redline':300,
                        'blueline':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        'isrun':True,
                        'isCloseCombat':True
                    }
        else:
            rtdata= {
                        'module':'linear',
                        'direct':rec52shape,
                        'distance':3,
                        'count':100,
                        'waitTime':0,
                        'redline':300,
                        'blueline':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        'isrun':True,
                        'isCloseCombat':True
                    }
        self.redline = rtdata['redline']
        self.blueline = rtdata['blueline']
        self.defaultskill = rtdata['defaultSkill']
        self.isrun = rtdata['isrun']
        return rtdata

    def start_work(self,mark = 'gedou',istest=False):
        '''
        description: 开始点在村口外，玛塔塔平原 351.203 
        param {*}
        return {*}
        '''   
        if istest:
            self.data = self.getAutoloopSet(mark=mark)
            isLoopDone = self.start()
            print (isLoopDone)
        else:
            self.data = self.getAutoloopSet(mark=mark)
            # self.gotoWork()  
            self.walkTo('n',distance=1)

            for i in range(self.wholeLoop):   
                logger.info('============================')
                logger.info(u'开始第 [%i] 轮循环' % i)
                logger.info('============================')
                if self.battle.red()<=self.redline or self.battle.blue()<=self.blueline :
                    # logger.info("red : %s / %s"(str(self.battle.red()),str(self.redline)))
                    # logger.info("blue: %s / %s"(str(self.battle.blue()),str(self.blueline)))
                    logger.info('血魔量太少了')
                    isLoopDone = True
                else:
                    self.data = self.getAutoloopSet(mark=mark)
                    isLoopDone = self.start()
                logger.info('singleLoop.done')
                if isLoopDone:
                    logger.info('refill..start')
                    self.refill()
                    logger.info('refill...done')
                self.waitbreak()
                
    def walktime_prove(self):
        sttime = time.time()

        self.backToGate()

        self.gotoHospital()
        mark1 = time.time()
        logger.info('gotoHospital use time.%s' % (str(mark1-sttime)))

        self.gotoNurse()
        mark2 = time.time()
        logger.info('gotoNurse use time.%s' % (str(mark2-sttime)))

        self.walkTo(direction='s',button='right')
        ans = self.hospital.heal()
        mark3 = time.time()
        logger.info('heal use time.%s' % (str(mark3-sttime)))


        self.leaveHospital()
        mark4 = time.time()
        logger.info('leaveHospital use time.%s' % (str(mark4-sttime)))

        self.gotoGate()
        mark5 = time.time()
        logger.info('gotoGate use time.%s' % (str(mark5-sttime)))
if __name__ == "__main__":
    from bin import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    default_region = (m[0],m[1],m[2]-m[0],m[3]-m[1])
    test = leiketaerSkill(rect = default_region,sigleLoop=60,wholeLoop = 40)
    # for i in range(100000):
    #     test.gotoMidVallage()
    #     time.sleep(1)
    #     print (i)
    # print ('done')
    # test.gotoFrontOfNurse()
    # test.logReturnMark()
    # test.backToGate()
    test.walktime_prove()
    # test.gotoFrontOfVallage()
    # if 0:
    #     for i in range(10000):
    #         test.logDoorMark()
    #         time.sleep(10)
    #         print (i)

    # test.backToGate()
    # test.gotoWork()
    # test.gotoNurse()

    # print (test.isVellage(isValue=True))

    # test.gotoGuard()
    # test.fromGuardToHospital()
    # test.leaveHospital()
    # test.refill()
