#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-04-30 18:21:55
LastEditTime: 2024-01-06 02:08:04
LastEditors: figo - uz
Description: 在杰诺瓦烧技能
copyright: figo software 2020-2021
'''

import time
import os
import sys
sys.path.append('.')
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('xierwei_skill')
logger.setLevel(logging.DEBUG)

from bin import opreate_charater as opc
from bin import walk as coordsys
from bin import hospital_heal

def timelog(func):
    def wrapper(*args, **kw):
        logger.info('我准备开始执行：{} 函数了:'.format(func.__name__))
        sttime = time.time()
        # 真正执行的是这行。
        resluts = func(*args, **kw)
        edtime = time.time()
        logger.info('%s 执行完毕，耗时 %s' % (func.__name__,str(edtime-sttime)))
        return resluts
    return wrapper

class jienuowaSkill(opc.opreate_character):
    def __init__(self,rect,wholeLoop = 50000) :
        super(jienuowaSkill,self).__init__(rect)
        self.wholeLoop = wholeLoop
        self.rect = self.real_rect(rect)
        self.defaultskill = {'human':[1,'enermy'],'pet':[1,'enermy']}
        self.limitrange = 4 if self.wintype == 640 else 5
        self.hospital = hospital_heal.Hospital(rect)
        self.coordsys = coordsys.walksys(rect,mark='jienuowa')

    def gotoGate(self):
        '''
        description: 开始点在医院门口
        param {*}
        return {*}
        '''  
        # self.coordsys.walkTo('s',distance=4)

        for i in range(10):
            ans = self.coordsys.goto([40,39],blur=2)
            if ans:
                break
            else:
                time.sleep(1)
        
        for i in range(10):
            ans = self.coordsys.goto([31,39],blur=2)
            if ans:
                break
            else:
                time.sleep(1)

        for i in range(15):
            self.coordsys.goto([24,40],blur=2)
            mapcheck = self.coordsys.isShalianna()
            if mapcheck:
                logger.info('到村口了')
                return True
            else:
                time.sleep(1)
        return False

    def backToGate(self):
        self.battleSys.setAutoBattle(False,f=1)
        #351,203
        for i in range(100):
            nowstatus = self.status()
            print ('backing gate',nowstatus)
            if nowstatus == 'battle_sel':
                self.battleSys.escape()
            elif nowstatus == 'pet':
                self.battleSys.petCastSkill(2,None)
            elif nowstatus == 'peace':                
                self.coordsys.goto([217,456],limitrange=self.limitrange)
                mapcheck = self.coordsys.isJienuowa()
                if mapcheck:
                    logger.info('到了到了到了')
                    return True
            else:
                time.sleep(0.1)
        return False


    def gotoHospital(self):
        '''
        description: 在村子里，去医院
        return {*}
        '''        
        for i in range(2):
            self.coordsys.walkTo('e',distance=5,clicks=1)
            # time.sleep(0.1)

        logger.info('goto mid village ok')

        for i in range(10):

            ans = self.coordsys.goto([41,37])
            if ans:
                break
        
        for i in range(5):
            self.coordsys.walkTo('en',distance=4,clicks=1)
            mapcheck = self.coordsys.isHospital()
            if mapcheck:
                logger.info('到了到了到了')
                return True


        logger.info('没进村啊。咋回事呢')
        return False
        
    def gotoNurse(self):
        '''
        description: 去找医生
        return {*}
        '''
        for i in range(2):
            self.coordsys.walkTo('ne',distance=3)

        for i in range(10):
            isok = self.coordsys.goto([10,5])
            if isok:
                break
                #return True
            else:
                time.sleep(1)
        logger.info('goto Nurse.finish')
        return True

    def leaveHospital(self):
        '''
        description: 离开医院
        return {*}
        '''
        self.coordsys.walkTo('ws',distance=4)
        for i in range(10):
            time.sleep(1)
            self.coordsys.goto([1,9],check=False)

            check = self.coordsys.isJienuowa()
            if check:
                return True

        return False


    def refill(self):
        logger.info('等待战斗结束')
        for i in range (500):
            nowst = self.status()
            print ('wait',i,nowst)
            
            if nowst == 'battle_sel':
                self.battleSys.escape()
            elif nowst == 'pet':
                self.battleSys.petCastSkill(2,None)
            elif nowst == 'peace':
                break
            else:
                time.sleep(0.1)
        logger.info('back To Gate.start')
        self.waitbreak()
        self.battleSys.setAutoBattle(True,f=1)
        self.backToGate()
        logger.info('back To Gate.finish')
        self.walktime_prove()
        return True


    def get_skill_train_set(self,mark):
        rtdata = {}
        if mark == 'wushi':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':400,
                        'blue_check_line':250,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'self'],'pet':[2,None]},
                        
                        
                    }
        elif mark == 'qijue':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':400,
                        'blue_check_line':250,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'self'],'pet':[2,None]},
                        
                        
                    }
        elif mark == 'moshu':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':500,
                        'blue_check_line':300,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        
                        
                    }
        elif mark == 'daixiaohao':
            rtdata= {                    
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':500,
                        'blue_check_line':300,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[1,'enermy']},
                        
                        
                    }
        elif mark == 'gongjian':
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':300,
                        'blue_check_line':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[1,'enermy']},
                        
                        
                    }
        elif mark == 'jianshi':
            rtdata= {                    
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':300,
                        'blue_check_line':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        
                        
                    }
        else:
            rtdata= {
                        'ischeck_red':True,
                        'ischeck_blue':True,
                        'red_check_line':300,
                        'blue_check_line':100,
                        'clicks':1,
                        'defaultSkill':{'human':[1,'enermy'],'pet':[2,None]},
                        
                        
                    }

        self.defaultskill = rtdata['defaultSkill']
        self.set_init_data(rtdata)
        return rtdata

    def peace_action(self):
        # super().peace_action()
        self.check_blue()
        self.check_red()
        self.coordsys.main()

    def battlesel_action(self):
        if self.reload_ready:
            self.battleSys.setAutoBattle(False)
            humanskill = self.defaultskill['human']
            self.battleSys.humanCastSkill(humanskill[0],target=humanskill[1])
            self.reload_ready = False

        self.battleSys.setAutoBattle(True)
        self.battleSys.reset_autobattle_pos()


    def pet_action(self):
        '''
        description: 宠物选择技能阶段，默认是使用1号技能攻击敌人
        return {*}
        '''       
        petskill = self.defaultskill['pet']
        print ('using pet skill',petskill)
        self.battleSys.petCastSkill(petskill[0],target=petskill[1]) 

    def start_work(self,mark = 'gedou',istest=False):
        '''
        description: 开始点在村口外，玛塔塔平原 351.203 
        param {*}
        return {*}
        '''   
        self.get_skill_train_set(mark)

        # self.coordsys.gotoWork()  
        self.coordsys.walkTo('w',distance=4)
        self.reload_ready = True
        self.isLoopDone = False
        for i in range(self.wholeLoop):   
            logger.info('============================')
            logger.info(u'开始第 [%i] 轮循环' % i)
            logger.info('============================')
            
            if self.is_blue_low :
                logger.info('蓝量太少了')
                self.isLoopDone = True
            elif self.is_red_low:
                logger.info('血量太少了')
                self.isLoopDone = True
            else:
                self.isLoopDone = self.main()

            logger.info('singleLoop.done')
            if self.isLoopDone:
                logger.info('refill..start')
                self.refill()
                self.coordsys.walkTo('w',distance=4)
                logger.info('refill...done')
            self.waitbreak()
                
    def walktime_prove(self):
        sttime = time.time()

        self.gotoHospital()
        mark1 = time.time()
        logger.info('gotoHospital use time.%s' % (str(mark1-sttime)))

        self.gotoNurse()
        mark2 = time.time()
        logger.info('gotoNurse use time.%s' % (str(mark2-sttime)))

        self.coordsys.walkTo(direction='e',button='right')
        ans = self.hospital.heal()
        mark3 = time.time()
        logger.info('heal use time.%s' % (str(mark3-sttime)))

        self.check_blue()
        self.check_red()

        self.leaveHospital()
        mark4 = time.time()
        logger.info('leaveHospital use time.%s' % (str(mark4-sttime)))

        self.gotoGate()
        mark5 = time.time()
        logger.info('gotoGate use time.%s' % (str(mark5-sttime)))

if __name__ == "__main__":
    from bin import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    test = jienuowaSkill(rect = m,wholeLoop = 40)

    # test.start_work()
    # test.start_work()
    test.get_skill_train_set('moshu')
    test.walktime_prove()
