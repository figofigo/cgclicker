#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-04-09 01:45:38
LastEditTime: 2024-08-20 23:13:37
LastEditors: figo - uz
Description: 新版保持领队
copyright: figo software 2020-2021
'''

import logging
import random
import sys
import time

import pyautogui
sys.path.insert (0,r'C:\gitlab\cgclicker')
from util import getArea
from util import dialog_moniter

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('skill_huodong _func')
logger.setLevel(logging.INFO)

class skill_huodong(getArea.cgwindows):
    def __init__(self,bluelow = 0.1,redlow = 0.1):
        super().__init__()
        self.leadwin = None
        self.bluelow = bluelow
        self.redlow = redlow
        self.log_count = 0 # 记录资源不足的计数，一般超过50次了就是掉线了
        self.fangdiaoxian_count = 0 # 有可能出现自动战斗，但是没有自动战斗面板的问题。


    def analy(self,data):
        action_list = []
        data['warn'] = []

        red_percent = round(data['red']/data['red_max'],2)
        blue_percent = round(data['blue']/data['blue_max'],2)
        if red_percent <= self.redlow and data['red']>=0:
            data['warn'].append('red low %s' % str(red_percent))
        if blue_percent <= self.bluelow and data['blue']>=0:
            data['warn'].append('blue low %s' % str(blue_percent))
            action_list.append('eat_blue')
        
        

        if 'count' in data.keys():
            checkline = 300
            if 0 < data['count'] < checkline:
                data['warn'].append('auto battle check [ %i/%i ],recount' % (data['count'],checkline))
                action_list.append('recount')
            else:
                print ('auto battle count [ %i/%i ]' % (data['count'],checkline))
        else:
            print('no [  count  ] attr')
        self.displaydata(data)
        return action_list

    # def action(self,wd,act,dt):
    #     if act:
    #         for aa in act:
    #             if aa == 'eat_blue':
    #                 logger.info('start eat blue')
    #                 self.stopwalking()
    #                 wd.set_to_top()
    #                 wd.bag.set_bag(True)
    #                 isfast = True
    #                 iseat = wd.bag.eat(blue_or_red='blue',isfast=isfast)
    #                 if iseat:
    #                     # 此处增加一个归位的功能，避免角色因为吃蓝偏移目标点
    #                     self.startwalking()
    #             elif aa =='recount':
    #                 isexitst = wd.autobattle.isAutoBattle()
    #                 if isexitst:
    #                     wd.autobattle.reset_count()
    #     else:
    #         return True


    def displaydata(self,data):
        print ('--- ID %s (%s,%s - %s)---' % 
                    (data['window_id'],
                     str(data['rect'][0]),
                     str(data['rect'][1]),
                     str(data['distance']),
                     ))
        print ('red :',data['red'],'|',data['red_max'],round(data['red']/data['red_max'],2)*100,'%')
        print ('blue:',data['blue'],'|',data['blue_max'],round(data['blue']/data['blue_max'],2)*100,'%')
        if data['warn']:
            for i in data['warn']:
                print (i)

    # def stopwalking(self):
    #     if self.leadwin:
    #         self.leadwin.autobattle.set_autowalk(False)

    # def startwalking(self):
    #     if self.leadwin:
    #         self.leadwin.autobattle.set_autowalk(True)

    def goto_nurse(self,thewin):
        # thewin = self.leadwin
        isstart = thewin.map.isHuanzhididiyiji()
        if isstart:
            thewin.map.goto([20,27])
            thewin.map.goto([21,21])
            thewin.map.walkTo('n',button='right')
            thewin.duihua.wait_duihua()
            return True
        else:
            return False
            
    def back_to_stone(self,thewin):
        for i in range(5):
            if thewin.map.isHuanzhididiyiji():
                return True
            else:
                backpos = self.read_start_pos(thewin)
                # backpos = [25,8]
                thewin.map.goto(backpos,check_count=30)
                return False

    def goto_stone(self,thewin):
        # thewin = self.leadwin
        isstart = thewin.map.isHuanzhididiyiji()
        if isstart:
            for i in range(30):
                isstart = thewin.map.isHuanzhididiyiji()
                if isstart:
                    logger.warn('仍然在遗迹中')
                    thewin.map.goto([19,30])
                    for __ in range(3):
                        thewin.waitbreak()
                        thewin.map.walkTo('s',distance=4)
                        isans = thewin.map.isHuanzhididiyiji()
                        if not isans:
                            return True
                    time.sleep(1)
                else:
                    return True
                

        else:
            logger.warn('不在幻之地底遗迹，无法去到传送石')

    def back_to_target(self,thewin):
        # self.make_area(thewin)
        centerpos = self.read_target_pos(thewin)
        print ('back to target',centerpos)
        if thewin.map.getDistance(centerpos,thewin.map.coord()) <= 2:
            print('到了')
            return True
        else:
            print ('没到',centerpos)
            self.goto_with_battle(thewin,centerpos,blur=2)

    def goto_vellage(self,thewin):
        # 打完卡，走出打卡所之后，站在门口空地
        # thewin.map.back()
        thewin.map.goto([78,67],validtimes=20)
        thewin.map.goto([99,88],validtimes=20)
        # 到了卖魔石小屋右边
        for i in range(50):
            thewin.waitbreak()
            thewin.map.walkTo('e',distance=4)
            check = thewin.map.isLixieliyabao()
            if check:break
        # 进里谢里雅堡
        thewin.map.goto([41,53],validtimes=20)

        for i in range(3):
            thewin.waitbreak()
            thewin.map.walkTo('n',distance=4)
            check = thewin.map.isLixieliyabao()
            if not check:break
        # 进城堡
        thewin.map.goto([65,24],validtimes=20)
        thewin.map.goto([45,24],validtimes=20)
        # 启程之间门口
        for i in range(10):
            thewin.waitbreak()
            thewin.map.walkTo('n',distance=4)
            check = thewin.map.isQichengzhijian()
            if check:break
        # 进启程之间
        thewin.map.goto([25,27],validtimes=20)
        thewin.map.goto([19,27],validtimes=20)
        thewin.map.goto([14,10],validtimes=20)
        thewin.map.goto([15,4],validtimes=20)
        # 杰诺瓦传送门口
        thewin.map.walkTo('e',button='right')
        thewin.duihua.select_yes()
        # 到杰诺瓦
        thewin.map.goto([8,9],validtimes=20)
        thewin.map.goto([12,6],validtimes=20)
        for i in range(10):
            thewin.waitbreak()
            thewin.map.walkTo('e',distance=4)
            check = thewin.map.isCunzhangjia()
            if check:break
        # 出传送间
        thewin.map.goto([11,9],validtimes=20)
        for i in range(10):
            thewin.waitbreak()
            thewin.map.walkTo('w',distance=4)
            check = thewin.map.isJienuowa()
            if check:break
        # 出村长家
        thewin.map.goto([53,37],validtimes=20)
        thewin.map.goto([68,21],validtimes=20)
        thewin.map.goto([68,18],validtimes=20)
        for i in range(10):
            thewin.waitbreak()
            thewin.map.walkTo('e',distance=4)
            check = thewin.map.isShalianna()
            if check:break

        
        self.goto_with_battle(thewin,[281,418],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[306,426],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[317,430],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[319,441],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[348,456],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[375,429],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[375,372],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[390,357],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[390,317],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[414,297],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[436,281],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[452,293],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[513,293],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[522,301],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[541,292],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[570,271],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[570,274])
        for i in range(10):
            thewin.waitbreak()
            thewin.map.walkTo('s',distance=4)
            check = thewin.map.isShalianna()
            if not check:break
        self.goto_with_battle(thewin,[33,44],validtimes=20)
        self.goto_with_battle(thewin,[43,43],validtimes=20)
        self.goto_with_battle(thewin,[48,38],validtimes=20)
        thewin.map.walkTo('e',button='right')
        thewin.duihua.select_next()
        thewin.duihua.select_next()
        thewin.duihua.select_next()
        thewin.duihua.select_next()
        thewin.duihua.select_next()
        thewin.duihua.select_next()
        thewin.duihua.select_ok()
        # 281 419
        # 294 420
        # 335 454
        # 356 454
        # 371 427
        # 381 360
        # 391 325
        # 435 282
        # 454 293
        # 513 293
        # 537 295
        # 565 274
        # 570 274

    def use_trsnsport_stone(self,thewin):
        nowpos = thewin.map.coord()
        if nowpos == [162,130]:
            thewin.map.walkTo('e',button='right')
        elif nowpos == [72,123]:            
            thewin.map.walkTo('e',button='right')
        elif nowpos == [233,78]:            
            thewin.map.walkTo('n',button='right')
        elif nowpos == [242,100]:
            thewin.map.walkTo('e',button='right')
        elif nowpos == [141,148]:
            thewin.map.walkTo('n',button='right')
        elif nowpos == [63,79]:
            thewin.map.walkTo('n',button='right')
        else:
            logger.warn('没在传送石位置上')
        time.sleep(0.5)
        for __ in range(100):
            if thewin.status == 'loading':
                time.sleep(0.1)
            else:
                break
        return thewin.map.coord()


    def goto_card(self,thewin):
        # thewin = self.leadwin
        # thewin.map.goto([229,84],validtimes=20,blur=3)
        # thewin.map.goto([229,84])
        # thewin.map.walkTo('n',button='right')
        # 去暗影阿蒙
        for __ in range(100):
            nowpos = self.use_trsnsport_stone(thewin)
            if nowpos[0]>200:
                break
            else:
                time.sleep(0.1)


        thewin.map.goto([236,77],validtimes=20,blur=0)
        thewin.map.goto([235,69],validtimes=20,blur=0)
        thewin.map.goto([238,67],validtimes=20,blur=3)
        thewin.map.goto([238,67])
        thewin.map.walkTo('n',button='right')
        thewin.duihua.select_next()
        thewin.duihua.select_next()
        thewin.duihua.select_yes()
        thewin.duihua.select_ok()
        # 去汉克

        thewin.map.goto([211,63],validtimes=20,blur=3)
        thewin.map.goto([211,40],validtimes=20,blur=3)
        thewin.map.goto([155,40],validtimes=20,blur=3)
        thewin.map.goto([160,28],validtimes=20,blur=3)
        thewin.map.goto([159,13],validtimes=20)
        thewin.map.walkTo('w',button='right')
        thewin.map.back()
        # 前往树
        
        for __ in range(100):
            nowpos = self.use_trsnsport_stone(thewin)
            if 130 < nowpos[0] < 170 and nowpos[1]>80:
                break
            else:
                time.sleep(0.1)



        thewin.map.goto([149,132],validtimes=20,blur=3)
        thewin.map.goto([145,132],validtimes=20,blur=0)
        thewin.map.walkTo('s',button='right')
        thewin.duihua.select_ok()
        # 去问小孩

        thewin.map.goto([153,123],validtimes=20,blur=1)
        for i in range(20):
            thewin.waitbreak()
            thewin.map.walkTo('n',distance=4)
            check = thewin.map.isLixieliyabao()
            if check:break


        thewin.map.goto([45,61],validtimes=20,blur=3)
        for i in range(20):
            thewin.waitbreak()
            thewin.map.walkTo('e',distance=4)
            check = thewin.map.isMingrentang()
            if check:break        
        #GM入口
        thewin.map.goto([10,12],validtimes=20,blur=0)
        thewin.map.walkTo('e',button='right')
        thewin.duihua.select_ok()


        thewin.map.goto([10,15],validtimes=20,blur=0)
        for i in range(20):
            thewin.waitbreak()
            thewin.map.walkTo('w',distance=4)
            check = thewin.map.isLixieliyabao()
            if check:break


        
        thewin.map.goto([45,64],validtimes=20,blur=3)
        thewin.map.goto([52,82],validtimes=20,blur=3)

        thewin.map.goto([47,90],validtimes=20,blur=0)        
        for i in range(20):
            thewin.waitbreak()
            thewin.map.walkTo('n',distance=4)
            check = thewin.map.isZhaohuanzhijian()
            if check:break
        # 召唤之间

        thewin.map.goto([4,17],validtimes=20,blur=0)
        thewin.map.goto([27,17],validtimes=20,blur=0)        
        for i in range(20):
            thewin.waitbreak()
            thewin.map.walkTo('n',distance=4)
            check = thewin.map.isHuilang()
            if check:break
        # #地牢1
        thewin.map.goto([38,4],validtimes=20,blur=2)
        thewin.map.goto([12,4],validtimes=20,blur=2)
        thewin.map.goto([7,12],validtimes=20,blur=2)
        thewin.map.goto([7,27],validtimes=20,blur=2)
        thewin.map.goto([23,27],validtimes=20,blur=0)        
        for i in range(20):
            thewin.waitbreak()
            thewin.map.walkTo('n',distance=4)
            check = thewin.map.isHuilang()
            if not check:break
        #地牢2
        self.goto_with_battle(thewin,[25,53],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[19,37],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[10,39],validtimes=20,blur=2)
        self.goto_with_battle(thewin,[9,52],validtimes=20,blur=0)
        self.goto_with_battle(thewin,[12,52],validtimes=20,blur=0)
        thewin.map.walkTo('e',button='right')
        thewin.map.back()
        #地牢2
        
        for __ in range(100):
            nowpos = self.use_trsnsport_stone(thewin)
            if nowpos[0]> 200:
                break
            else:
                time.sleep(0.1)



        thewin.map.goto([236,77],validtimes=40,blur=0)
        thewin.map.goto([235,69],validtimes=20,blur=0)
        thewin.map.goto([238,67],validtimes=20,blur=0)
        thewin.map.walkTo('n',button='right')
        thewin.duihua.select_ok()

    def goto_with_battle(self,thewin,target,validtimes=3,blur=0):
        for i in range(100):
            try:
                gets = thewin.map.goto(target,validtimes=validtimes,blur=blur)
            except:
                gets = None
                time.sleep(0.5)

                
            if gets:
                return True
            else:
                if thewin.autobattle.isAutoBattle():
                    time.sleep(1)
                else:
                    if thewin.status == 'battle_sel':
                        thewin.cast.escape()
                    elif thewin.status == 'pet':
                        thewin.cast.petCastSkill(2)
                        thewin.autobattle.setAutoBattle(True)
    def log_start_pos(self,thewin):
        startpos = thewin.map.coord()
        if not startpos:
            print ('获取到的坐标为NONE')
            return False
        if startpos[0]>99 or startpos[1] > 99:
            print ('数值超过两位数，不合法，没有更新')
            return False
        
        thewin.extra_dict.update({'startpos':startpos})
        print('log start pos',startpos)
        return True

    def write_step(self,thewin,step):
        thewin.extra_dict.update({'step':step})
        return True
    
    def read_step(self,thewin):
        return thewin.extra_dict.get('step')

    def read_start_pos(self,thewin):
        return thewin.extra_dict.get('startpos')
    
    def read_target_pos(self,thewin):
        return thewin.extra_dict.get('targetpos')
    
    def read_target_area(self,thewin):
        return thewin.extra_dict.get('targetarea')
    def read_target_dir(self,thewin):
        return thewin.extra_dict.get('targetdir')

    def moniter_red_blue(self,thewin,redline=300,blueline=90):
        for i in range(1000):
            if thewin.status != 'loading':
                red = thewin.red
                blue = thewin.blue

                if red< redline or blue < blueline:
                    logger.warn('战斗资源不足')
                    return False
                else:
                    return True

    # def get_area_list(self,pos,dis=2):
    #     return [[pos[0]-dis,pos[1]],
    #             [pos[0]+dis,pos[1]],
    #             [pos[0],pos[1]-dis],
    #             [pos[0],pos[1]+dis]]
    

    def make_area(self,thewin,distance=5):
        # thewin = self.leadwin
        get_dir = thewin.map.get_direction_of_random_map()
        rotateDict = {
            'n':[0,-1],
            'e':[1,0],
            's':[0,1],
            'w':[-1,0],
        }
        basepos = self.read_start_pos(thewin)
        dirpos_normal = rotateDict[get_dir]
        dirpos_distance = [i*distance for i in dirpos_normal]
        targetpos = [basepos[0] + dirpos_distance[0],
                    basepos[1] + dirpos_distance[1]]
        thewin.extra_dict.update({'targetpos':targetpos})
        dirDict = {
            'n':'ew',
            'e':'ns',
            's':'ew',
            'w':'ns'
        }

        hanginglist = []
        posA_offset = [i*(distance-1) for i in dirpos_normal]
        posA = [basepos[0] + posA_offset[0],
                    basepos[1] + posA_offset[1]]
        hanginglist.append(posA)

        posB_offset = [i*(distance+1) for i in dirpos_normal]
        posB = [basepos[0] + posB_offset[0],
                    basepos[1] + posB_offset[1]]
        hanginglist.append(posB)

        hanginglist = hanginglist * 2


        thewin.extra_dict.update({'targetarea':hanginglist})
        thewin.extra_dict.update({'targetdir':dirDict[get_dir]})
        return True
    
    def autowalk_after_first_in_stone(self,thewin):
        hanging_list = self.read_target_area(thewin)
        # base_pos = self.read_target_pos(thewin)
        base_dir = self.read_target_dir(thewin)
        for __ in range(1000):
            if 'peace' != thewin.status:
                logger.info('战斗了')
                return True
            else:
                for i in base_dir:
                    thewin.map.walkTo(i,distance=2,clicks = 2)
                for i in hanging_list:
                    thewin.map.goto(i,check_count=1,blur=1)
        return True

    def act(self,thewin):
        #战斗资源充足
            #人物技能
            #宠物技能
            #动画
            #和平
                #在遗迹
                #在火洞
        #战斗资源不足
            #人物技能
            #宠物技能
            #动画
            #和平
                #在遗迹
                #在火洞
        isok = self.moniter_red_blue(thewin)
        if isok:
            logger.info('战斗资源 [充足] ... %i' % self.fangdiaoxian_count)
            self.fangdiaoxian_count += 1
            self.log_count = 0
            st = thewin.status
            if st == 'peace':
                issafe = thewin.map.isHuanzhididiyiji()
                if issafe:
                    logger.info('从护士去到练级场的传送石')
                    
                    self.goto_stone(thewin)
                    self.write_step(thewin, 'first_in_stone')
                else:
                    logger.info('从练级场初始位置到练级位置')
                    if self.read_step(thewin) == 'first_in_stone':
                        self.log_start_pos(thewin)
                        self.make_area(thewin)
                        thewin.autobattle.setAutoBattle(False)
                        self.back_to_target(thewin)
                        self.write_step(thewin, 'shaojineng')
                        thewin.autobattle.setAutoBattle(False)
                        self.autowalk_after_first_in_stone(thewin)
                    else:
                        istaopao = thewin.autobattle.isTaopao()
                        if istaopao:
                            thewin.autobattle.setAutoBattle(False)
                            self.autowalk_after_first_in_stone(thewin)
                        else:
                            thewin.autobattle.setAutoBattle(True)
                            thewin.autobattle.set_autowalk(True)
            elif st == 'battle_sel':
                if not thewin.autobattle.isAutoBattle():
                    try:
                        thewin.cast.humanCastSkill(1)
                    except:
                        logger.warn('人物技能没施展成功')
            elif st == 'pet':
                try:
                    thewin.cast.petCastSkill(2)
                except:
                    logger.warn('宠物技能没施展成功')

                thewin.autobattle.setAutoBattle(True)
                thewin.autobattle.set_autowalk(True)
            else:
                if self.read_step(thewin) == 'first_in_stone':
                    self.write_step(thewin, 'shaojineng')

                if self.fangdiaoxian_count % 20 == 0:
                    thewin.autobattle.setAutoBattle(True)
                    thewin.autobattle.set_autowalk(True)

                elif self.fangdiaoxian_count % 5 == 0:
                    seed = random.random()
                    time.sleep(seed*3)
                    offset_x = int(random.random()*20)
                    offset_y = int(random.random()*20)
                    centerarea = [int(thewin.size[0]*0.5)+offset_x,
                    int(thewin.size[1]*0.5)+offset_y]
                    pyautogui.click(centerarea,duration=seed*4)
                    # 增加一些随机动作，试图避免掉线


        else:
            logger.info('战斗资源 [不足] ... %i' % self.log_count)
            self.fangdiaoxian_count = 0
            self.log_count += 1
            st = thewin.status
            if st == 'peace':
                issafe = thewin.map.isHuanzhididiyiji()
                if issafe:
                    logger.info('去找护士')
                    thewin.autobattle.setAutoBattle(False)
                    self.goto_nurse(thewin)
                    thewin.duihua.hospital_heal()
                    # thewin.duihua.select_cancel()
                else:
                    logger.info('从练级场回传送石')
                    self.back_to_stone(thewin)
            elif st == 'battle_sel':
                thewin.cast.escape()
            elif st == 'pet':                
                thewin.cast.petCastSkill(2)

                thewin.autobattle.setAutoBattle(False)
                # thewin.autobattle.set_autowalk(False)
            else:
                pass
        if self.log_count > 50:
            raise RuntimeError ('掉线了')

        # self.write_step(thewin,'first_walk')
        # self.autowalk_after_first_in_stone(thewin)
        # self.write_step(thewin,'normal_walk')

    def refill(self,thewin,start_step=1):
        for first_step in range(1000):
            st = thewin.status
            if st == 'peace':
                issafe = thewin.map.isHuanzhididiyiji()
                if issafe:
                    logger.info('去找护士')
                    thewin.autobattle.setAutoBattle(False)
                    self.goto_nurse(thewin)
                    thewin.duihua.hospital_heal()
                    # thewin.duihua.select_cancel()
                else:
                    logger.info('从练级场回传送石')
                    self.back_to_stone(thewin)
            elif st == 'battle_sel':
                thewin.cast.escape()
            elif st == 'pet':                
                thewin.cast.petCastSkill(2)

                thewin.autobattle.setAutoBattle(True)
                thewin.autobattle.set_autowalk(False)
            else:
                pass
            isok = self.moniter_red_blue(thewin)
            if isok:
                break
        # 以上是第一步，补血并回到石头

    def card(self):
        loops_win = list(self.windows)
        self.goto_card(loops_win[0])

    def vellage(self):
        loops_win = list(self.windows)
        self.goto_vellage(loops_win[0])


    def main(self):
        loops_win = list(self.windows)

        dialog = dialog_moniter.dialog_moniter(loops_win[0])
        # self.leadwin = loops_win[0]
        # print (self.leadwin)
        # 此处加入初始地址代码
        #self.leadwin.map.goto(self.read_target_pos(self.leadwin))

        # 此处加入游走中心位置
        for loopcount in range(99999999999):
            for wd in loops_win:
                wd.serialize()
                self.act(wd)
                time.sleep(0.2) # 降低刷新开销
            isRefresh = dialog.isMigongshuaxin()
            # print ('迷宫刷新',isRefresh)



if __name__ == '__main__':
    mt = skill_huodong()
    mt.windows[0].map.back()
    #gg = mt.goto_vellage(mt.windows[0])
    #print (gg)
    # mt.main()