#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-11-15 21:54:42
LastEditTime: 2024-02-16 22:53:09
LastEditors: figo - uz
Description: 这里放了一些常用的功能
FilePath: \cgclicker\bin\basic.py
copyright: figo software 2020-2021
'''
import keyboard
import time
import random
import math
import os
import pyautogui

def timeit(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f'{func.__name__} 运行时间为：{end - start} 秒')
        return result
    return wrapper

class basic():

    def __init__(self):
        super(basic,self).__init__()
        self.testPath = '%s/resources/test' % self.root_path()
        self.wintype = 0
        self.rect = (0, 0, 1, 1)
        self.cg_path = ''
        # 魔力宝贝的启动路径

    def is_big_window(self):
        # print (self.rect)
        check_val = self.rect[2] #- self.rect[0]
        if 700<check_val<900:
            return True
        else:
            return False

    def is_small_window(self):
        check_val = self.rect[2] #- self.rect[0]
        if 100<check_val<700:
            return True
        else:
            return False

    def root_path(self):
        filePath = os.path.abspath(__file__).replace('bin','')
        rootPath = os.path.dirname(filePath)
        return rootPath

    def getDistance(self,pos1,pos2):
        # 计算两个坐标之间的距离
        dis = math.sqrt(math.pow(pos2[0]-pos1[0],2) + math.pow(pos2[1]-pos1[1],2))
        return int(dis)

    def localx(self,offsetval,baserect = None):
        '''
        description: 尝试返回，基于窗口原点局部坐标的世界位置 x 轴
        return {*}
        '''        
        returnval = self.rect[0] + offsetval
        return returnval


    def localy(self,offsetval,baserect = None):
        '''
        description: 尝试返回，基于窗口原点局部坐标的世界位置 y 轴
        return {*}
        '''        
        returnval = self.rect[1] + offsetval
        return returnval


    def localpos(self,offsetlist,baserect = None):
        pass

    def real_rect(self,rect):
        '''
        description: 这个修正取的是画面中真正的rect，不包含边框和标题
        return {*}
        '''        
        titleHeight = 26
        return_rect = (rect[0]+2,rect[1]+titleHeight,rect[2]+1,rect[3])
        if (rect[2]) > 700:
            self.wintype = 800
        else:
            self.wintype = 640
        return return_rect

    def waitbreak(self):
        if keyboard.is_pressed('alt'): #if key 'alt' is pressed 
            raise KeyboardInterrupt ('手动停止自动循环')

    def wait(self,lenth):
        rnd = random.random()
        time.sleep(lenth + rnd)

    def resetMouse(self,pos = []):
        if not pos:
            pos = [87,23]
        rseed = random.random()
        offsetx = int(rseed*80) + pos[0] #87
        offsety = int(rseed*10) + pos[1] #23
        pos_x = self.rect[0] + offsetx
        pos_y = self.rect[1] + offsety
        pyautogui.moveTo(pos_x,pos_y)   

    def isloading(self):
        checklength = 90
        modcheck = (self.localx(10),self.localy(27),checklength ,1)
        im = pyautogui.screenshot(region = modcheck)
        others = 0
        for i in range(checklength):
            lineval = im.getpixel((i,0))
            sumnum =  (sum(lineval))
            if sumnum<600:
                others += 1
                # print (sumnum)

        if others:
            return False
        else:
            return True
        # print (others,'/',checklength)
        # im.save('d:/modline.png')

    def set_rect(self,m):
        self.rect = self.real_rect(m)

    def isSimilarColor(self,input_color,regular_color,limit = 10):
        # print ('input',input_color)
        # print ('regular_color',regular_color)
        for i in range(len(input_color)):
            if input_color[i] not in range(regular_color[i]-limit,regular_color[i]+limit):
                return False
        return True
            
    # @timeit
    def status(self):
        '''
        description: 检查当前是在那个状态，战斗外/切换/战斗动画/战斗选择
        return {*}
        '''        
        # 战斗开始后。会有人物指令，宠物指令，还有动画阶段
        # logger.debug('?? 查询当前战斗步骤')

        # size = 20 # 识别窗口右上角，size见方的区域
        win_width = self.rect[2]
        x = (self.rect[0]) + win_width-16
        y = (self.rect[1]) + 2
        dataArea = (x,y,1,3)
        
        im = pyautogui.screenshot(region=dataArea)
        # im.save('d:/status_check.png')
        # print ('dataArea',dataArea)
        # 完成区域指定

        peace={'0':(230, 226, 200),
                    '1':(186, 136, 48),
                    '2':(241, 202, 130)
                    }
        
        battle_sel={'0':None,
                    '1':None,
                    '2':(105, 85, 29)
                    }

        battle_pet = {'human_off':(223,195,146),
                    'human_on':(176,205,147),
                    'white':(250, 247, 236),
                    'hover':(176,176,176)
                    }

        pt2 = im.getpixel((0,2))
        
        if sum(pt2)<20:
            return "loading"
        else:
            isbattlesel = self.isSimilarColor(pt2,battle_sel["2"])

            if isbattlesel:
                pet_x = (self.rect[0]) + win_width-180
                pet_y = (self.rect[1]) + 50
                pet_dataArea = (pet_x,pet_y,1,3)
                
                pet_im = pyautogui.screenshot(region=pet_dataArea)
                pet_pt = pet_im.getpixel((0,2))
                for key,val in battle_pet.items():
                    petcheck = self.isSimilarColor(pet_pt,val)
                    if petcheck:
                        # print ('petcheck',petcheck)
                        if key in ['human_off','human_on','hover']:
                            # print ('button status',key)
                            # print (pet_pt)
                            return 'battle_sel'
                        elif key in ['white']:
                            return 'pet'
                        else:
                            continue
                    else:
                        continue


            ispeace2 = self.isSimilarColor(pt2,peace["2"])
            if ispeace2:
                pt1 = im.getpixel((0,1))
                ispeace1 = self.isSimilarColor(pt1,peace["1"])
                if ispeace1:
                    return ('peace')


        return "animation"

    def say(self,words):
        self.resetMouse()
        pyautogui.click()
        pyautogui.typewrite(words)
        time.sleep(0.3)
        pyautogui.press('enter')
        time.sleep(0.1)
        pyautogui.press('enter')

    def reset_item_window_pos(self):
        pos = [125,self.rect[3]-170]
        statuspath = '%s\\resources\\status' % self.root_path()
        onmark = 'itemBag.png'
        onPath = '%s\\%s' % (statuspath,onmark)
        
        for repeat in range(5):
            self.resetMouse()
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect) 
            except:
                ans = None
            finally:
                targetpos = [self.rect[0] + pos[0],self.rect[1]+pos[1]]
            
            
            if not ans:
                isfoundopenbag = False
                self.resetMouse()
                pyautogui.rightClick()
                for i in range(3):       
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('e') 
                    pyautogui.keyUp('ctrl')
                    for k in range(5):
                        try:
                            ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)  
                        except:
                            ans = None
                        finally:
                            pass
                        if ans:
                            isfoundopenbag = True
                            break
                        else:
                            time.sleep(0.1)
                    if isfoundopenbag:
                        break

            dis = math.sqrt(math.pow(targetpos[0]-ans[0],2)+math.pow(targetpos[1]-ans[1],2))
            if dis <2:
                pyautogui.keyDown('ctrl') 
                pyautogui.press('e') 
                pyautogui.keyUp('ctrl')
                return True
            else:
                pyautogui.moveTo(ans[0],ans[1])
            
                pyautogui.dragTo(self.rect[0]+pos[0],self.rect[1]+pos[1],duration=1,mouseDownUp=True)
        return True
    
    def reset_map_window_pos(self):
        pos = [565,35]
        if self.is_big_window():
            pos = [715,35]
        statuspath = '%s\\resources\\where' % self.root_path()
        onmark = 'ditu.png'
        onPath = '%s\\%s' % (statuspath,onmark)
        
        for repeat in range(5):
            self.resetMouse()
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect) 
            except:
                ans = None
            finally:
                targetpos = [self.rect[0] + pos[0],self.rect[1]+pos[1]]

            
            
            if not ans:
                isfoundopenbag = False
                self.resetMouse()
                pyautogui.rightClick()
                for i in range(3):       
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('s') 
                    pyautogui.keyUp('ctrl')
                    for k in range(5):
                        ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)  
                        if ans:
                            isfoundopenbag = True
                            break
                        else:
                            time.sleep(0.1)
                    if isfoundopenbag:
                        break

            dis = math.sqrt(math.pow(targetpos[0]-ans[0],2)+math.pow(targetpos[1]-ans[1],2))
            if dis <2:
                return True
            else:
                pyautogui.moveTo(ans[0],ans[1])
            
                pyautogui.dragTo(self.rect[0]+pos[0],self.rect[1]+pos[1],duration=1,mouseDownUp=True)
        return True
    
    def reset_skill_window_pos(self):
        pos = [self.rect[2]-235,self.rect[3]-335]
        offset = [0,-10]
        statuspath = '%s\\resources\\battle' % self.root_path()
        onmark = 'skill_start2.png'
        onPath = '%s\\%s' % (statuspath,onmark)
        
        for repeat in range(5):
            self.resetMouse()
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect) 
            except :
                ans = None
            finally:
                pass

            targetpos = [self.rect[0] + pos[0],self.rect[1]+pos[1]]

            
            
            if not ans:
                isfoundopenbag = False
                self.resetMouse()
                pyautogui.rightClick()
                for i in range(3):       
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('w') 
                    pyautogui.keyUp('ctrl')
                    for k in range(5):
                        try:
                            ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)
                        except :
                            ans = None
                        finally:
                            if ans:
                                isfoundopenbag = True
                                break
                            else:
                                time.sleep(0.1)
                    if isfoundopenbag:
                        break

            dis = math.sqrt(math.pow(targetpos[0]-ans[0],2)+math.pow(targetpos[1]-ans[1],2))
            if dis <2:
                pyautogui.keyDown('ctrl') 
                pyautogui.press('w') 
                pyautogui.keyUp('ctrl')
                return True
            else:
                pyautogui.moveTo(ans[0]+offset[0],ans[1]+offset[1])
            
                pyautogui.dragTo(self.rect[0]+pos[0]+offset[0],self.rect[1]+pos[1]+offset[1],duration=1,mouseDownUp=True)
        return True
    
    def close_brief(self):
        pyautogui.click(400,300,duration=0.5)
    
    def set_path(self,val):
        self.cg_path = val


    def load_chat(self,istest = False):
        # install_list = [r'C:\Program Files (x86)\易玩通\魔力宝贝6.0\Log',
        #                 r'D:\Program Files (x86)\易玩通\魔力宝贝6.0\Log',
        #                 r'E:\Program Files (x86)\易玩通\魔力宝贝6.0\Log',
        #                 r'F:\Program Files (x86)\易玩通\魔力宝贝6.0\Log',
        #                 r'G:\Program Files (x86)\易玩通\魔力宝贝6.0\Log']
        # for i in install_list:
        #     if os.path.exists(i):
        #         log_dir = i
        #         break
        log_dir = ''
        if istest:
            log_dir = r'D:\Program Files (x86)\易玩通\魔力宝贝6.0\Log'
        else:
            cgdir = os.path.dirname(self.cg_path)
            log_dir = f'{cgdir}/Log'
        files = os.listdir(log_dir)
        files.sort(reverse=True)
        file_path = f'{log_dir}/{files[0]}'
        alllines = []
        print ('start read file',file_path)
        with open(file_path, 'r', encoding='gbk', errors='ignore') as f:
            alllines = [line for line in f]

        if istest:
            print (alllines)
        return alllines
    
    def isMigongshuaxin(self):
        alllines = self.recent_chat()
        for i in alllines:
            if i['type'] == 'anno':
                if '你感觉到一股不可思议的力量' in i['chat']:
                    print ('迷宫刷新，时间是',i['time_str'])
                    return True
        return False

    def analyze_chat(self,val):
        datadict = {}
        if '丂' in val:
            sptime = val.split('丂')
            strtime = sptime[0]
            rest_string = sptime[1]
            datadict['time_str'] = strtime
            if ':' in rest_string:
                spval = rest_string.split(':')
                user_sp = spval[0].split(']')
                datadict['type'] = 'chat'
                datadict['chat'] = spval[-1].replace('\n','')
                datadict['user'] = user_sp[-1]
                datadict['range'] = user_sp[0][1:]
            else:
                datadict['type'] = 'anno'
                datadict['chat'] = rest_string.replace('\n','')
                datadict['user'] = 'system'
                datadict['range'] = 'system'
        else:
            datadict['time_str'] = 'none'
            datadict['type'] = 'other'
            datadict['chat'] = val.replace('\n','')
            datadict['user'] = 'system'
            datadict['range'] = 'system'

        return datadict

    def recent_chat(self,length = 3):
        lines = self.load_chat()
        returnlist = []
        for i in lines[-length:]:
            data = self.analyze_chat(i)
            returnlist.append(data)
        return returnlist

    def test(self):
        pet_x = (self.rect[0]) + 800-180
        pet_y = (self.rect[1]) + 55
        pet_dataArea = (pet_x,pet_y,1,3)
        pyautogui.moveTo(pet_x,pet_y)
        # pet_im = pyautogui.screenshot(region=pet_dataArea)
        # pet_im.save('d:/pet_im.png')

if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    bc = basic()
    bc.set_rect(m)
    # bc.set_path(cgw.main['path'])
    ans = bc.load_chat()
    print (ans)
    # pos = bc.test()
    # print (pos)