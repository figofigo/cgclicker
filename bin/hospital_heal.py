#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-11-15 21:42:00
LastEditTime: 2023-02-05 22:41:47
LastEditors: figo - uz
Description: 在医院恢复的模块功能
FilePath: \cgclicker\bin\hospital_heal.py
copyright: figo software 2020-2021
'''
import pyautogui
import os
import sys
import time
import logging
sys.path.append('.')
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('hospital_heal')
logger.setLevel(logging.INFO)

from bin import basic
import random

class Hospital(basic.basic):

    def __init__(self,rect):
        super(Hospital,self).__init__()

        self.rect = self.real_rect(rect)

        if self.wintype == 800:
            self.click1_pos = [self.localx(360),self.localy(260)]
        else:
            self.click1_pos = [self.localx(280),self.localy(200)]
        rootPath = self.root_path()
        self.statuspath = '%s\\resources\\status' % rootPath
        if self.wintype == 640:         
            self.basepos = [self.localx(104),self.localy(121)]
        else:
            self.basepos = [self.localx(184),self.localy(181)]
    def cornerSolver(self,region):
        im = pyautogui.screenshot(region=region)
        width,height = im.size
        mid = 0
        white = 0
        black = 0
        other = 0
        for w in range(width):
            for h in range(height):
                val = sum(im.getpixel((w,h)))
                if abs(val - 765) < 10 :
                    white += 1
                elif abs(val - 251) < 10:
                    mid += 1
                elif val < 10:
                    black += 1
                else:
                    other += 1
        
        if white == 1 and black == 1 and mid == 2:
            return True
        else:
            # print ('black',black)
            # print ('white',white)
            # print ('mid',mid)
            # print ('other',other)
            return False

    def isHealing(self,isdebug=False):
        '''
        description: 快速确定是否在和医院的护士交谈治疗中
        return {*}
        '''   



        if isdebug:
            qregion = (self.basepos[0],self.basepos[1],20,20)
            im = pyautogui.screenshot(region=qregion)
            im.save(r'D:/start_corner.png')

            qregion = (self.basepos[0],self.basepos[1]+212,20,20)
            im = pyautogui.screenshot(region=qregion)
            im.save(r'D:/start_corner2.png')

            qregion = (self.basepos[0]+432,self.basepos[1]+212,20,20)
            im = pyautogui.screenshot(region=qregion)
            im.save(r'D:/start_corner3.png')

            qregion = (self.basepos[0]+432,self.basepos[1],20,20)
            im = pyautogui.screenshot(region=qregion)
            im.save(r'D:/start_corner4.png')
        else:
            qregion_lefttop = (self.basepos[0],self.basepos[1],2,2)
            c_lefttop = self.cornerSolver(qregion_lefttop)
            
            qregion_leftbottom = (self.basepos[0],self.basepos[1]+212,2,2)
            c_leftbottom = self.cornerSolver(qregion_leftbottom)

            qregion_rightbottom = (self.basepos[0]+432,self.basepos[1]+212,2,2)
            c_rightbottom = self.cornerSolver(qregion_rightbottom)

            qregion_righttop = (self.basepos[0]+432,self.basepos[1],2,2)
            c_righttop = self.cornerSolver(qregion_righttop)

            if c_leftbottom and c_lefttop and c_rightbottom and c_righttop:
                return True
            else:
                return False


    # def reset_cursor(self):
    #     '''
    #     description: 把鼠标挪到一个不碍事的地方
    #     return {*}
    #     '''        
    #     pyautogui.moveTo(self.localx(450),self.localy(120))


    def heal_action(self,duration=0.3,offset=0,keyname='all'):
        for i in range(5):
            status = self.heal_status()
            if status == "asking":
                pyautogui.click(self.click1_pos[0]+self.shack_x(),
                                self.click1_pos[1]+offset,
                                duration=0.5,clicks=1)
            elif status == keyname:
                break
            else:
                time.sleep(duration)

        trigger = False

        for i in range(60):
            # print (i)
            status = self.heal_status()
            if status in [keyname,'noneed',keyname+'ok']:
                
                if status == keyname:
                    self.clickyes(duration=duration)
                else:
                    self.clickconfirm(duration=duration)
                    trigger = True
            elif status == 'asking' and trigger:
                logger.info ('%s_finish' % keyname)
                return True
            else:                
                time.sleep(duration)
        return False
            

    def heal(self,withwait = True):
        '''
        description: 护士补血界面打开后，进行操作
        return {*}
        '''      
        if withwait:  
            for i in range(20):
                isStart = self.isHealing()  
                if isStart:
                    break
                else:time.sleep(0.5)
        else:
            isStart = self.isHealing()  
        if not isStart:
            logger.warn('当前不在医院补血界面')
            return False
        # 判断有没有开始   
        duration_con = 0.2
        isok_all = self.heal_action(duration=duration_con,offset=0,keyname='all')
        logger.info ('isok_all [%s]' % isok_all)
        # raise()
        isok_red = self.heal_action(duration=duration_con,offset=35,keyname='red')
        logger.info ('isok_red [%s]' % isok_red)
        isok_pet = self.heal_action(duration=duration_con,offset=70,keyname='pet')
        logger.info ('isok_pet [%s]' % isok_pet)


        self.heal_close()
        if isok_all and isok_red and isok_pet:            
            logger.info('heal done')
            return True
        else:
            logger.info('heal fail')
            return False

    def heal_close(self):
        for i in range(5):
            ans = self.heal_status()
            if ans != 'suspend':
                if ('ok' in ans) or ans == 'noneed':
                    self.clickconfirm()
                elif ans == 'asking':
                    self.clickcancel()
                else:
                    self.clickyes()
                
            else:
                return True
            time.sleep(1)
        return False

    def fastcheck(self):
        qregion = (self.localx(280-2),self.localy(170-26),20,20)
        # im = pyautogui.screenshot(qregion)
        im = pyautogui.screenshot(region=qregion)
        im.save(r'D:/gg.png')
        # pyautogui.moveTo(self.localx(280),self.localy(200),duration=1) # all
        # pyautogui.moveTo(self.localx(280),self.localy(235),duration=1) # red
        # pyautogui.moveTo(self.localx(280),self.localy(270),duration=1) # pet
        # pyautogui.moveTo(self.localx(300),self.localy(320),duration=1) # cansel/confirm
        # pyautogui.moveTo(self.localx(240),self.localy(320),duration=1) # yes
        # pyautogui.moveTo(self.localx(100),self.localy(110),duration=1)
        pass

    def shack_x(self,range=20):
        randomseed = random.random()
        return int(randomseed*range)

    def clickcancel(self,duration=0.2):
        
        # if self.wintype == 640:         
        #     self.basepos = [self.localx(104),self.localy(121)]
        # else:
        #     self.basepos = [self.localx(184),self.localy(181)]
        #640
        pyautogui.click(self.basepos[0]+196+self.shack_x(),
                        self.basepos[1]+194,
                        duration=duration)
        # pyautogui.click()

    def clickconfirm(self,duration=0.2):
        #640
        pyautogui.click(self.basepos[0]+196+self.shack_x(),
                        self.basepos[1]+199,
                        duration=duration)
        # pyautogui.click()

    def clickyes(self,duration=0.2):
        # 640
        pyautogui.click(self.basepos[0]+136+self.shack_x(),
                        self.basepos[1]+199,
                        duration=duration) # yes
        # pyautogui.click()

    def status_indentyfy(self,data_soup,isCommunicate=False):
        '''
        description: 根据data_soup分析当前处于哪个状态
        return {*}
        '''   
        
        if self.isHealing():
            logger.debug('处在对话状态中')
            if isCommunicate:
                return 'communication'
            else:
                if (data_soup[101] and data_soup[183] and data_soup[345]):
                    return 'asking' #
                elif (data_soup[23] and data_soup[28] and data_soup[281]):
                    return 'allok' #
                elif (data_soup[366] and data_soup[385]):
                    return 'all' #
                elif (data_soup[97] and data_soup[177] and data_soup[277]):
                    return 'redok' # 
                elif (data_soup[70] and data_soup[90] and data_soup[190]):
                    if (data_soup[82] and data_soup[84] and data_soup[86]):
                        return 'petok' # 
                    else:
                        return 'red'
                elif (data_soup[33] and data_soup[139] and data_soup[215]):
                    return 'pet'
                elif (data_soup[235] and data_soup[301] and data_soup[50]):
                    return 'noneed'
                else:
                    return 'error'

        else:
            return 'suspend'


    def heal_status(self,isdebug=False,debugtitle='heal',isCommunicate=False): 
        '''
        description: 新版血查询
        return {*}
        '''               
        logger.debug('查询当前与护士对话的进度')
        
        if self.wintype == 800:
            qregion = (self.localx(360-2),self.localy(230-26),20,20)
            logger.debug('in 800 mode')
        else:
            qregion = (self.localx(280-2),self.localy(170-26),20,20)
            logger.debug('in 640 mode')

        # 判断当前状态栏处在什么状态，并初始化范围数据


        keypoint_list = list(set([0,70,82,84,86,90,190,298,23,28,281,101,183,345,366,385,33,139,215,235,301,50,400,97,177,277]))
        # 优化过的查询点信息


        im = pyautogui.screenshot(region = qregion)
        width, height = im.size
        range_total = width*height
        # print (range_total)
        pic_datasoup = [0 for i in range(range_total)]
        # 初始化data_soup
        count = 0
        for hgt in range(height):
            for wid in range(width):
                if count in keypoint_list:
                    val = im.getpixel((wid,hgt))
                    if val[0] >100:
                        pic_datasoup[count]=1
                        # 根据优化过的点数据，获取相应的点颜色
                count+=1
        # print (len(pic_datasoup))
        heal_status = self.status_indentyfy(pic_datasoup,isCommunicate=isCommunicate)

        if isdebug: # 查错模式
            testsave = ('D:/%s.png' % (debugtitle))
            im.save(testsave)
            print ('status',qregion)
        return heal_status


if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    gg = Hospital(m)
    ans = gg.heal()
    # ans = gg.heal_status()
    # ans = gg.fastcheck()
    print (ans)