#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2023-12-16 20:59:34
LastEditTime: 2024-05-26 18:42:33
LastEditors: figo - uz
Description: 登出或回城系统
FilePath: \cgclicker\bin\logout.py
copyright: figo software 2020-2021
'''
import datetime
import random
import os
import sys
sys.path.append('.')
import time
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('whereAmI')
logger.setLevel(logging.DEBUG)

import pyautogui
from bin import basic

class logout(basic.basic):
    def __init__(self,rect) :
        super().__init__()

        rootPath = self.root_path()
        self.sourcepath = '%s\\resources\\logout' % rootPath
        self.rect = self.real_rect(rect)
        self.px = 0
        self.py = 0


    def open_sys_window(self,withclick=True):
        
        mark = 'logout.png'
        if self.is_small_window():
            mark = 'logout640.png'
        Path = '%s\\%s' % (self.sourcepath,mark)

        pos_x = 0
        pos_y = 0
        isFound = False
        for i in range(3):
            try:
                ans = pyautogui.locateCenterOnScreen(Path,region = self.rect)
                if ans:
                    logger.debug ('找到了')   
                    pos_x = ans[0] 
                    pos_y = ans[1]+ 30
                    isFound = True
                    break
            except:
                self.resetMouse()
                pyautogui.rightClick()
                pyautogui.press('esc') 
                time.sleep(0.5)

        if isFound:
            for _ in range(3):
                pyautogui.click(pos_x,pos_y,duration=0.05)
                logger.debug ('点一下') 
                modcheck = (pos_x-100,pos_y-25,2,2)
                # print (modcheck)
                im = pyautogui.screenshot(region = modcheck)  
                # im.save('D:/test.png') 
                opencolor = [217,207,173]
                pt = im.getpixel((0,0))
                isbattlesel = self.isSimilarColor(pt,opencolor)
                if isbattlesel:
                    self.px = pos_x
                    self.py = pos_y
                    return True
        return False

    def out(self):
        ans = self.open_sys_window()
        if ans:
            time.sleep(0.1)
            pyautogui.click(self.px-150,self.py+15,duration=0.1,clicks=1,interval=0.1)
            # now = datetime.now() # current date and time
            date_time = time.strftime("%Y-%m-%d, %H:%M:%S",time.localtime())
            print("date and time:",date_time)

    def back(self):
        ans = self.open_sys_window()
        if ans:
            time.sleep(2)
            pyautogui.click(self.px-150,self.py-15,duration=0.1,clicks=1,interval=0.1)

if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    test = logout(m)

    ans = test.out()
    print (ans)

