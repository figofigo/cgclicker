#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-31 10:08:47
LastEditTime: 2024-02-02 01:31:49
LastEditors: figo - uz
Description: 战斗模块
inhert: battle_query
FilePath: \cgclicker\bin\battle.py
copyright: figo software 2020-2021
'''

import sys
import math
import logging

sys.path.append('.')
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('battle_actions')
logger.setLevel(logging.INFO)
import time
import json
import pyautogui
import os
import random

from bin import battle_query


class battle(battle_query.battle_query):
    def __init__(self,rect) :
        super(battle,self).__init__(rect=rect)
        
        self.rect = self.real_rect(rect)
        # 截图检查的时候，确定一下区域

        filePath = os.path.abspath(__file__).replace('bin','')
        rootPath = os.path.dirname(filePath)
        self.sourcepath = '%s\\resources\\battle' % rootPath        
        self.mosterpath = '%s\\resources\\moster' % rootPath

        self.lastSkillWindowPos = []
        # 人物找到技能栏位置之后，记录下来。
        self.lastSkillSlotPos = []
        self.lastSkillLevelPos = []
        # 人物快速施法使用的
        self.lastEnermyStatus = None
        self.lastHumanStatus = None

        # self.checkredblue = signal('checkredblue')
        # self.loopDone = signal('loop_done')


    def petCastSkill(self,slot,target='enermy',repeat=5):
        petwinpic = "%s\\pet_skill_start.png" % self.sourcepath
        isSel = self.isPetSkillSelected()
        if isSel == 'error':
            return False
        
        if isSel == 'wait':
            # ans = None
            isFoundWindow = False
            for i in range(repeat):
                try:
                    ans = pyautogui.locateCenterOnScreen(petwinpic,region = self.rect)
                except:
                    ans = None
                    time.sleep(0.2)
                finally:
                    if not ans:
                        if i>3:
                            self.resetMouse()
                            pyautogui.click()
                        pyautogui.rightClick()
                    else:
                        isFoundWindow = True
                        break
                
            if not isFoundWindow:
                logger.warn('没有找到技能窗口')
                return False
            else:
                offset_x = self.offset_x(val=50,noneg=True)
                offset_y = (slot+1) * self.skillSlotHeight #+16
                pos_x = ans[0] + offset_x
                pos_y = ans[1] + offset_y
                pyautogui.moveTo(pos_x,pos_y,duration = 0.2)
                pyautogui.click()
                # 使用技能


        if target == 'enermy':
            positiondata  = self.mosterState()
            list_val = []
            for i in positiondata.values():
                if i['ishas']:
                    list_val.append(i)
            rdseed = random.random()
            if rdseed >= 0.5:
                list_val.sort(key=lambda x:str(x['id']),reverse=True)

            for tg in list_val:
                x = tg['pos'][0]
                y = tg['pos'][1]
                pyautogui.moveTo(x,y)
                pyautogui.click()
        return True
                
    
    def humanCastSkill(self,slot,level=0,target='enermy'):
        '''
        description: 人物施展技能，对敌人按照随机排序，对自身按照血线排序
        param {*} self
        param {*} slot 指定使用几号位技能
        param {*} level 默认0，使用最高级技能
        param {*} target 指定敌人还是指定自身。
        return {*}
        '''

        if self.is_big_window():
            # print ('big')
            pos = [600,30]
        else:
            # print ('small')
            pos = [440,30]
        localx = self.localx(pos[0])
        localy = self.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        for i in range (3):
            pyautogui.click()

        try:
            self.skillSlot(slot,withclick=1)
        except:
            # 在这里做一个未检测到图像时的反馈
            logger.warning('没有找到战斗中技能窗口的位置')
            return False
        finally:
            time.sleep(0.2)
        try:
            self.skillSlot_maxlevel(querylevel=level,withclick=True,isValue=True,isPreview=False)
        
        except:
            # 在这里做一个未检测到图像时的反馈
            logger.warning('没有找到战斗中技能窗口的位置')
            return False
        finally:
            time.sleep(0.2)
            
        if target == 'enermy':
            positiondata  = self.mosterState()
            list_val = []
            for i in positiondata.values():
                if i['ishas']:
                    list_val.append(i)
            rdseed = random.random()
            if rdseed >= 0.5:
                list_val.sort(key=lambda x:str(x['id']),reverse=True)
            tg = list_val[0]
            x = tg['pos'][0]
            y = tg['pos'][1]
        else:
            positiondata = self.humanState()
            list_val = []
            for i in positiondata.values():
                if i['ishas']:
                    list_val.append(i)
            # list_val.sort(key=lambda x:str(x['red']),reverse=False)
            # print (list_val)
            lowest = 100
            for i in list_val:
                keyval = int(i['red'])
                if keyval < lowest:
                    lowest = keyval
                    x = i['pos'][0]
                    y = i['pos'][1]
        returnVal = True
        try:
            pyautogui.moveTo(x,y)
            pyautogui.click(clicks=2,interval=0.2)
        except:
            print ('没有获取到人物的信息，施展失败')
            returnVal = False

        finally:
            return returnVal
        

    def attack(self):
        if self.is_big_window():
            pos = [550,30] 
        else:
            pos = [390,30]
        localx = self.localx(pos[0])
        localy = self.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        pyautogui.click()

        positiondata  = self.mosterState()
        list_val = []
        for i in positiondata.values():
            if i['ishas']:
                list_val.append(i)
        rdseed = random.random()
        if rdseed >= 0.5:
            list_val.sort(key=lambda x:str(x['id']),reverse=True)

        for tg in list_val:
            x = tg['pos'][0]
            y = tg['pos'][1]
            pyautogui.moveTo(x,y)
            pyautogui.click()
        return True
    
    def escape(self):    
        if self.is_big_window():
            pos = [750,50]
        else:
            pos = [590,50]
        localx = self.localx(pos[0])
        localy = self.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        pyautogui.click()

    def guard(self): 
        if self.is_big_window():
            pos = [550,50] 
        else:
            pos = [390,50]
        localx = self.localx(pos[0])
        localy = self.localy(pos[1])
        pyautogui.moveTo(localx,localy)    
        pyautogui.click()

        

    def action(self,target={'human':[1,'self'],'pet':[1,'enermy']},repeat=3,isCloseCombat=False):
        # self.placeCursor('center')
        self.resetMouse()
        pyautogui.rightClick()
        # 聚焦到当前窗口
        ishumanok = False
        ispetok = False
        for i in range(repeat):

            check = self.humanCastSkill(target['human'][0],target=target['human'][1],repeat=repeat,isCloseCombat=isCloseCombat)
            if check:
                logger.info('human cast success')
            else:
                continue

            time.sleep(0.5)
            
            step_check = self.status()
            if step_check == 'pet':
                ishumanok = True
                check_pet = self.petCastSkill(target['pet'][0],target=target['pet'][1],repeat=repeat)
                if check_pet:
                    ispetok = True
            else:
                continue
            
            if ispetok and ishumanok:
                return True


        return False





if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    # default_region = (m[0],m[1],m[2]-m[0],m[3]-m[1])
    test = battle(m)
    gg = test.humanCastSkill(1,target='self')
    # gg = test.petCastSkill(1,target='enermy')
    print (gg)
