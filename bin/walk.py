#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2023-12-15 00:26:54
LastEditTime: 2024-02-16 21:18:56
LastEditors: figo - uz
Description: 负责线性移动或者随机遇敌的模块
FilePath: \cgclicker\bin\walk.py
copyright: figo software 2020-2021
'''

import math
import random
import time
import keyboard
import sys
import logging
sys.path.append('.')
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('walksys')
logger.setLevel(logging.WARNING)

try:
    from bin import coordsys
except ModuleNotFoundError:
    from . import coordsys
finally:pass


class walksys(coordsys.coordsys):
    
    def __init__(self,rect,mark='default'):
        '''
        description: 
        rect 这里的数据是从win32里面拿到的，没有进行title偏移过的数据
        return {*}
        '''        

        super(walksys,self).__init__(rect=rect)
                
        self.rect = self.real_rect(rect)
        # 这个修正取的是画面中真正的rect，不包含边框和标题
        print('self.pos',self.pos)
        self.st_pos = self.pos
        self.mark = mark
        self.data = self.getSet(self.mark)

    def reset_stpos(self):
        '''
        description: 重置随机移动的起始点
        return {*}
        '''        
        self.st_pos = self.pos(f=1)

    def getSet(self,mark):
        rtdata = {}
        if mark == 'banshan':
            rtdata= {
                        'module':'random',
                        'direct':'70.63',
                        'distance':4,
                        'count':1000,
                        'waitTime':0.01
                    }
        elif mark == 'xierwei':
            rtdata= {
                        'module':'random',
                        'direct':'357.192',
                        'distance':3,
                        'count':1000,
                        'waitTime':0.01
                    }  
        elif mark == 'jienuowa':
            rtdata= {
                        'module':'random',
                        'direct':'211.456',
                        'distance':3,
                        'count':1000,
                        'waitTime':0.01
                    }        
        elif mark == 'kanniu':
            rtdata= {
                        'module':'random',
                        'direct':'35.80',
                        'distance':4,
                        'count':1000,
                        'waitTime':0.01
                    }
        elif mark in ['xuanwu','huodong']:
            self.reset_stpos()
            gete_x = self.st_pos[0]
            gate_y = self.st_pos[1]
            current_direction = self.get_direction_of_random_map()
            if current_direction == 'west':
                direct_str = '%i.%i' % (gete_x-5,gate_y)
            elif current_direction == 'east':
                direct_str = '%i.%i' % (gete_x+5,gate_y)
            elif current_direction == 'north':
                direct_str = '%i.%i' % (gete_x,gate_y-5)
            else:
                direct_str = '%i.%i' % (gete_x,gate_y+5)
            rtdata= {
                        'module':'random',
                        'direct':direct_str,
                        'distance':2,
                        'count':1000,
                        'waitTime':0.01
                    }
        
        elif mark == 'ns':
            rtdata= {
                        'module':'linear',
                        'direct':'n.s',
                        'distance':3,
                        'count':1000,
                        'waitTime':0.005
                    }
        
        elif mark == 'ew':
            rtdata= {
                        'module':'linear',
                        'direct':'e.w',
                        'distance':3,
                        'count':1000,
                        'waitTime':0.005
                    }
        
        else:
            rtdata= {
                        'module':'random',
                        'direct':'%i.%i' % (self.st_pos[0],self.st_pos[1]),
                        'distance':2,
                        'count':100000,
                        'waitTime':0.01
                    }

        return rtdata


    def walk_while_battle(self):
        data = self.data
        module = data['module']
        directList = data['direct'].split('.')
        distance = data['distance']
        clicks = 3
        waitTime = data['waitTime']        
        count = data['count']

        lastPos = [0,0]
        lastKey = 0
        startPos = [0,0]
        random_list = []
        if module =='random':
            startPos =[int(i) for i in directList]
            datalist = self.get_random_range_list(distance)

            random_list = []
            for strpos in datalist:
                intpos = [int(i) for i in strpos.split(',')]
                tarpos = [pos_og + offsetpos for pos_og,offsetpos in zip(startPos,intpos)]
                random_list.append(tarpos)

            halfkey = int(round(len(random_list)*0.5,0))
            logger.info(f'halfkey is {halfkey}')
            fullkey = len(random_list)
            logger.info(f'fullkey is {fullkey}')


        for i in range(count):              
            if keyboard.is_pressed('alt'): 
                raise KeyboardInterrupt ('手动停止自动循环')

            if module == 'linear':
                for dt in directList:  
                    # 按照线性列表的指示运行一圈
                    randomval = 0
                    self.walkTo(dt,
                                    distance=distance+randomval,
                                    clicks=clicks,
                                    wait=waitTime)
                    self.goto(self.st_pos,check=1,limitrange=distance)
                logger.debug('walk to [%s] - linear' % dt)
            elif module == 'p2p':
                spi = dt.split('-')
                pt = ([int(spi[0]),int(spi[1])])
                self.goto(pt,check=1,limitrange=distance)
                logger.debug('walk to %s - p2p' % str(pt))
            else:

                rd_seed = (random.random()-0.5)*2.0
                # 标号的随机偏移
                offset_key = int(rd_seed*(distance*1.0))
                # logger.debug (rd_seed,offset_key,distance)
                # 将随机偏移和distance关联
                tarpos = random_list[lastKey]
                self.goto(tarpos,check=0,duration=waitTime,limitrange=4)
                lastKey = int((halfkey + offset_key + lastKey) % fullkey)
                # logger.debug (lastKey)
                # 生成新的key
                logger.debug('walk to [%i,%i] - random' % (tarpos[0],tarpos[1]))
            isbattleend = self.status()
            if isbattleend not in ['peace']:
                return True


            logger.info('第 %s/%s 次循环' % (str(i),str(count)))
        return True
    
    def get_random_range_list(self,r):
        '''
        description: 根据半径依次获取圆上坐标
        return {*}
        '''        
        datalist = []
        for angle in range(360):
            x = r * math.cos(angle*math.pi/180.0)
            y = r * math.sin(angle*math.pi/180.0)
            insert_chat = f'{int(round(x,0))},{int(round(y,0))}'
            if insert_chat not in datalist:
                datalist.append(insert_chat)

        return datalist
    
    def set_mark(self,mark):
        self.mark = mark
        self.data = self.getSet(mark=mark)
        logger.warning('当前walk系统切换为[ %s ]' % mark)
    

    def main(self):
        self.walk_while_battle()

if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    wk = walksys(m)
    wk.main(mark='default')

