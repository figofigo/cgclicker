#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2024-03-30 23:25:47
LastEditTime: 2024-06-04 23:20:30
LastEditors: figo - uz
Description: 监控当前窗口的信息
copyright: figo software 2020-2021
'''

import math
import os
import random
import time
import win32com.client
import win32process
import win32gui
import psutil
import sys
import pyautogui
import logging
sys.path.append('.')
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('winstatus')
logger.setLevel(logging.DEBUG)

from util import util
from util import source_check
from util import module_autobattle
from util import module_bag
from util import module_coord
from util import module_cast
from util import module_mail
from util import module_duihua

class win_status(util.basic):
    def __init__(self,parent):
        self.parent = parent # win get area data
        
        self._handle = None

        self._window_id = 0
        self._window_rect = []
        self._window_size = []
        self._window_pos = []

        self._distance = 0

        self._status = 'peace'
        self._red = 0
        self._blue = 0
        self._red_max = -1
        self._blue_max = -1
        self._map = ''
        self._restart_time = 0
        self._start_path = ''
        
        self.source = source_check.source_check(self)
        self.autobattle = module_autobattle.autobattle_check(self)
        # self.autobattle.reset_window_pos()
        # self.autobattle.serialize()
        self.bag = module_bag.itemSystem(self)
        # self.bag.eat()
        self.map = module_coord.coordsys(self)
        # self.map.serialize()
        self.cast = module_cast.cast(self)

        self.duihua = module_duihua.module_duihua(self)
        
        self.mail = module_mail.mailsys(self)
        # self.mail.serialize()
        # self.bag.serialize()
        self.extra_dict = {}

    def set_handle(self,val):
        self._handle = val
    
    @property
    def handle(self):
        return self._handle

    @property
    def red(self):
        return self.source.red()
    
    @property
    def blue(self):
        return self.source.blue()
    
    @property
    def redmax(self):
        if self._red_max < 0:
            self._red_max = self.source.red_max()
        elif self._red_max == 0:
            self._red_max = -1
        return self._red_max
    
    @property
    def bluemax(self):
        if self._blue_max < 0:
            self._blue_max = self.source.blue_max()
        elif self._blue_max == 0:
            self._blue_max = -1
        return self._blue_max

    @property
    def rect(self):
        m = win32gui.GetWindowRect(self.parent)
        self._window_rect = (m[0],m[1],m[2]-m[0],m[3]-m[1])
        return self._window_rect
    
    @property
    def size(self):
        m = win32gui.GetWindowRect(self.parent)
        self._window_size = (m[2]-m[0]-6,m[3]-m[1]-29)
        return self._window_size
    
    @property
    def pos(self):
        m = win32gui.GetWindowRect(self.parent)
        self._window_pos = (m[0],m[1])
        return self._window_pos


    def set_windows_id(self,idval):
        self._window_id = idval
    @property
    def window_id(self):
        return self._window_id
        
    def get_window_executable_path(self,hwnd):
        thread_id, process_id = win32process.GetWindowThreadProcessId(hwnd)
        process = psutil.Process(process_id)
        executable_path = process.exe()
        executable_path = os.path.realpath(executable_path)
        return executable_path
        
    @property
    def path(self):
        return self.get_window_executable_path(self.parent)

    @property
    def distance(self):
        rect = self.rect
        powx = math.pow(rect[0],2)
        powy = math.pow(rect[1],2)
        distance = math.sqrt(powx+powy)
        return distance

    def localx(self,offsetval):
        '''
        description: 尝试返回，基于窗口原点局部坐标的世界位置 x 轴
        return {*}
        '''        
        returnval = self.pos[0] + offsetval + 3
        return returnval


    def localy(self,offsetval):
        '''
        description: 尝试返回，基于窗口原点局部坐标的世界位置 y 轴
        return {*}
        '''        
        returnval = self.pos[1] + offsetval + 26
        return returnval


    def localpos(self,offsetlist):
        '''
        description: 尝试返回，基于窗口原点局部坐标的世界位置 [x,y] 轴
        return {*}
        '''        
        return [self.pos[0] + offsetlist[0] + 3,self.pos[1] + offsetlist[1]+26]


    def wrangle_pos(self,pos,xoffset = 10,yoffset = 10):
        rseed_x = random.random()-0.5
        rseed_y = random.random()-0.5
        x_add = xoffset*rseed_x
        y_add = yoffset*rseed_y
        return [pos[0]+x_add,pos[1]+y_add]
    
    def resetMouse(self,pos = []):
        if not pos:
            pos = [87,23]
        rseed = random.random()
        offsetx = int(rseed*80) + pos[0] #87
        offsety = int(rseed*10) + pos[1] #23
        # pyautogui.moveTo(self.localpos([offsetx,offsety]))   
        pyautogui.moveTo(self.localpos(pos))   

    def isloading(self):
        checklength = 90
        modcheck = (self.localx(10),self.localy(27),checklength ,1)
        im = pyautogui.screenshot(region = modcheck)
        others = 0
        for i in range(checklength):
            lineval = im.getpixel((i,0))
            sumnum =  (sum(lineval))
            if sumnum<600:
                others += 1
                # print (sumnum)

        if others:
            return False
        else:
            return True
        
    @property
    def status(self):
        '''
        description: 检查当前是在那个状态，战斗外/切换/战斗动画/战斗选择
        return {*}
        '''        
        # 战斗开始后。会有人物指令，宠物指令，还有动画阶段
        # logger.debug('?? 查询当前战斗步骤')

        # size = 20 # 识别窗口右上角，size见方的区域
        win_width = self.size[0]
        x = self.localx(win_width-14) 
        y = self.localy(2)
        dataArea = (x,y,1,3)
        
        im = pyautogui.screenshot(region=dataArea)
        # im.save('d:/status_check.png')
        # 完成区域指定

        peace={'0':(233, 228, 204),
                    '1':(192, 146, 61),
                    '2':(248, 225, 175)
                    }
        
        battle_sel={'2':None,
                    '1':None,
                    '0':(105, 85, 29)
                    }

        battle_pet = {'human_off':(212,173,106),
                    'human_on':(147,187,108),
                    'white':(250, 247, 236),
                    'hover':(147,147,147)
                    }

        pt0 = im.getpixel((0,0))
        
        if sum(pt0)<20:
            return "loading"
        else:
            isbattlesel = self.isSimilarColor(pt0,battle_sel["0"])

            if isbattlesel:
                pet_x = self.localx(win_width-180) 
                pet_y = self.localy(50)
                pet_dataArea = (pet_x,pet_y,1,3)
                
                pet_im = pyautogui.screenshot(region=pet_dataArea)
                # pet_im.save('d:/status_check.png')
                pet_pt = pet_im.getpixel((0,2))
                for key,val in battle_pet.items():
                    petcheck = self.isSimilarColor(pet_pt,val)
                    if petcheck:
                        if key in ['human_off','human_on','hover']:
                            return 'battle_sel'
                        elif key in ['white']:
                            return 'pet'
                        else:
                            continue
                    else:
                        continue


            ispeace2 = self.isSimilarColor(pt0,peace["0"])
            if ispeace2:
                pt1 = im.getpixel((0,1))
                ispeace1 = self.isSimilarColor(pt1,peace["1"])
                if ispeace1:
                    return ('peace')


        return "animation"



    def reset_item_window_pos(self):
        pos = [125,self.size[1]-170]
        statuspath = '%s\\resources\\status' % self.root_path()
        onmark = 'itemBag.png'
        onPath = '%s\\%s' % (statuspath,onmark)
        
        for repeat in range(5):
            self.resetMouse()
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect) 
            except:
                ans = None
            finally:
                targetpos = self.localpos[pos]
            
            
            if not ans:
                isfoundopenbag = False
                self.resetMouse()
                pyautogui.rightClick()
                for i in range(3):       
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('e') 
                    pyautogui.keyUp('ctrl')
                    for k in range(5):
                        try:
                            ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)  
                        except:
                            ans = None
                        finally:
                            pass
                        if ans:
                            isfoundopenbag = True
                            break
                        else:
                            time.sleep(0.1)
                    if isfoundopenbag:
                        break
            dis = self.getDistance(targetpos,ans)
            if dis <2:
                pyautogui.keyDown('ctrl') 
                pyautogui.press('e') 
                pyautogui.keyUp('ctrl')
                return True
            else:
                pyautogui.moveTo(ans[0],ans[1])
            
                pyautogui.dragTo(self.rect[0]+pos[0],self.rect[1]+pos[1],duration=1,mouseDownUp=True)
        return True
    
    def set_to_top(self):
        win32gui.BringWindowToTop(self.handle)
        shell = win32com.client.Dispatch("WScript.Shell")
        shell.SendKeys('%')
        win32gui.SetForegroundWindow(self.handle)

    def testfn(self):
        self.cast.action(target={'human':[1,'enermy'],'pet':[2,None]})


    #@util.timeit
    def serialize(self):
        self.set_to_top()
        # self.cast.serialize()
        ab = self.autobattle.serialize()
        # print ('ab',ab)

        # self.duihua.serialize()
        # if self.window_id == 0:
        # if self.status in ['battle_sel','pet']:
            # self.cast.action(target={'human':[1,'enermy'],'pet':[2,None]})

        serial_data = {
            'status':self.status,
            'rect':self.rect,
            'distance':self.distance,
            'path':self.path,
            'window_id':self.window_id,
            'red':self.red,
            'blue':self.blue,
            'red_max':self.redmax,
            'blue_max':self.bluemax,
        }
        serial_data.update(ab)
        return serial_data
    
    
# if __name__ == "__main__":
#     duihua = win_status()
#     duihua.serialize()