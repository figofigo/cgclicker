#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-29 05:58:29
LastEditTime: 2025-02-17 23:50:18
LastEditors: figo - uz
Description: 基于地图窗口坐标的位置查询系统
inhert: basic.basic
copyright: figo software 2020-2021
'''
import random
import os
import sys
sys.path.append('.')
import time
import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('whereAmI')
logger.setLevel(logging.INFO)

import pyautogui
import PIL
from bin import basic

class whereAmI(basic.basic):
    def __init__(self,rect) :
        super(whereAmI,self).__init__()

        rootPath = self.root_path()
        self.sourcepath = '%s\\resources\\where' % rootPath
        self.numberpath = '%s\\resources\\where\\number' % rootPath
        self.numberDict = self.initNumber()
        self.rect = self.real_rect(rect)

        self.num_wid = 5
        self.num_hgt = 8

        self.map_pos = (0,0,0,0)
        # newpos = self.isMapopen(isval=True)
        # if not isinstance(newpos,bool):
        #     # 返回值如果不是False，就是坐标
        #     self.map_pos = newpos

    def searchFunction(self,picname):
        mark = '%s.png' % picname
        Path = '%s\\%s' % (self.sourcepath,mark)
        for i in range(10):
            isblack = self.isloading()
            if isblack:
                logger.debug('loading',i,'/ 5')
                time.sleep(0.1)
            else:
                break
        try:
            ans = pyautogui.locateCenterOnScreen(Path,region = self.rect)
        except:
            ans = None
        finally:
            pass
        
        if ans:
            logger.info('现在在[%s]' % picname)
            return True
        else:
            logger.debug('无法获取到地图信息')
            return False

    def isXuanwu20(self):
        
        onmark = 'xuanwu20'
        ans = self.searchFunction(onmark)
        return ans

    def isXuanwuzhijing(self):
        
        onmark = 'xuanwuzhijing'
        ans = self.searchFunction(onmark)
        return ans



    def isHospital(self):
        onmark = 'yiyuan'
        ans = self.searchFunction(onmark)
        return ans

    def isShalianna(self):
        
        onmark = 'shalianna'
        ans = self.searchFunction(onmark)
        return ans

    def isJienuowa(self):
        
        onmark = 'jienuowa'
        ans = self.searchFunction(onmark)
        return ans

    
    def isMatata(self):
        
        onmark = 'matatapingyuan'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isNowhere(self):
        
        onmark = 'nowhere'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isYinmishanlu(self):
        
        onmark = 'yinmishanlu'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isHuanzhididiyiji(self):
        
        onmark = 'huanzhididiyiji'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isHuodong(self):
        
        onmark = 'huodong'
        
        ans = self.searchFunction(onmark)
        return ans
    
    def isKulukesi(self):
        
        onmark = 'kulukesi'
        
        ans = self.searchFunction(onmark)
        return ans

    
    def isLeiketaer(self):
        
        onmark = 'leiketaer'
        
        ans = self.searchFunction(onmark)
        return ans

    def isXierwei(self):
        
        onmark = 'xierweicun'
        ans = self.searchFunction(onmark)
        return ans

    def saveMark(self,bbox_orig,saveDir):
        '''
        description: 将指定的坐标信息存成图片的功能
        return {*}
        '''        
        if not os.path.exists(saveDir):
            os.makedirs(saveDir)
        saveFile = saveDir + '\\' + str(time.time()) + '.png'

        real_region = [ self.localx(bbox_orig[0]),
                        self.localy(bbox_orig[1]),
                        self.localx(bbox_orig[2]),
                        self.localy(bbox_orig[3])]
        im = PIL.ImageGrab.grab(real_region)
        im.save(saveFile)
        return saveFile

    def initNumber(self):
        '''
        description: 将数字装载到字典中
        return {*}
        '''        
        rtList = {}
        filelist = os.listdir(self.numberpath)
        for i in filelist:
            key = i.split('.')[0]
            fullpath = '%s\\%s' % (self.numberpath,i)
            if os.path.isfile(fullpath):
                rtList[key] = fullpath
        return rtList

    def reset_mapwindow_pos(self,q=0):
        '''
        description: 将地图窗口移动到指定位置
        return {*}
        '''     
        self.setMapopen(True)

        if self.wintype == 800:
            tx=self.localx(700)
            ty=self.localy(35)
        else:
            tx=self.localx(100)
            ty=self.localy(370)
        
        dragoffset_x = 20

        if q:
            nowpos = self.isMapopen(isval=True)
            localpos = [nowpos[0] - self.rect[0],nowpos[1] - self.rect[1]]
            print ('local pos',localpos)
            return [tx,ty]
        
        isSuccess = False
        for i in range(10):
            logger.info('moveloop[%i] ' % i)
            baseinfo = self.isMapopen(isval=True)
            if not baseinfo:
                time.sleep(2)
                continue
            sx = baseinfo[0]
            sy = baseinfo[1]

            offset_x = tx-sx
            offset_y = ty-sy

            if (offset_x + offset_y + dragoffset_x) == 0:
                logger.info('done')
                isSuccess = True
                break
            else:
                logger.info('offset [%i,%i]' % (offset_x +dragoffset_x,offset_y))
                pyautogui.moveTo(sx+dragoffset_x,sy,duration=0.2)
                pyautogui.dragRel(offset_x+dragoffset_x,offset_y,duration=1)
        return isSuccess





    def setMapopen(self,val,f=1):
        
        if isinstance(val,bool):
            if f:
                rdn = int(random.random() * 10)
                # pyautogui.moveTo(self.rect[0]+80+rdn,self.rect[1]+80+rdn,duration=0.1)
                pyautogui.rightClick(self.localx(450+rdn),
                                    self.localy(120+rdn),
                                    duration=0.1)

            nowStatus = self.isMapopen(isval=False)

            if nowStatus == val == True:
                self.map_pos = self.isMapopen(isval=True)
                logger.debug('setmap way ALL True')
                logger.debug(self.map_pos)
            elif nowStatus == False and val == True:
                logger.info('open map')
                pyautogui.keyDown('ctrl') 
                pyautogui.press('s') 
                pyautogui.keyUp('ctrl')
                time.sleep(1)
                self.map_pos = self.isMapopen(isval=True)
            elif nowStatus == True and val == False:
                logger.info('close map')
                pyautogui.keyDown('ctrl') 
                pyautogui.press('s') 
                pyautogui.keyUp('ctrl')
                self.map_pos =(0,0)
            else:
                logger.info('setmap way ALL False')
                self.map_pos =(0,0)
        return True



    def get_number(self,offsetx):
        '''
        description: 查x轴百位
        return {*}
        '''     
        if (isinstance(self.map_pos,bool)):
            logger.warning ('地图位置获取失败，请确认是否已经打开了地图')
            return -1

        if self.map_pos == (0,0,0,0):
            raise ValueError ('地图数值获取为 (0,0,0,0),确认寻址模块是否已经获取过内容')


        if not (isinstance(self.map_pos,bool)):
            # print(self.map_pos)
            tgx = (self.map_pos.x + offsetx)
            tgy = (self.map_pos.y + 15)

            

            search_region = (tgx,tgy,self.num_wid,self.num_hgt)
            # print (search_region)

            im = pyautogui.screenshot(region=search_region)

            # im.save('d:/testnumber%s.png' % str(offsetx))
            # print (99999999999999999999999999)
            # raise

            firstLoop = {'check1':(1,1),'check2':(4,1),'check3':(1,4)}
            nextLoop = {}

            firstData = {}
            for key,val in firstLoop.items():
                temp = im.getpixel(val)
                firstData[key] = int(sum(temp) > 150)
            # 这一波可以判断出来6,1,9,[4,5],[0.2.3.7.8]

            
            checkStr = '%(check1)i%(check2)i%(check3)i' % firstData

            if checkStr == '111':
                return 6
            elif checkStr == '011':
                return 9
            elif checkStr == '100':
                return 1
            elif checkStr == '000':
                temp = im.getpixel((2,3))
                ans = (sum(temp) > 150)
                if ans:
                    return 5
                else:
                    temp2 = im.getpixel((1,3))
                    ans2 = (sum(temp2) > 150)
                    if ans2:
                        return 4
                    else:
                        return 0
            else:
                nextLoop = {'check1':(0,2),'check2':(0,3),'check3':(2,3)}

                nextData = {}
                for key,val in nextLoop.items():
                    temp = im.getpixel(val)
                    nextData[key] = int(sum(temp) > 150)


                nextStr = '%(check1)i%(check2)i%(check3)i' % nextData

                if nextStr == '000':
                    return 7
                elif nextStr == '100':
                    return 2
                elif nextStr == '101':
                    return 8
                elif nextStr == '110':
                    return 0
                elif nextStr == '001':
                    return 3
                else:
                    # 空白为，没有数字
                    return 0

                
            # 如果在循环里没找到，就是0
            return 0
        else:
            logger.debug(self.map_pos)
            logger.debug ('地图没打开，获取不到地图基础坐标')
            return False


    def get_pos(self,direct,testtime=False):
        '''
        description: 获取坐标的功能
        direct {str} 只接受x与y
        return {int}
        '''        

        if direct == 'x':
            offsets = {'ones':-43,'tens':-49,'hundreds':-55}
        else:
            offsets = {'ones':65,'tens':59,'hundreds':53}

        ons = self.get_number(offsets['ones'])
        # print ('ones',ons)
        if ons>=0:
            tens = self.get_number(offsets['tens'])
            # print ('tens',tens)
        else:
            return -1
        
        if ons>=0 and tens>=0:
            hundreds = self.get_number(offsets['hundreds'])
            # print ('hundreds',hundreds)
        else:
            return -1


        value = ons+(tens*10) + (hundreds*100)

        return value
    
    @property
    def pos(self):
        '''
        description: 地图模块的坐标
        return {*}
        '''   
        pic_list = ['ditu.png','ditu2.png']
        for pic in pic_list:
            onPath = '%s\\%s' % (self.sourcepath,pic)
            print ('onpath',onPath)
            print ('self.rect',self.rect)
            try:
                ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)
                isfound = True
            except:
                pass
            finally:
                if isfound:
                    return ans

        return None

    def coord(self):
        
        thex = self.get_pos('x')
        they = self.get_pos('y')

        return [thex,they]
    def serialize(self):
        coord = self.coord()
        print (coord)

if __name__ == "__main__":
    from util import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.wins[0].rect
    print (m)
    whr = whereAmI(m)
    # whr.logNumber()
    gets = whr.coord()
    
    # gets = whr.pos()
    print (gets)