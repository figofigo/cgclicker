#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2022-05-27 23:30:58
LastEditTime: 2024-01-04 00:56:47
LastEditors: figo - uz
Description: 和战斗有关的查询功能
inhert:basic.basic
copyright: figo software 2020-2021
'''

import os
import sys
sys.path.append('.')
import math
import logging
import PIL
import ctypes

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('cgclicker')
logger.setLevel(logging.INFO)
import time
import json
import pyautogui
import os
import random

from bin import basic

class battle_query(basic.basic):
    def __init__(self,rect):
        super(battle_query,self).__init__()
        self.rect = self.real_rect(rect)
        # 截图检查的时候，确定一下区域

        rootPath = self.root_path()
        self.sourcepath = '%s\\resources\\battle' % rootPath        
        self.mosterpath = '%s\\resources\\moster' % rootPath

        self.lastSkillWindowPos = [0,0]
        # 人物找到技能栏位置之后，记录下来。
        self.lastSkillSlotPos = [0,0]
        self.lastSkillLevelPos = [0,0]
        # 人物快速施法使用的
        self.lastEnermyStatus = None
        self.lastHumanStatus = None

        self.skillSlotHeight = 16
        

    def number_indentyfy(self,data_soup):
        '''
        description: 根据data_soup分析数字
        return {*}
        '''    
        dataparttern = {
            'e':"0000000",#[False,False,False,False,False,False,False],
            '0':"0111100",#[False,True ,True ,True ,True ,False,False],
            '1':"0100011",#[False,True ,False,False,False,True ,True ],
            '2':"0101001",#[False,True ,False,True ,False,False,True ],
            '3':"0111001",#[False,True, True, True, False,False, True],
            '4':"0011011",#[False,False,True ,True ,False,True ,True ],
            '5':"1111100",#[True ,True ,True ,True ,True ,False,False],
            '6':"0111110",#[False,True ,True ,True ,True ,True ,False],
            '7':"1001011",#[True ,False,False,True ,False,True ,True ],
            '8':"0111101",#[False,True ,True ,True ,True ,False,True ],
            '9':"0101100",#[False,True ,False,True ,True ,False,False],
            'w':"1111111",#[True ,True ,True ,True ,True ,True ,True ],


        }
        input_barray = int(data_soup)
        for key,val in dataparttern.items():
            
            calcu = input_barray - int(val)
            if calcu == 0:
                return key
            # else:
        print ('error',data_soup)
        return False
                

    def shelf_mode(self):
        '''
        description: 判断左上角状态栏处于哪个状态
        return {*}
        '''        
        checkheight = 6
        modcheck = (self.localx(137),self.localy(17),1 ,checkheight)
        im = pyautogui.screenshot(region = modcheck)
        # im.save('d:/modline.png')
        vallist = []
        for i in range(checkheight):
            lineval = im.getpixel((0,i))
            vallist.append (sum(lineval))
        # print (vallist)
        cktop = 138<vallist[0] < 142
        ckbottom = 138<vallist[-1] < 142
        ckmid = (vallist[3]==396) or (vallist[3]==734)

        if not ckmid:
            return "close"
        elif ckbottom and not cktop :
            return "normal"
        elif cktop and not ckbottom:
            return "shrink"
        else:
            logger.warn('error at check shelf mod')
            return "error"

    # @basic.timeit
    def red(self,maskcode = '1110'): 
        '''
        description: 新版血查询
        return {*}
        '''               
        logger.debug('查询当前角色血量')
        
        shelfmod = self.shelf_mode()
        if shelfmod == 'normal':
            y_axis = 19
            x_base = 21
            logger.debug('in normal mode')
        elif shelfmod == 'shrink':
            y_axis = 2
            x_base = 21
            logger.debug('in shrink mode')
        else:
            logger.debug('in close mode')
            return -1
        # 判断当前状态栏处在什么状态，并初始化范围数据

        areadict_soup = {
            "ten":(self.localx(x_base+14),self.localy(y_axis),3 ,7),
            'hundred':(self.localx(x_base+7),self.localy(y_axis),3 ,7),
            'unit':(self.localx(x_base+21),self.localy(y_axis),3 ,7),
            'thousand':(self.localx(x_base),self.localy(y_axis),3 ,7),
        }

        areadict = {}
        if maskcode[0] == '1':
            areadict['thousand'] = areadict_soup['thousand']
        if maskcode[1] == '1':
            areadict['hundred'] = areadict_soup['hundred']
        if maskcode[2] == '1':
            areadict['ten'] = areadict_soup['ten']
        if maskcode[3] == '1':
            areadict['unit'] = areadict_soup['unit']
        # 根据掩码，继续优化数据源

        keypoint_list = [0,1,6 ,7,10,17,18]
        # 优化过的查询点信息

        result_dict = {}

        for key,val in areadict.items():
            im = pyautogui.screenshot(region = val)
            width, height = im.size
            pic_datasoup = ''
            # 初始化data_soup
            count = 0
            for wid in range(width):
                for hgt in range(height):
                    if count in keypoint_list:
                        # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                        get_R = im.getpixel((wid,hgt))[0]
                        # im.putpixel((wid,hgt),(255,0,0))
                        # im.save('d:/%s.png' % key) 
                        if get_R <100:
                            pic_datasoup+='1'
                            # 根据优化过的点数据，获取相应的点颜色
                        else:
                            pic_datasoup+='0'
                    count+=1
            # print (pic_datasoup)
            thenumber = self.number_indentyfy(pic_datasoup)
            # print (thenumber)
            if thenumber == 'e' or isinstance(thenumber,bool):
                result_dict[key] = 0
            elif thenumber.isdigit():
                result_dict[key] = int(thenumber)
            else:
                return -1


        sumval = 0
        for key,val in result_dict.items():
            if key == 'thousand':
                sumval += (val*1000)
            elif key == 'hundred':
                sumval += (val*100)
            elif key == 'ten':
                sumval += (val*10)
            elif key == 'unit':
                sumval += (val*1)
            else:
                pass

        if sumval == 0:
            print (result_dict)
        return sumval


    # @basic.timeit
    def blue(self,maskcode = '1111'):   
        '''
        description: 新版蓝查询
        return {*}
        '''      
        
        logger.debug('查询当前角色魔法量')
        
        shelfmod = self.shelf_mode()
        # print ('shelfmod',shelfmod)
        if shelfmod == 'normal':
            y_axis = 29
            x_base = 21
            logger.debug('in normal mode')
        elif shelfmod == 'shrink':
            y_axis = 12
            x_base = 21
            logger.debug('in shrink mode')
        else:
            logger.debug('in close mode')
            return -1
        # 判断当前状态栏处在什么状态，并初始化范围数据

        areadict_soup = {
            "ten":(self.localx(x_base+14),self.localy(y_axis),3 ,7),
            'hundred':(self.localx(x_base+7),self.localy(y_axis),3 ,7),
            'unit':(self.localx(x_base+21),self.localy(y_axis),3 ,7),
            'thousand':(self.localx(x_base),self.localy(y_axis),3 ,7),
        }

        areadict = {}
        if maskcode[0] == '1':
            areadict['thousand'] = areadict_soup['thousand']
        if maskcode[1] == '1':
            areadict['hundred'] = areadict_soup['hundred']
        if maskcode[2] == '1':
            areadict['ten'] = areadict_soup['ten']
        if maskcode[3] == '1':
            areadict['unit'] = areadict_soup['unit']
        # 根据掩码，继续优化数据源

        keypoint_list = [0,1,6 ,7,10,17,18]
        # 优化过的查询点信息

        result_dict = {}

        for key,val in areadict.items():
            im = pyautogui.screenshot(region = val)
            width, height = im.size
            pic_datasoup = ''
            # 初始化data_soup
            count = 0
            for wid in range(width):
                for hgt in range(height):
                    if count in keypoint_list:
                        # get_R = self.get_pixel_red(start_x+wid,start_y+hgt)
                        get_R = im.getpixel((wid,hgt))[0]
                        # im.putpixel((wid,hgt),(255,0,0))
                        # im.save('d:/%s.png' % key) 
                        if get_R <100:
                            pic_datasoup+='1'
                            # 根据优化过的点数据，获取相应的点颜色
                        else:
                            pic_datasoup+='0'
                    count+=1
            # print (pic_datasoup)
            thenumber = self.number_indentyfy(pic_datasoup)
            # print (thenumber)
            if thenumber == 'e' or isinstance(thenumber,bool):
                result_dict[key] = 0
            elif thenumber.isdigit():
                result_dict[key] = int(thenumber)
            else:
                return -1


        sumval = 0
        for key,val in result_dict.items():
            if key == 'thousand':
                sumval += (val*1000)
            elif key == 'hundred':
                sumval += (val*100)
            elif key == 'ten':
                sumval += (val*10)
            elif key == 'unit':
                sumval += (val*1)
            else:
                pass

        if sumval == 0:
            print (result_dict)
        return sumval


    def skillSlot(self,slotnum,withclick=False):
        '''
        description: 获取技能选择界面的位置（slot 0）以及slot的坐标
        return {*}
        '''        
        offset_y = slotnum * self.skillSlotHeight + 3+self.offset_y(val=2)
        offset_x = self.offset_x()

        pos_x = 0
        pos_y = 0
        file = '%s\\skill_start2.png' % (self.sourcepath)
            
        if os.path.exists(file):  
            ans = pyautogui.locateCenterOnScreen(file,region = self.rect)  

            if ans:
                if slotnum == 0:
                    # 0号槽指的是窗体本身的识别位置
                    return ans
                
                self.lastSkillWindowPos = ans
                pos_x = ans[0] + offset_x
                pos_y = ans[1] + offset_y
                pyautogui.moveTo(pos_x,pos_y,duration=0.1)
                if withclick:
                    # time.sleep(0.2)
                    pyautogui.click()
                
                return [pos_x,pos_y]
            else:
                logger.info('没找到人物技能识别图像')
                return None
        else:
            logger.warn('没找到文件')
            return None


    def skillSlot_maxlevel(self,querylevel = 0,istest=False,isValue = False,withclick=False,isPreview=False):
        
        file = '%s\\skill_level_page.png' % (self.sourcepath)

        ans = pyautogui.locateCenterOnScreen(file,region = self.rect)  
        if ans:
            region = (ans[0]+93,ans[1]+10,ans[0]+94,ans[1]+170)
            im = None
            im = PIL.ImageGrab.grab(region)
            if istest:
                strtime = str(time.time())
                # im.save("C:\\gitlab\\cgclicker\\resources\\test\\"+strtime + '.png')

            maxlevel = 1
            maxoffset = 0
            if querylevel>0:
                loopcount = querylevel
            else:
                loopcount = 10

            for y in range(loopcount):         
                they = (y*self.skillSlotHeight)+int(self.skillSlotHeight*0.5)  +6
                
                getcolor = im.getpixel((0,they))
                logger.debug('the color is')
                logger.debug(getcolor)
                if (getcolor[0]+getcolor[1]+getcolor[2])>3:
                    maxlevel = y+1
                    maxoffset = they
                else:
                    break
            logger.info('技能最高等级是%i级' % maxlevel)
            target_x = ans[0] + self.offset_x()
            target_y = ans[1]+maxoffset # + self.offset_y(val=1)
            if withclick:
                pyautogui.moveTo(target_x,target_y,duration=0.1)
                time.sleep(0.1)
                pyautogui.click()

            if isPreview:
                pyautogui.moveTo(target_x,target_y,duration=0.1)

            if isValue:
                logger.info('返回坐标')
                return [target_x,target_y]
            else:
                logger.info('返回级数')
                return maxlevel
        else:
            logger.warn('等级选择页面没有打开')
            return False



    def offset_x(self,val = 10,noneg = False):
        if noneg:
            seed = random.random()
        else:
            seed = random.random()-0.5

        return int(val*seed)

    def offset_y(self,val = 5,noneg = False):
        if noneg:
            seed = random.random()
        else:
            seed = random.random()-0.5

        return int(val*seed)




    def pixelSolver(self,center,val = '',region = [10,2],istest=True,targetColor=[255,255,255]):
        if istest:
            region = [10,10]
        bbox_orig = (center[0]-region[0],center[1]-region[1],center[0]+region[0],center[1]+region[1])
        im = None
        im = PIL.ImageGrab.grab(bbox_orig)
        if istest:
            strtime = str(time.time())
            # im.save("C:\\gitlab\\cgclicker\\resources\\test\\"+strtime + '.png')
        for x in range(region[0]*2):
            for y in range(region[1]*2):                
                getcolor = im.getpixel((x,y))
                if getcolor[0]==targetColor[0] and getcolor[1]==targetColor[1] and getcolor[2]==targetColor[2]:
                    
                    return 1
        return 0

    def mosterSlot(self,q=1):
        '''
        description: 详细的获取怪物信息，可以在读秒外使用
        return {*}
        '''        
        pass

    def numMonster(self):
        data = self.mosterState()
        count = 0
        for i in range(10):
            if data[i]['ishas']:
                count += 1
        return count

    def mosterState(self,isdisplay=True):
        '''
        在读秒的时候使用
        粗略的获取怪物的信息，只有位置和有没有怪
        description: 从左到右，从下到上，1到10
        return {*}
        '''
        # base_x = self.rect[0]
        # base_y = self.rect[1]-26
        
        # 83.606 开始  截取怪物的名字
        if self.wintype == 800:
            slots = {
                1:[115,385],
                2:[195,340],
                3:[275,295],
                4:[353,250],
                5:[433,205],
                6:[40,323],
                7:[120,278],
                8:[200,233],
                9:[280,188],
                10:[358,141]        
            }
        else:
            slots = {
                1:[90,310],
                2:[154,273],
                3:[218,236],
                4:[282,199],
                5:[346,162],
                6:[40,260],
                7:[94,223],
                8:[158,186],
                9:[222,149],
                10:[286,112]        
            }

        # self.setAttack(False)
        self.resetMouse()
        dataDict = {}
        
        for i in range(10):
            id= i+1
            #print ('pos',id)
            realpos_x = self.localx(slots[id][0])
            realpos_y = self.localy(slots[id][1])-26
            
            # pyautogui.moveTo(realpos_x,realpos_y,duration=1)
            
            gets = self.pixelSolver([realpos_x,realpos_y])
            lineName = "back"
            if id < 6:
                lineName = "front"
            if gets == 1:
                dataDict[i+1] = {'ishas':True,'pos':[realpos_x,realpos_y],'id':id,'line':lineName}
            else:
                dataDict[i+1] = {'ishas':False,'pos':None,'id':id,'line':lineName}


        if isdisplay:
            # 以更直观的方式打印出来识别信息
            self.display_moster_status(dataDict)

        return dataDict
    
    def display_moster_status(self,data):
        display_data = {}
        for key,val in data.items():
            newkey = 'slot'+str(key)
            dis_str = '[无] --- '
            if val['ishas']:
                dis_str = '存活 --- '

            display_data[newkey] = dis_str

        line1 = '= 6 ====== 7 ====== 8 ====== 9 ====== 10 ==='
        line2 = '%(slot6)s%(slot7)s%(slot8)s%(slot9)s%(slot10)s' % display_data
        line3 = '= 1 ====== 2 ====== 3 ====== 4 ====== 5 ===='
        line4 = '%(slot1)s%(slot2)s%(slot3)s%(slot4)s%(slot5)s' % display_data
        print ('windows size:',self.wintype)
        print(line1)
        print(line2)
        print(line3)
        print(line4)


    def blue_sync(self,clr):
        '''
        description: 根据输入值，判断蓝槽当前是有蓝还是没蓝，或者是空数据
        return {*}
        '''        
        good = (125, 183, 255)
        empty = (0, 0, 1)
        isgood = self.isSimilarColor(clr,good)
        if isgood:
            return 'good'
        else:
            isempty = self.isSimilarColor(clr,empty)
            if isempty:
                return 'empty'
        return 'other'


    def blue_percent(self,im):
        pos_100 = (34,2)
        pos_90 = (31,2)
        pos_75 = (27,2)
        pos_60 = (22,2)
        pos_50 = (19,2)
        pos_40 = (16,2)
        pos_25 = (11,2)
        pos_10 = (6,2)
        pos_0 = (3,2)
        val_50 = im.getpixel(pos_50)

        ans_50 = self.blue_sync(val_50)
        if ans_50 == 'good':
            val_75 = im.getpixel(pos_75)
            ans_75 = self.blue_sync(val_75)
            if ans_75 == 'good':
                val_90 = im.getpixel(pos_90)
                ans_90 = self.blue_sync(val_90)
                if ans_90 == 'good':
                    val_100 = im.getpixel(pos_100)
                    ans_100 = self.blue_sync(val_100)
                    if ans_100 == 'good':
                        return 100
                    else:
                        return 90   
                else:
                    return 75         
            else:
                val_60 = im.getpixel(pos_60)
                ans_60 = self.blue_sync(val_60)
                if ans_60 == 'good':
                    return 60
                else:
                    return 50
        else:            
            val_25 = im.getpixel(pos_25)
            ans_25 = self.blue_sync(val_25)
            if ans_25 == 'good':
                val_40 = im.getpixel(pos_40)
                ans_40 = self.blue_sync(val_40)
                if ans_40 == 'good':
                    return 40
                else:
                    return 25
            else:
                val_10 = im.getpixel(pos_10)
                ans_10 = self.blue_sync(val_10)
                if ans_10 == 'good':
                    return 10
                else:
                    val_0 = im.getpixel(pos_0)
                    ans_0 = self.blue_sync(val_0)
                    if ans_0 == "good":
                        return 0
                    else:
                        return 5


    def red_sync(self,clr):        
        '''
        description: 根据输入值，判断血槽当前是有血还是没血，或者是空数据
        return {*}
        '''        
        good = (255, 0, 0)
        empty = (84, 84, 84)
        isgood = self.isSimilarColor(clr,good)
        if isgood:
            return 'good'
        else:
            isempty = self.isSimilarColor(clr,empty)
            if isempty:
                return 'empty'
        return 'other'


    def red_percent(self,im):
        pos_100 = (34,0)
        pos_90 = (31,0)
        pos_75 = (27,0)
        pos_60 = (22,0)
        pos_50 = (19,0)
        pos_40 = (16,0)
        pos_25 = (11,0)
        pos_10 = (6,0)
        pos_0 = (3,0)
        val_50 = im.getpixel(pos_50)

        ans_50 = self.red_sync(val_50)
        if ans_50 == 'good':
            val_75 = im.getpixel(pos_75)
            ans_75 = self.red_sync(val_75)
            if ans_75 == 'good':
                val_90 = im.getpixel(pos_90)
                ans_90 = self.red_sync(val_90)
                if ans_90 == 'good':
                    val_100 = im.getpixel(pos_100)
                    ans_100 = self.red_sync(val_100)
                    if ans_100 == 'good':
                        return 100
                    else:
                        return 90   
                else:
                    return 75         
            else:
                val_60 = im.getpixel(pos_60)
                ans_60 = self.red_sync(val_60)
                if ans_60 == 'good':
                    return 60
                else:
                    return 50
        else:            
            val_25 = im.getpixel(pos_25)
            ans_25 = self.red_sync(val_25)
            if ans_25 == 'good':
                val_40 = im.getpixel(pos_40)
                ans_40 = self.red_sync(val_40)
                if ans_40 == 'good':
                    return 40
                else:
                    return 25
            else:
                val_10 = im.getpixel(pos_10)
                ans_10 = self.red_sync(val_10)
                if ans_10 == 'good':
                    return 10
                else:
                    val_0 = im.getpixel(pos_0)
                    ans_0 = self.red_sync(val_0)
                    if ans_0 == "good":
                        return 0
                    else:
                        return 5

    def is_need_heel(self,line=51):
        '''
        description: 在战斗animation阶段判断是否有补血的必要
        return {*}
        '''        
        pyautogui.press('home')
        positiondata = self.humanState()
        list_val = []
        for i in positiondata.values():
            if i['ishas']:
                list_val.append(i)
        list_val.sort(key=lambda x:str(x['red']),reverse=True)
        lowest = 100
        for i in list_val:
            keyval = int(i['red'])
            if keyval < lowest:
                lowest = keyval
        print ('当前最低为 ',lowest,'/',line)
        if lowest< line:
            print('激活补血')
            return True
        else:
            print ('没有触发')
            return False

    @basic.timeit
    def humanState(self,checkred=True,checkblue=False,isDisplay=True):
        # pyautogui.press('home')
        self.resetMouse()
        key_color = [
            (158, 200, 202),
            (250, 248, 241),
            (105, 86, 37)
            ]

        if self.wintype == 800:
            slots = {
                1:[433,523],  #416,565
                2:[514,479],
                3:[595,435], # ok
                4:[676,391],
                5:[758,348],
                6:[358,466], # 341,506
                7:[438,421], # 421 462
                8:[518,376],
                9:[596,330],
                10:[676,285]        
            }
        else:
            slots = {
                1:[345,415],
                2:[410,380],
                3:[475,345],
                4:[540,310],
                5:[605,275],
                6:[285,370],
                7:[349,334],
                8:[413,298],
                9:[476,261],
                10:[540,225]        
            }
        # self.setAttack(False)
        pyautogui.press('home')
        dataDict_temp = {}
        length = 35
        height = 3 +47
        for i in range(10):
            isvalid = False
            # 判断是否是有效位置的检查项
            id= i+1
            
            realpos_x = self.localx(slots[id][0]) -19
            realpos_y = self.localy(slots[id][1]) + 17 # -25

            bbox_orig = (realpos_x,realpos_y,length,height)
            im = pyautogui.screenshot(region=bbox_orig)
            # im.save('d:/number/%i.png' % i)
            get_list = []
            for px,clr in enumerate(key_color):
                get_clr = im.getpixel((px,0))
                gets = self.isSimilarColor(clr,get_clr)
                get_list.append(gets)
            uni_get_list = list(set(get_list))
            if len(uni_get_list) == 1 and uni_get_list[0] == True:
                isvalid = True
            
            red_pct = '__'
            blue_pct = '__'

            if isvalid:
                if checkred:
                    red_pct = str(self.red_percent(im)).zfill(2)
                else:
                    red_pct = '__'
                if checkblue:
                    blue_pct = str(self.blue_percent(im)).zfill(2)
                else:
                    blue_pct = '__'

            dataDict_temp[str(i+1)] = [isvalid,red_pct,blue_pct]
        if isDisplay:
            self.display_human_status(dataDict_temp)

        dataDict = {}
        for i in range(10):
            id= i+1
            #print ('pos',id)
            realpos_x = self.localx(slots[id][0]) 
            realpos_y = self.localy(slots[id][1])
            current_slot = dataDict_temp[str(id)]
            dataDict[id] = {'ishas':current_slot[0],'pos':[realpos_x,realpos_y],'id':id,'red':current_slot[1],'blue':current_slot[2]}

        return dataDict

    def display_human_status(self,data):
        display_data = {}
        for key,val in data.items():
            newkey = 'slot'+key
            dis_str = 'None --- '
            if val[0]:
                dis_str = '%s/%s' % (val[1],val[2])
                if len(dis_str) == 5:
                    dis_str = dis_str + ' -- '
                else:
                    dis_str = dis_str + ' - '

            display_data[newkey] = dis_str

        line1 = '= 6 ====== 7 ====== 8 ====== 9 ====== 10 ==='
        line2 = '%(slot6)s%(slot7)s%(slot8)s%(slot9)s%(slot10)s' % display_data
        line3 = '= 1 ====== 2 ====== 3 ====== 4 ====== 5 ===='
        line4 = '%(slot1)s%(slot2)s%(slot3)s%(slot4)s%(slot5)s' % display_data
        print ('windows size:',self.wintype)
        print(line1)
        print(line2)
        print(line3)
        print(line4)
    


    def isBattle(self):
        '''
        description: 快速查询是否在战斗状态
        return {*}
        '''        
        check = self.status()
        if check != 'peace':
            return True
        else:
            return False

    def isBattleAni(self):
        check = self.status()
        if check == 'animation':
            return True
        else:
            return False


    def reset_autobattle_pos(self,q=0):
        '''
        description: 将autobattle的窗口移动到指定位置
        return {*}
        '''     
        if self.wintype == 800:
            tx=self.localx(100)
            ty=self.localy(470)
        else:
            tx=self.localx(100)
            ty=self.localy(370)
        
        if q:
            return [tx,ty]
        
        baseinfo = self.isAutoBattle(isRunover=False,isClearMouse=True)
        if not baseinfo['status']:
            logger.warning('自动战斗面板没有打开，跳过')
            return False
        sx = 0
        sy = 0
        isSuccess = False
        for i in range(10):
            baseinfo = self.isAutoBattle(isRunover=False,isClearMouse=True)
            if not baseinfo['status']:
                time.sleep(0.5)
                continue
            else:
                sx = baseinfo['pos'][0]
                sy = baseinfo['pos'][1]
                

                if sx and sy:
                    offset_x = tx-sx
                    offset_y = ty-sy

                    if (offset_x + offset_y) == 0:
                        isSuccess = True
                        
                    else:
                        pyautogui.moveTo(sx,sy,duration=0.2)
                        pyautogui.dragRel(tx-sx,ty-sy,duration=1)
        return isSuccess

    

    def setAutoBattle(self,val,f=1,renew=1):
        if isinstance(val,bool):
            # if not val:

            check = self.isAutoBattle(isRunover=True,isClearMouse=True)
            logger.debug('start battle is [%s]' % str(check['status']))
            if renew:
                if check['runover'] and (val is True): 
                    self.resetMouse()
                    pyautogui.rightClick()
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('z') 
                    time.sleep(0.1)
                    pyautogui.press('z') 
                    pyautogui.keyUp('ctrl')
                    logger.info('刷新自动战斗')
                    # val = True

            for i in range(2):
                if check['status'] != val:
                    self.resetMouse()
                    pyautogui.rightClick()
                    # 取消自动的时候一定要聚焦一下
                    pyautogui.keyDown('ctrl') 
                    pyautogui.press('z') 
                    pyautogui.keyUp('ctrl') 
                    logger.info('自动战斗设置为 %s' % str(val))
                else:
                    logger.debug('期望设置的状态为[%s]，当前状态为[%s],设置相符，退出。'  % (str(val),str(check['status'])))
                    break
                check = self.isAutoBattle(isClearMouse=True)



    def isAutoBattle(self,isRunover=False,isAutorun=False,isClearMouse=False,f=False):
        '''
        description:这个功能改写成查询自动攻击的状态的字典
        return {dict}  {"status":True/False,"run":True/False,"runover":True/False,"pos":[0,0]}
        '''    
        basicDict = {"status":None,"run":None,"runover":None,"pos":[0,0]}    

        self.resetMouse()
        ans=False                
        fullpath = '%s\\autobattle\\800on.png' % (self.sourcepath)
        if self.is_small_window():
            fullpath = '%s\\autobattle\\640on.png' % (self.sourcepath)

        # print (os.path.exists(fullpath))
        # print (self.rect)
        try:
            ans = pyautogui.locateCenterOnScreen(fullpath,region = self.rect)
        except:
            ans = None
        finally:
            pass
        if ans:
            logger.debug('正在自动战斗')
            basicDict['status'] = True
            basicDict['pos'] = ans
            if isAutorun:
                bbox_orig = (ans[0]-50,ans[1]+0,ans[0]+10,ans[1]+30)
                btlpic = '%s/auto_taopao.png' % self.sourcepath
                runans = pyautogui.locateCenterOnScreen(btlpic,region = bbox_orig)
                if runans:                    
                    basicDict['run'] = True
                else:
                    basicDict['run'] = False

            if isRunover:
                length = 5
                height = 3
                bbox_orig = (ans[0]-18,ans[1]+59,length,height)
                im = pyautogui.screenshot(region=bbox_orig)
                # im.save('d:/test.png')

                for x in range(length):
                    getcolor = im.getpixel((x,2))
                    if getcolor[0] == 255 and getcolor[1] == 0 and getcolor[2] ==0 :
                        im = None
                        logger.info('自动战斗剩余次数即将耗尽')
                        basicDict['runover'] = True
                        break

                if basicDict['runover'] == None:
                    basicDict['runover'] = False
                    logger.info('自动战斗剩余次数充沛')

        else:
            logger.debug('自动战斗没有打开')
            basicDict['status'] = False
        return basicDict
        

    def isPetSkillWindow(self):
        self.resetMouse()
        onmark = 'pet_skill_start.png'
        onPath = '%s\\%s' % (self.sourcepath,onmark)

        # for i in marks
        ans = pyautogui.locateCenterOnScreen(onPath,region = self.rect)

        if ans:
            logger.info('宠物技能面板已经打开了')
            return True
        else:
            logger.info('宠物技能面板')
            return False

    def isPetSkillSelected(self):
        '''
        description: 查询当前宠物技能窗口是否打开的状态
        return {*}
        '''

        win_width = self.rect[2]
        x = (self.rect[0]) + win_width-280
        y = (self.rect[1]) + 50 +3
        
        length = 20
        dataArea = (x,y,length,1)
        
        im = pyautogui.screenshot(region=dataArea)
        clr1 = (250, 247, 237)
        clr2 = (0,0,0)
        mark1 = False
        mark2 = False
        for i in range(length):
            pt1 = im.getpixel((i,0))
            if not mark1:
                mark1 = self.isSimilarColor(pt1,clr1)
            if not mark2:
                mark2 = self.isSimilarColor(pt1,clr2)
            if mark1 and mark2:
                return 'Sel'
        if mark1 and (not mark2):
            return 'wait'
        return 'error'


    def islike(self,val1,val2,the = 12):
        '''
        description: battle step 配套功能
        return {*}
        '''        
        threod = abs(val1-val2)
        if threod<the:
            return (True)
        else:
            return (False)

    def compareData(self,data1,data2,the=12):
        '''
        description: battle step 配套功能，快速比较时用的
        return {*}
        '''        
        ans1 = self.islike(data1['lt'],data2['lt'],the=the)
        ans2 = self.islike(data1['lb'],data2['lb'],the=the)
        #print(ans1,ans2)
        return (ans1 and ans2)





if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    test = battle_query(m)

    ans = test.reset_autobattle_pos()
    print (ans)


