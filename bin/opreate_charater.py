#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2023-12-15 00:11:50
LastEditTime: 2024-02-14 02:23:49
LastEditors: figo - uz
Description: 新版的操作角色用的模块
FilePath: \cgclicker\bin\opreate_charater.py
copyright: figo software 2020-2021
'''

import sys
import logging
import time
sys.path.append('.')
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('opreatesys')
logger.setLevel(logging.INFO)

try:
    from bin import basic
except ModuleNotFoundError:
    from . import basic
finally:pass
try:
    from bin import battle
except ModuleNotFoundError:
    from . import battle
finally:pass
# try:
#     from bin import itemSystem
# except ModuleNotFoundError:
#     from . import itemSystem
# finally:pass
try:
    from bin import hospital_heal
except ModuleNotFoundError:
    from . import hospital_heal
finally:pass

class opreate_character(basic.basic):
    def __init__(self,rect):        
        super(opreate_character,self).__init__()
                
        self.rect = self.real_rect(rect)
        # 这个修正取的是画面中真正的rect，不包含边框和标题
        self.blue_now = -1  # 最新检查出来的蓝量
        self.blue_maximum = 0 # 检查出来蓝量的最大值
        self.red_now = -1 # 最新检查出来的血量
        self.red_maximum = 0 # 检查出来血量的最大值
        self._is_blue_low = False # 是否低于警戒线
        self._is_red_low = False # 是否低于警戒线

        self.battle_count = 0 # 战斗了多少次
        self.battle_count_trigger = False  
        self.battle_count_list = []

        self.is_logout = False # 是否要登出
        self.boaring_time = None # 目前已经发呆了多久
        self.boaring_time_start = None # 开始发呆的时间
        self.boaring_time_max = 500 # 发呆时间警告线
        self.status_log = 'loading'

        self.is_fast_eat = False # 设置是否快速吃料理

        self.init_dict = {'blue_check_line':150,
                        'red_check_line':300,
                        'ischeck_red':False,
                        'ischeck_blue':True
                        }
        # 配置这个字典实现功能切换

        # self.itemSys = itemSystem.itemSystem(rect=rect)
        # 初始化物品栏系统

        self.healSys = hospital_heal.Hospital(rect=rect)
        # 初始化医院系统
        
        self.battleSys = battle.battle(rect=rect)
        # 初始化战斗系统

    def set_fast_eat(self,data):
        self.is_fast_eat = bool(data)

    def set_init_data(self,data):
        # print ('dddddddd',data)
        self.init_dict.update(data)
        self.init_data()
        

    def init_data(self):
        self.blue_line = self.init_dict['blue_check_line']
        self.blue_trigger = self.init_dict['ischeck_blue']

        
        self.red_line = self.init_dict['red_check_line']
        self.red_trigger = self.init_dict['ischeck_red']

    def check_blue(self):
        '''
        description: 当前魔法量的检查功能，会更新当前值到全局，并判断是否低于警戒值
        return {*}
        '''  
        if self.blue_trigger:
            self.blue_now = self.battleSys.blue()
            if self.blue_now>self.blue_maximum:self.blue_maximum = self.blue_now
            blue_iter = self.blue_now - self.blue_line
            if (blue_iter < 31) and (self.blue_now >= 0):
                logger.info('蓝量警戒已经触发 %s / %s' % (str(self.blue_now),str(self.blue_line)))
                self.is_blue_low = True
            else:
                self.is_blue_low = False



    @property
    def is_blue_low(self):
        return self._is_blue_low

    @is_blue_low.setter
    def is_blue_low(self,val):
        logger.debug('is blue low now is %s' % str(val))
        self._is_blue_low = val 

    @property
    def is_red_low(self):
        return self._is_red_low

    @is_red_low.setter
    def is_red_low(self,val):
        logger.debug('is red low now is %s' % str(val))
        self._is_red_low = val 

    def check_red(self):
        '''
        description: 当前血量的检查功能，会更新当前值到全局，并判断是否低于警戒值
        return {*}
        '''        
        if self.red_trigger:
            self.red_now = self.battleSys.red()
            if self.red_now>self.red_maximum:self.red_maximum = self.red_now
            red_iter = self.red_now - self.red_line
            if (red_iter < 31) and (self.red_now > 0):
                self.is_red_low = True
            else:
                self.is_red_low = False

    def display_status(self):
        print ('=====',self.rect,'=====')
        print ('red ',self.red_now,'of',self.red_maximum)
        print ('blue',self.blue_now,'of',self.blue_maximum)
        print ('status',self.status_now)
        print ('count',self.battle_count)
        print ('===========================')


    def peace_action(self):
        '''
        description: 如果是和平模式，默认会吃料理补魔，检查当前角色血蓝线
        return {*}
        '''   
        self.check_blue()
        self.check_red()     
        if self.is_blue_low:
            
            logger.info('使用回蓝物品')
            # fnisheat = self.itemSys.eat(withUse=True,blue_or_red='blue',isfast=self.is_fast_eat)
            # if fnisheat:
            #     logger.info('蓝量恢复完毕')
        else:
            self.battleSys.setAutoBattle(True,renew=1,f=0)


    def anim_action(self):
        '''
        description: 战斗动画阶段，默认功能是检查自动战斗以及本角色的血蓝线
        return {*}
        '''        
        self.battleSys.setAutoBattle(True,renew=1,f=0)
        logger.info('refresh auto battle done')
        self.check_blue()
        self.check_red()

    def battlesel_action(self):

        self.battleSys.setAutoBattle(True,renew=1,f=0)
        # 这里写一些给自己回血的功能，丢血瓶什么的

    
    def pet_action(self):
        '''
        description: 宠物选择技能阶段，默认是使用1号技能攻击敌人
        return {*}
        '''        
        self.battleSys.petCastSkill(1,target='enermy')

    def battle_count_fn(self,status):
        
        if status == 'peace':  
            self.battle_count_list = []    
        else:
            self.battle_count_list.append(status)

        merged_lst = []
        for item in self.battle_count_list:
            if not merged_lst or item != merged_lst[-1]:
                merged_lst.append(item)
        self.battle_count = merged_lst.count('battle_sel')

    def main(self):
        self.init_data()
        for i in range(10):
            self.status_now = self.status()
            # print ('opc',i,self.status_now)
            if self.status_now in ['loading']:
                time.sleep(0.1)
        # 等待读屏结束

        if self.status_now == 'peace':
            self.peace_action()      
        elif self.status_now == 'animation':
            self.anim_action()
        elif self.status_now == 'battle_sel':
            self.battlesel_action()
        elif self.status_now == 'pet':
            self.pet_action()
        else:
            logger.debug (self.status_now)

        self.battle_count_fn(self.status_now)   
        self.display_status()

