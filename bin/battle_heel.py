#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2023-12-16 22:22:00
LastEditTime: 2023-12-17 22:25:30
LastEditors: figo - uz
Description: 
FilePath: \cgclicker\bin\battle_heel.py
copyright: figo software 2020-2021
'''

import sys
import logging
import time

import pyautogui
sys.path.append('.')
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s|%(name)s - %(module)s - %(funcName)s : %(message)s')
logger = logging.getLogger('opreatesys')
logger.setLevel(logging.DEBUG)

try:
    from bin import battle_query
except ModuleNotFoundError:
    from . import battle_query
finally:pass

try:
    from bin import itemSystem
except ModuleNotFoundError:
    from . import itemSystem
finally:pass

class battle_heel(battle_query.battle_query):
    def __init__(self,rect):
        
        super(battle_heel,self).__init__(rect)
                
        self.rect = self.real_rect(rect)

        self.onColor = [147 ,187 ,108]

        self.itemsys = itemSystem.itemSystem(rect)

    def open_bag_in_battle(self):
        if self.is_small_window():
            pos_x = self.localx(620)
            pos_y = self.localy(50)
        else:
            pos_x = self.localx(720)
            pos_y = self.localy(50)

        pyautogui.moveTo(pos_x,pos_y,duration=0.1)
        pyautogui.click()
        getcolor = pyautogui.pixel(pos_x-2,pos_y)
        check_ans = self.isSimilarColor(getcolor,self.onColor)
        print (check_ans)
        return check_ans
        
    def gg(self):
        humandata = self.humanState()
        target = []
        for key,val in humandata.items():
            if val['ishas']:
                target = val['pos']
        self.open_bag_in_battle()
        self.itemsys.eat(withUse=True,blue_or_red='red',ispeace=False)
        pyautogui.moveTo(target[0],target[1])
        pyautogui.click()

if __name__ == "__main__":
    import getArea
    cgw = getArea.cgwindows()
    cgw.update()
    m = cgw.main['size']
    print (m)
    whr = battle_heel(m)
    whr.open_bag_in_battle()